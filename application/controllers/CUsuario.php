<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CUsuario extends CI_Controller {


	//Administrador
	public function usuariosAdministrador()
	{
		if (!$this->session->userdata('s_usuario')) {
		 redirect('/login/','refresh');
	 }
		else{

		  $mensaje=$this->session->flashdata("mensaje");
			if(isset($mensaje)){
				$data["mensaje"]=$mensaje;
			}
			$error=$this->session->flashdata("error");
			if(isset($error)){
				$data["error"]=$error;
			}
			//Consultando tipos de usuarios
		$this->db->select("*");
		$this->db->from("t_usuario");
		$data["tiposUsuarios"]=$this->db->get()->result();

		//Consultando usuarios
		$this->db->select("idusuario,nombre,username,telefono,puesto,salario,t_usuario.tipo,habilitado");
		$this->db->from("usuario");
		$this->db->join('t_usuario','usuario.idtipouser=t_usuario.idtipouser');
		$data["usuarios"]=$this->db->get()->result();

		//Trayendo todas las sedes
		$this->db->select("idsede,sede");
		$this->db->from("sede");

        $this->db->where('habilitado',0);
		$data["sedes"]=$this->db->get()->result();

        $this->load->view('home/head');
		$this->load->view('home/navbar');
		$this->load->view("home/header");
		$this->load->view('admin/usuariosAdmin/usuario',$data);
		$this->load->view("home/footer");
	}
	}
	//Administrador
	public function crearUsuario()
	{
		//echo $this->input->post("nombre");
		$this->input->post("username");


		$this->db->select("count(*) cont");
		$this->db->from("usuario");
		$this->db->where("username",$this->input->post("username"));
		//echo $this->db->get()->result()[0]->cont;
		if($this->db->get()->result()[0]->cont>'0')
		{
			$this->session->set_flashdata('mensaje',"Username Repetido!");
      		$this->session->set_flashdata('error',true);
       		redirect('index.php/CUsuario/usuariosAdministrador','refresh');
		}else if(( pathinfo($_FILES["imagen"]["name"], PATHINFO_EXTENSION) == 'jpg'
		|| pathinfo($_FILES["imagen"]["name"], PATHINFO_EXTENSION) == 'png'
		|| pathinfo($_FILES["imagen"]["name"], PATHINFO_EXTENSION) == 'gif'
		|| pathinfo($_FILES["imagen"]["name"], PATHINFO_EXTENSION) == 'PNG')
		|| pathinfo($_FILES["imagen"]["name"], PATHINFO_EXTENSION) == 'JPG'){

			$this->input->post("telefono");
			$this->input->post("puesto");
			$this->input->post("salario");
			$this->input->post("pass");
			$this->input->post("pass1");
			$target_dir = "public/photos/";
			$nombrearchivo= microtime(true).'.'.pathinfo($_FILES["imagen"]["name"], PATHINFO_EXTENSION);
			$nombrereal=$_FILES["imagen"]["name"];
			$target_file = $target_dir.$nombrearchivo;

			if (move_uploaded_file($_FILES["imagen"]["tmp_name"], $target_file)) {

			}
			$datos= array(
				'idtipouser'=>$this->input->post("tipo"),
				'idarea'=>$this->input->post("area"),
				'nombre'=>$this->input->post("nombre"),
				'username'=>$this->input->post("username"),
				'password'=>sha1($this->input->post("pass")),
				'telefono'=>$this->input->post("telefono"),
				'puesto'=>$this->input->post("puesto"),
				'salario'=>$this->input->post("salario"),
				'foto'=>$nombrearchivo,
				'habilitado'=>1,

			);


			$this->db->insert('usuario',$datos);
			$this->session->set_flashdata('mensaje',"Usuario insetado Correctamente!");
      		$this->session->set_flashdata('error',false);
       		redirect('index.php/CUsuario/usuariosAdministrador','refresh');
		}else{
			$this->session->set_flashdata('mensaje',"Imagen no valida!");
      		$this->session->set_flashdata('error',true);
			redirect('index.php/CUsuario/usuariosAdministrador','refresh');
		}

	}

	//Administrador
	public function deshabilitarUsuario($id)
	{
		$this->db->where('idusuario',$id);
		$this->db->update('usuario',array('habilitado'=>0));
		if($this->db->affected_rows()>0)
		{
			$this->session->set_flashdata('mensaje',"Usuario Deshabilitado!");
      		$this->session->set_flashdata('error',false);
       		redirect('index.php/CUsuario/usuariosAdministrador','refresh');
		}else{
			$this->session->set_flashdata('mensaje',"Ocurrio un error!");
      		$this->session->set_flashdata('error',true);
       		redirect('index.php/CUsuario/usuariosAdministrador','refresh');
		}
	}
	//Administrador
	public function habilitarUsuario($id)
	{
		$this->db->where('idusuario',$id);
		$this->db->update('usuario',array('habilitado'=>1));
		if($this->db->affected_rows()>0)
		{
			$this->session->set_flashdata('mensaje',"Usuario Habilitado!");
      		$this->session->set_flashdata('error',false);
       		redirect('index.php/CUsuario/usuariosAdministrador','refresh');
		}else{
			$this->session->set_flashdata('mensaje',"Ocurrio un error!");
      		$this->session->set_flashdata('error',true);
       		redirect('index.php/CUsuario/usuariosAdministrador','refresh');
		}
	}



	public function verImagenUsuario($id)
	{
		$this->db->select("foto");
        $this->db->from("usuario");
        $this->db->where("idusuario",$id);
        $curriculum = $this->db->get()->result()[0];

        force_download('public/photos /'.$curriculum->foto, NULL);
	}

	//ver perfil tecnico
	public function verPerfilTecnico()
	{
		//
		$mensaje=$this->session->flashdata("mensaje");
		if(isset($mensaje)){
			$data["mensaje"]=$mensaje;
		}
		$error=$this->session->flashdata("error");
		if(isset($error)){
			$data["error"]=$error;
		}
		//capturando id
		$idUsuario= $this->session->userdata("s_idusuario");

		//consulta de informacion del usuarios
		$this->db->select("*");
		$this->db->from("usuario");
		$this->db->where("idusuario",$idUsuario);
		$data["usuario"]=$this->db->get()->result();

		//informacion de las actividades
		$this->db->select("t.idticket, u.nombre nombreusu, sede.sede area,
		u.sede, u.telefono, e.nombre nombreequipo, e.idequipo idequipo,i.idinvequipo, t.descripcion, t.fechainicio,
		t.fechafinal, t.estado, t.tipomantenimiento, t.informetecnico, a.fechaasignacion");
		$this->db->from("ticket t");
		$this->db->join("inventarioequipo i","t.idinvequipo=i.idinvequipo");
		$this->db->join("equipo e","i.idequipo=e.idequipo");
		$this->db->join("usuario u","t.idusuario=u.idusuario");
		$this->db->join("sede","sede.idsede=u.idarea");
		$this->db->join("asignacionticket a","t.idticket=a.idticket");
		$this->db->where("a.idusuario",$idUsuario);
		$this->db->order_by("a.fechaasignacion", "desc");
		$data["ticket"]=$this->db->get()->result();

		//informacion de Mantenimientos
		$this->db->select("count(t.idticket) finalizado");
		$this->db->from("ticket t");
		$this->db->join("inventarioequipo i","t.idinvequipo=i.idinvequipo");
		$this->db->join("equipo e","i.idequipo=e.idequipo");
		$this->db->join("usuario u","t.idusuario=u.idusuario");
		$this->db->join("asignacionticket a","t.idticket=a.idticket");
		$this->db->where("a.idusuario",$idUsuario);
		$this->db->where("t.estado",1);
		$data["finalizados"]=$this->db->get()->result()[0];

		$this->db->select("count(t.idticket) solicitado");
		$this->db->from("ticket t");
		$this->db->join("inventarioequipo i","t.idinvequipo=i.idinvequipo");
		$this->db->join("equipo e","i.idequipo=e.idequipo");
		$this->db->join("usuario u","t.idusuario=u.idusuario");
		$this->db->join("asignacionticket a","t.idticket=a.idticket");
		$this->db->where("a.idusuario",$idUsuario);
		$this->db->where("t.estado",2);
		$data["solicitados"]=$this->db->get()->result()[0];

		$this->db->select("count(t.idticket) proceso");
		$this->db->from("ticket t");
		$this->db->join("inventarioequipo i","t.idinvequipo=i.idinvequipo");
		$this->db->join("equipo e","i.idequipo=e.idequipo");
		$this->db->join("usuario u","t.idusuario=u.idusuario");
		$this->db->join("asignacionticket a","t.idticket=a.idticket");
		$this->db->where("a.idusuario",$idUsuario);
		$this->db->where("t.estado",4);
		$data["enproceso"]=$this->db->get()->result()[0];

		//cargando vistas
		$this->load->view('home/head');
		$this->load->view('home/navbar');
		$this->load->view("home/header");
		$this->load->view('personalTecnico/perfiltecnico',$data);
		$this->load->view("home/footer");
	}

	//info ticketverperfil
	public function infoTicket()
	{
		//informacion por post
		$idticket=$this->input->post('idticket');

		//consulta de info del ticket
		$this->db->select("t.idticket, u.nombre nombreusu, sede.sede area,
		u.sede, u.telefono, e.nombre nombreequipo, e.idequipo idequipo, t.descripcion, t.fechainicio,
		t.fechafinal, t.estado, t.tipomantenimiento, t.informetecnico, a.fechaasignacion, t.informetecnico");
		$this->db->from("ticket t");
		$this->db->join("inventarioequipo i","t.idinvequipo=i.idinvequipo");
		$this->db->join("equipo e","i.idequipo=e.idequipo");
		$this->db->join("usuario u","t.idusuario=u.idusuario");
		$this->db->join("sede","sede.idsede=u.idarea");
		$this->db->join("asignacionticket a","t.idticket=a.idticket");
		$this->db->where("t.idticket",$idticket);
		echo json_encode($this->db->get()->result());
	}

	//info ticketverperfil
	public function infoEquipo()
	{
		//informacion por post
		$idequipo=$this->input->post('idequipo');

		//consulta de info del ticket
		$this->db->select("*");
		$this->db->from("observaciones");
		$this->db->where("idinvequipo",$idequipo);
		echo json_encode($this->db->get()->result());
	}

	//minutos del ticket
	public function minutosTicket()
	{
		//informacion por post
		$idticket=$this->input->post('idticket');
		//consulta de cantidad minutos
		$this->db->select("MINUTE(TIMEDIFF(fechafinal,fechainicio)) minutos");
		$this->db->from("mantenimiento");
		$this->db->where("idticket",$idticket);
		echo json_encode($this->db->get()->result());
	}

	//salario del tecnico
	public function salarioTecnico()
	{
		//informacion por post
		$id=$this->session->userdata("s_idusuario");
		//consulta de cantidad minutos
		$this->db->select("salario");
		$this->db->from("usuario");
		$this->db->where("idusuario",$id);
		echo json_encode($this->db->get()->result());
	}

	//cambiar estado de Ticket en proceso
	public function empezarTicket()
	{
		//
		$id=$this->input->post('midEquipo');
		$this->db->where('idticket',$id);
		$this->db->update('ticket',array('estado'=>4));
		if($this->db->affected_rows()>0)
		{
			$datos= array(
				'idticket'=>$id,
				'fechainicio'=>date('Y-m-d H:i:s')
			);
			$this->db->insert('mantenimiento',$datos);
			$this->session->set_flashdata('mensaje',"Orden de trabajo iniciada!");
      $this->session->set_flashdata('error',false);
      redirect('index.php/CUsuario/verPerfilTecnico','refresh');
			}else{
			$this->session->set_flashdata('mensaje',"Ocurrio un error!");
      $this->session->set_flashdata('error',true);
      redirect('index.php/CUsuario/verPerfilTecnico','refresh');
		}
	}

	public function FinalizarTicket($id)
	{
		//informacion del ticket
		$this->db->select("ticket.descripcion,ticket.tipomantenimiento,ticket.fechainicio,usuario.nombre name,ticket.idticket,equipo.nombre, mantenimiento.idmantenimiento");
		$this->db->from("ticket");
		$this->db->join("usuario","usuario.idusuario=ticket.idusuario");
		$this->db->join("inventarioequipo","inventarioequipo.idinvequipo=ticket.idinvequipo");
		$this->db->join("equipo","inventarioequipo.idequipo=equipo.idequipo");
		$this->db->join("asignacionticket","ticket.idticket=asignacionticket.idticket");
		$this->db->join("mantenimiento","asignacionticket.idticket=mantenimiento.idticket");
		$this->db->where("ticket.idticket",$id);
		$data["infot"]=$this->db->get()->result()[0];


		$this->load->view('home/head');
		$this->load->view('home/navbar');
		$this->load->view("home/header");
		$this->load->view('personalTecnico/finalizarT',$data);
		$this->load->view('home/footer');
	}

	public function modificarUsuario($id)
	{
			$this->db->select("*");
			$this->db->from("usuario");
			$this->db->where("idusuario",$id);
			$data["usuario"]=$this->db->get()->result()[0];

			//Trayendo todas las sedes
			$this->db->select("idsede,sede");
			$this->db->from("sede");
			$data["sedes"]=$this->db->get()->result();

			$this->load->view('home/head');
			$this->load->view('home/navbar');
			$this->load->view("home/header");
			$this->load->view("admin/usuariosAdmin/usuariosMod",$data);
			$this->load->view("home/footer");
	}


	public function infoUsuario()
	{
			$id=$this->input->post('idusuario');
			$this->db->select("*");
			$this->db->from("usuario");
			$this->db->where("idusuario",$id);
			echo json_encode($this->db->get()->result());
	}

	public function modificarNombre()
	{
			$this->db->where("idusuario",$this->input->post("id"));
			$this->db->update("usuario",array('nombre'=>$this->input->post("nombre")));
			$this->session->set_flashdata('insert', true);
			$this->session->set_flashdata('mensaje',"Nombre");
	redirect('/CUsuario/modificarUsuario/'.$this->input->post("id"), 'refresh');
	}

	public function modificarUsername()
	{
			$this->db->where("idusuario",$this->input->post("id"));
			$this->db->update("usuario",array('username'=>$this->input->post("username")));
			$this->session->set_flashdata('insert', true);
			$this->session->set_flashdata('mensaje',"Username");
	redirect('/CUsuario/modificarUsuario/'.$this->input->post("id"), 'refresh');
	}

	public function modificarArea()
	{
			$this->db->where("idusuario",$this->input->post("id"));
			$this->db->update("usuario",array('idarea'=>$this->input->post("area")));
			$this->session->set_flashdata('insert', true);
			$this->session->set_flashdata('mensaje',"Area");
	redirect('/CUsuario/modificarUsuario/'.$this->input->post("id"), 'refresh');
	}

	public function modificarPass()
	{
			$this->db->where("idusuario",$this->input->post("id"));
			$this->db->update("usuario",array('password'=>sha1($this->input->post("password"))));
			$this->session->set_flashdata('insert', true);
			$this->session->set_flashdata('mensaje',"Contraseña");
	redirect('/CUsuario/modificarUsuario/'.$this->input->post("id"), 'refresh');
	}

	public function modificarTelefono()
	{
			$this->db->where("idusuario",$this->input->post("id"));
			$this->db->update("usuario",array('telefono'=>$this->input->post("telefono")));
			$this->session->set_flashdata('insert', true);
			$this->session->set_flashdata('mensaje',"Telefono");
	redirect('/CUsuario/modificarUsuario/'.$this->input->post("id"), 'refresh');
	}

	public function modificarPuesto()
	{
			$this->db->where("idusuario",$this->input->post("id"));
			$this->db->update("usuario",array('puesto'=>$this->input->post("puesto")));
			$this->session->set_flashdata('insert', true);
			$this->session->set_flashdata('mensaje',"Puesto");
	redirect('/CUsuario/modificarUsuario/'.$this->input->post("id"), 'refresh');
	}

	public function modificarSalario()
	{
			$this->db->where("idusuario",$this->input->post("id"));
			$this->db->update("usuario",array('salario'=>$this->input->post("salario")));
			$this->session->set_flashdata('insert', true);
			$this->session->set_flashdata('mensaje',"Salario");
	redirect('/CUsuario/modificarUsuario/'.$this->input->post("id"), 'refresh');
	}

	public function actualizarImagen()
	{
		if(( pathinfo($_FILES["imagen"]["name"], PATHINFO_EXTENSION) == 'jpg'
		|| pathinfo($_FILES["imagen"]["name"], PATHINFO_EXTENSION) == 'png'
		|| pathinfo($_FILES["imagen"]["name"], PATHINFO_EXTENSION) == 'gif'
		|| pathinfo($_FILES["imagen"]["name"], PATHINFO_EXTENSION) == 'PNG')
		|| pathinfo($_FILES["imagen"]["name"], PATHINFO_EXTENSION) == 'JPG'){

			$this->input->post("telefono");
			$this->input->post("puesto");
			$this->input->post("salario");
			$this->input->post("pass");
			$this->input->post("pass1");
			$target_dir = "public/photos/";
			$nombrearchivo= microtime(true).'.'.pathinfo($_FILES["imagen"]["name"], PATHINFO_EXTENSION);
			$nombrereal=$_FILES["imagen"]["name"];
			$target_file = $target_dir.$nombrearchivo;

			if (move_uploaded_file($_FILES["imagen"]["tmp_name"], $target_file)) {

			}
			$this->db->where("idusuario",$this->input->post("id"));
			$this->db->set('foto',$nombrearchivo);
			$this->db->update("usuario");
			$this->session->set_flashdata('insert', true);
			$this->session->set_flashdata('mensaje',"Foto actualizada correctamente");
			redirect('/CUsuario/modificarUsuario/'.$this->input->post("id"), 'location');
		}else
		{
			$this->session->set_flashdata('insert', false);
			$this->session->set_flashdata('mensaje',"Ocurrio un error");
			redirect('/CUsuario/modificarUsuario/'.$this->input->post("id"), 'location');
		}


	}
}
