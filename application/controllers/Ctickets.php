<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ctickets extends CI_Controller {


	//Administrador
	public function ticketsAdmin()
	{
		if (!$this->session->userdata('s_usuario')) {
		 redirect('/login/','refresh');
	 }
		else{

		$this->db->select("idticket,usuario.nombre,equipo.nombre nombreequipo,ticket.descripcion,cast(ticket.fechainicio as date) fecha,ticket.estado,ticket.tipomantenimiento");
		$this->db->from("ticket");
		$this->db->join("usuario","usuario.idusuario=ticket.idusuario");
		$this->db->join("inventarioequipo","inventarioequipo.idinvequipo=ticket.idinvequipo");
		$this->db->join("equipo","equipo.idequipo = inventarioequipo.idequipo");
		$this->db->where("inventarioequipo.habilitado",1);
		//$this->db->where("inventarioequipo.habilitado",0)
		$data["tickets"]=$this->db->get()->result();

		$this->load->view('home/head');
		$this->load->view('home/navbar');
		$this->load->view("home/header");
		$this->load->view('admin/ticketAdmin/ticket',$data);
		$this->load->view("home/footer");
	}
	}
	//Administrador
	public function observacionesAdmin()
	{
		if (!$this->session->userdata('s_usuario')) {
		 redirect('/login/','refresh');
	 }
		else{

		$this->db->select("equipo.nombre,equipo.idequipo,sede.sede,observaciones.idinvequipo,observaciones.idobservacion,observaciones.observacion ");
		$this->db->from("observaciones");
		$this->db->join("inventarioequipo","observaciones.idinvequipo=inventarioequipo.idinvequipo");
		$this->db->join("equipo","equipo.idequipo=inventarioequipo.idequipo");
		$this->db->join("sede","inventarioequipo.sede=sede.idsede");
		$data["inventario"]=$this->db->get()->result();
		$this->load->view('home/head');
		$this->load->view('home/navbar');
		$this->load->view("home/header");
		$this->load->view('admin/ticketAdmin/observaciones',$data);
		$this->load->view("home/footer");
	}
	}
	//Administrador
	public function ticketsAsignar($id)
	{
		if (!$this->session->userdata('s_usuario')) {
		 redirect('/login/','refresh');
	 }
		else{
		//informacion del ticket
		$this->db->select("ticket.descripcion,tipomantenimiento,fechainicio,usuario.nombre name,ticket.idticket,equipo.nombre");
		$this->db->from("ticket");
		$this->db->join("usuario","usuario.idusuario=ticket.idusuario");
		$this->db->join("inventarioequipo","inventarioequipo.idinvequipo=ticket.idinvequipo");
		$this->db->join("equipo","inventarioequipo.idequipo=equipo.idequipo");
		$this->db->where("ticket.idticket",$id);
		$data["infot"]=$this->db->get()->result()[0];


		$this->db->select("usuario.nombre,usuario.idusuario,sede.sede area,usuario.puesto,usuario.sede");
		$this->db->from("usuario");
		$this->db->join("sede","sede.idsede=usuario.idarea");
		$this->db->where("idtipouser",3);
		$data["tecnicos"]=$this->db->get()->result();
		$this->load->view('home/head');
		$this->load->view('home/navbar');
		$this->load->view("home/header");
		$this->load->view('admin/ticketAdmin/asignarTickets',$data);
		$this->load->view('home/footer');
	}
	}

	public function asignarTicketSave($idticket, $idTecnico)
	{
		//Verificamos que este ticket no este asignado
		$this->db->select("estado");
		$this->db->from("ticket");
		$this->db->where("idticket",$idticket);
		$info=$this->db->get()->result()[0];
		if($info->estado>0)
		{
			redirect('/Ctickets/ticketsAdmin', 'refresh');
		}

		//Generamos la orden
		$datos=array(
			'idusuario'=>$this->session->userdata("s_idusuario")
		);
		$this->db->insert("orden",$datos);
		$idorden = $this->db->insert_id();

		$datosNuevo = array(
			'idticket'=>$idticket,
			'idusuario'=>$idTecnico,
			'fechaAsignacion'=>date('Y-m-d H:i:s'),
			'idorden'=>$idorden
		);
		$this->db->insert("asignacionticket",$datosNuevo);

		//cambiando el estado del ticket
		$this->db->where("idticket",$idticket);
		$this->db->update("ticket",array('estado'=>2));
		$this->session->set_flashdata('insert', true);
		redirect('/Ctickets/ticketsAdmin', 'refresh');
	}

	//personal Medico
	public function generarTickets()
	{
		$this->db->select("equipo.idequipo, inventarioequipo.idinvequipo, equipo.nombre, marca, serie, modelo, clasificacion, fabricante,
		aniofabri, servicio, idespecificaciones, idadquisicion, tiempomantprev,	tipoequipo.nombre tipoequipo ");
		$this->db->from("equipo");
		$this->db->join("tipoequipo","tipoequipo.idtequipo=equipo.tipoequipo");
		$this->db->join("inventarioequipo","equipo.idequipo=inventarioequipo.idequipo");
		$this->db->join("sede","sede.idsede=inventarioequipo.sede");
		$this->db->where('sede.idsede',$this->session->userdata("s_area"));
		$this->db->where('inventarioequipo.habilitado',1);
		$data["equipos"]=$this->db->get()->result();

		$this->load->view('home/head');
		$this->load->view('home/navbar');
		$this->load->view("home/header");
		$this->load->view('ticketMedico/ticketsgenerar',$data);
		$this->load->view('home/footer');
	}

	//personal Medico
	public function generarTicketEquipo($id)
	{
		$data["id"]=$id;
		$this->load->view('home/head');
		$this->load->view('home/navbar');
		$this->load->view("home/header");
		$this->load->view('medico/ticket/ticketGenerar',$data);

		$this->load->view('home/footer');
	}

	//Personal Medico
	public function saveTicket()
	{
		$idequipo=$this->input->post("id");
		$descripcion = $this->input->post("descripcion");
		$datos =
		array(
			'idusuario'=>$this->session->userdata("s_idusuario"),
			'idinvequipo'=>$idequipo,
			'descripcion'=>$descripcion,
			'fechainicio'=>date('Y-m-d H:i:s'),
			'estado'=>0,
			'tipomantenimiento'=>'Correctivo',
			'color'=>'#F81919'
		);
		$this->db->insert("ticket",$datos);
		$this->session->set_flashdata('insert', true);
		redirect('index.php/Ctickets/generarTickets' , 'refresh');
	}

	//Personal Medico
	public function addObservaciones()
	{
		$this->db->select("equipo.nombre,equipo.idequipo,sede.sede,inventarioequipo.idinvequipo");
		$this->db->from("inventarioequipo");
		$this->db->join("equipo","equipo.idequipo=inventarioequipo.idequipo");
		$this->db->join("sede","inventarioequipo.sede=sede.idsede");
		$this->db->where("sede.idsede",$this->session->userdata("s_area"));
		$data["inventario"]=$this->db->get()->result();

		$this->load->view('home/head');
		$this->load->view('home/navbar');
		$this->load->view("home/header");
		$this->load->view('ticketMedico/addObservaciones',$data);
		$this->load->view('home/footer');
	}
//Personal Medico
	public function addObservacionesEquipo($id)
	{
		$data["id"]=$id;
		$this->load->view('home/head');
		$this->load->view('home/navbar');
		$this->load->view("home/header");
		$this->load->view("medico/observaciones/observacion",$data);
		$this->load->view('home/footer');
	}
//Personal Medico
	public function addObservacionesEquipoSave()
	{
		$id=$this->input->post("id");
		$observacion=$this->input->post("descripcion");
		$datos=array(
			'idinvequipo'=>$id,
			'observacion'=>$observacion,
			'visto'=>0,
			'fecha'=>date('Y-m-d H:i:s')
		);
		$this->db->insert("observaciones",$datos);
		$this->session->set_flashdata('insert', true);
		redirect('index.php/Ctickets/addObservaciones', 'refresh');
	}

	public function verEstado()
	{
		//informacion del ticket
		$this->db->select("inventarioequipo.idinvequipo,equipo.idequipo,estado,equipo.nombre,equipo.marca,equipo.modelo");
		$this->db->from("ticket");
		$this->db->join("inventarioequipo","inventarioequipo.idinvequipo=ticket.idinvequipo");
		$this->db->join("equipo","equipo.idequipo=inventarioequipo.idequipo");
		$this->db->join("usuario u","ticket.idusuario=u.idusuario");
		$this->db->join("sede","sede.idsede=u.idarea");
		$this->db->where("sede.idsede",$this->session->userdata('s_area'));
		$this->db->where_in("estado",array(0,1,2,4));
		$data["equipos"]=$this->db->get()->result();


		$this->load->view('home/head');
		$this->load->view('home/navbar');
		$this->load->view("home/header");
		$this->load->view('ticketMedico/verEstadoCombo',$data);
		$this->load->view('home/footer');
	}

	//funcion para agregar materiales y tambien para modificar del inventario
	public function materialesInventario(){
		//modificando el ticket
    $informe=$this->input->post("informe");
		$id=$this->input->post('idticket');
		$idmantenimiento=$this->input->post('idmantenimiento');
		$this->db->where('idticket',$id);
		$this->db->update('ticket',array('informetecnico'=>$informe,'estado'=>1));

		if($this->db->affected_rows()>0)
		{
			$this->db->where('idticket',$id);
			$this->db->update('mantenimiento',array('fechafinal'=>date('Y-m-d H:i:s')));

			if($this->db->affected_rows()>0)
			{
				$idmateriales=$this->input->post("materiales");
		    $cantidades=$this->input->post("cantidades");
		    $i=0;$j=0;
		    $materiales=null;
				if(!empty($idmateriales))
       {
		    foreach($idmateriales as $id)
		    {
					foreach($cantidades as $cantidad)
		      {
		       if($i==$j)
		       {
		          $materiales[]=array('id'=>$id, 'cantidad'=>$cantidad);
		       }
		       $j++;
		      }
		      $j=0;
		      $i++;
		    }
			}

			if(!empty($materiales))
		 	{
				foreach($materiales as $m)
        {
            $data=array(
                'idmantenimiento'=>$idmantenimiento,
                'idinventario'=>$m["id"],
                'cantidad'=>$m["cantidad"]
            );
            $this->db->insert("mantinvent",$data);

						//consulta de inventario para saber cantidades
						$this->db->select("cantidad");
						$this->db->from("inventario");
						$this->db->where("idinventario",$m["id"]);
						$info=$this->db->get()->result()[0];

						//actualizar el inventario
						$cantidadtotal=0;
						$cantidadfinal=0;
						$cantidadtotal=$info->cantidad;
						$cantidadfinal=$cantidadtotal-$m["cantidad"];
						$this->db->where('idinventario',$m["id"]);
						$this->db->update('inventario',array('cantidad'=>$cantidadfinal));

						if ($this->db->affected_rows()==0) {
							$this->session->set_flashdata('mensaje',"no se actualizo inventario");
				      $this->session->set_flashdata('error',false);
				      redirect('index.php/CUsuario/verPerfilTecnico','refresh');
						}
        }
			}

				$this->session->set_flashdata('mensaje',"Orden de trabajo finalizada con exito!");
	      $this->session->set_flashdata('error',false);
	      redirect('index.php/CUsuario/verPerfilTecnico','refresh');
			}
			else {
				$this->session->set_flashdata('mensaje',"Ocurrio un error en actualizar tick!");
	      $this->session->set_flashdata('error',true);
	      redirect('index.php/CUsuario/verPerfilTecnico','refresh');
			}

		}
		else {
			$this->session->set_flashdata('mensaje',"Ocurrio un error al primer if!");
      $this->session->set_flashdata('error',true);
      redirect('index.php/CUsuario/verPerfilTecnico','refresh');
		}

	}
	//fin de la funcion materialesInventario

	public function getTickets()
	{
		if ($this->session->userdata('s_tipoUser') != 2) {
			$this->db->select('t.idticket, u.nombre nombreusu, sede.sede area,
			u.telefono, e.nombre nombreequipo, e.idequipo idequipo,i.idinvequipo, t.descripcion,
			t.estado, t.tipomantenimiento, t.informetecnico, CONCAT(t.tipomantenimiento,\'-'.'\',u.nombre) title,
			CONCAT(date_format(a.fechaAsignacion,\'%Y-%m-%d\'),\'T'.'\',date_format(a.fechaAsignacion,\'%h:%i:%s\')) start, t.color');
			$this->db->from("ticket t");
			$this->db->join("inventarioequipo i","t.idinvequipo=i.idinvequipo");
			$this->db->join("equipo e","i.idequipo=e.idequipo");
			$this->db->join("usuario u","t.idusuario=u.idusuario");
			$this->db->join("sede","sede.idsede=u.idarea");
			$this->db->join("asignacionticket a","t.idticket=a.idticket");
			$this->db->where("sede.idsede",$this->session->userdata('s_area'));
			echo json_encode($this->db->get()->result());

		}else {
			//calendario para administrador
			$this->db->select('t.idticket, u.nombre nombreusu, u.area,
			u.sede, u.telefono, e.nombre nombreequipo, e.idequipo idequipo,i.idinvequipo, t.descripcion,
			t.estado, t.tipomantenimiento, t.informetecnico, a.fechaasignacion, CONCAT(t.tipomantenimiento,\'-'.'\',u.nombre) title,
			CONCAT(date_format(a.fechaAsignacion,\'%Y-%m-%d\'),\'T'.'\',date_format(a.fechaAsignacion,\'%h:%i:%s\')) start, t.color');
			$this->db->from("ticket t");
			$this->db->join("inventarioequipo i","t.idinvequipo=i.idinvequipo");
			$this->db->join("equipo e","i.idequipo=e.idequipo");
			$this->db->join("usuario u","t.idusuario=u.idusuario");
			$this->db->join("asignacionticket a","t.idticket=a.idticket");
			echo json_encode($this->db->get()->result());
		}

	}

}
