<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CMateriales extends CI_Controller {


    //Administrador
    public function verInventarioMateriales()
        {
            $this->db->select("idinventario,nombre,cantidad,descripcion,precio,proveedor,sede");
            $this->db->from("inventario");
            $data["inventario"]=$this->db->get()->result();
            $this->load->view('home/head');
            $this->load->view('home/navbar');
            $this->load->view("home/header");
            $this->load->view("admin/materiales/materiales",$data);
            $this->load->view("home/footer");
        }
    public function saveMaterial()
    {
        $datos=array(
            'nombre'=>$this->input->post("nombre"),
            'cantidad'=>$this->input->post("cantidad"),
            'descripcion'=>$this->input->post("descrip"),
            'precio'=>$this->input->post("precio"),
            'proveedor'=>$this->input->post("prov"),
            'sede'=>$this->input->post("sede")
        );
        $this->db->insert("inventario",$datos);
        $this->session->set_flashdata('insert', true);
		redirect('/CMateriales/verInventarioMateriales', 'refresh');
    }


    public function modificarMaterial($id)
    {
        $this->db->select("idinventario,nombre,cantidad,descripcion,precio,proveedor,sede");
        $this->db->from("inventario");
        $this->db->where("idinventario",$id);
        $data["inventario"]=$this->db->get()->result()[0];

        $this->load->view('home/head');
        $this->load->view('home/navbar');
        $this->load->view("home/header");
        $this->load->view("admin/materiales/materialesMod",$data);
        $this->load->view("home/footer");
    }

    //Funciones para modificar
    public function modificarNombre()
    {
        $this->db->where("idinventario",$this->input->post("id"));
        $this->db->update("inventario",array('nombre'=>$this->input->post("nombre")));
        $this->session->set_flashdata('insert', true);
        $this->session->set_flashdata('mensaje',"Nombre");
		redirect('/CMateriales/modificarMaterial/'.$this->input->post("id"), 'refresh');
    }

    public function modificarCantidad()
    {
        $this->db->where("idinventario",$this->input->post("id"));
        $this->db->update("inventario",array('cantidad'=>$this->input->post("cantidad")));
        $this->session->set_flashdata('insert', true);
        $this->session->set_flashdata('mensaje',"Cantidad");
		redirect('/CMateriales/modificarMaterial/'.$this->input->post("id"), 'refresh');
    }

    public function modificarDescripcion()
    {
        $this->db->where("idinventario",$this->input->post("id"));
        $this->db->update("inventario",array('descripcion'=>$this->input->post("descripcion")));
        $this->session->set_flashdata('insert', true);
        $this->session->set_flashdata('mensaje',"Descripción");
		redirect('/CMateriales/modificarMaterial/'.$this->input->post("id"), 'refresh');
    }

    public function modificarPrecio()
    {
        $this->db->where("idinventario",$this->input->post("id"));
        $this->db->update("inventario",array('precio'=>$this->input->post("precio")));
        $this->session->set_flashdata('insert', true);
        $this->session->set_flashdata('mensaje',"Precio");
		redirect('/CMateriales/modificarMaterial/'.$this->input->post("id"), 'refresh');
    }

    public function modificarProveedor()
    {
        $this->db->where("idinventario",$this->input->post("id"));
        $this->db->update("inventario",array('proveedor'=>$this->input->post("proveedor")));
        $this->session->set_flashdata('insert', true);
        $this->session->set_flashdata('mensaje',"Proveedor");
		redirect('/CMateriales/modificarMaterial/'.$this->input->post("id"), 'refresh');
    }
    public function modificarSede()
    {
        $this->db->where("idinventario",$this->input->post("id"));
        $this->db->update("inventario",array('sede'=>$this->input->post("sede")));
        $this->session->set_flashdata('insert', true);
        $this->session->set_flashdata('mensaje',"Sede");
		redirect('/CMateriales/modificarMaterial/'.$this->input->post("id"), 'refresh');
    }

    //Administrador
    public function getInventarioMateriales()
    {
        $this->db->select("idinventario,nombre,cantidad,descripcion,precio,proveedor,sede");
        $this->db->from("inventario");
        echo json_encode($this->db->get()->result());
    }

    public function getCantidadMaterial()
    {
        $id=$this->input->post('idmaterial');
        $this->db->select("cantidad");
        $this->db->from("inventario");
        $this->db->where("idinventario",$id);
        echo json_encode($this->db->get()->result());
    }
}
