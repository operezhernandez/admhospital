<?php
  class CLogin extends CI_Controller
  {

    function __construct()
    {
      parent:: __construct();

      $this->load->model('MLogin');
    }

    public function index(){
      if ($this->session->userdata('s_usuario')) {
        redirect('/inicio/','refresh');
      }

      $mensaje=$this->session->flashdata("mensaje");
        if(isset($mensaje)){
            $data["mensaje"]=$mensaje;
        }
        $error=$this->session->flashdata("error");
        if(isset($error)){
            $data["error"]=$error;
        }

        if(isset($mensaje)){
          $this->load->view('vLogin',$data);
        }else {
          $this->load->view('vLogin');
        }
    }

    public function ingresar(){
      $username=$this->input->post('txtUsername');
      $pass=sha1($this->input->post('txtPass'));

      $res=$this->MLogin->ingresar($username,$pass);

      if ($res==1) {

        $this->db->select("equipo.idequipo,inventarioequipo.idinvequipo,equipo.tiempomantprev,formaadquisicion.fecha,now(),datediff(now(),cast(formaadquisicion.fecha as datetime)) diferencia,preventido");
        $this->db->from("inventarioequipo");
        $this->db->join("equipo",'equipo.idequipo=inventarioequipo.idequipo');
        $this->db->join('formaadquisicion','formaadquisicion.idadquisicion=equipo.idadquisicion');
        $inventarioequipo=$this->db->get()->result();


        foreach($inventarioequipo as $in)
        {
          if($in->diferencia>=($in->tiempomantprev*$in->preventido))
          {
            $data=array(
              'idusuario'=>6,
              'idinvequipo'=>$in->idinvequipo,
              'descripcion'=>'MANTENIMIENTO PREVENTIVO, CREADO POR EL SISTEMA',
              'fechainicio'=>date('Y-m-d H:i:s'),
              'estado'=>0,
              'tipomantenimiento'=>'Preventivo',
              'color'=>'#19A7F8'
            );
            $this->db->insert('ticket',$data);

            $data=array(
              'preventido'=>($in->preventido+1)
            );
            $this->db->where('idequipo',$in->idequipo);
            $this->db->update('equipo',$data);
          }
        }



        redirect('/inicio/', 'refresh');
      }
      else{
        $this->session->set_flashdata('mensaje',$res["mensaje"]);
        $this->session->set_flashdata('error',$res["error"]);
        redirect('/login/','refresh');
      }
    }

    public function prueba()
    {

    }
    public function cerrarSesion(){
      $this->session->sess_destroy();
      redirect('/login/', 'refresh');
    }
  }
?>
