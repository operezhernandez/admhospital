<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CInventario extends CI_Controller {
   //Administrador
    public function inventario()
    {
        $data["vali"]=true;
        $mensaje=$this->session->flashdata("mensaje");
        if(isset($mensaje)){
            $data["mensaje"]=$mensaje;
        }
        $error=$this->session->flashdata("error");
        if(isset($error)){
            $data["error"]=$error;
        }

        $this->db->select("equipo.idequipo, equipo.nombre ,equipo.marca ,equipo.serie,equipo.modelo,equipo.clasificacion,equipo.fabricante,
		    equipo.aniofabri,equipo.servicio,equipo.idespecificaciones ,equipo.idadquisicion ,	equipo.tiempomantprev,	tipoequipo.nombre tipoequipo ");
        $this->db->from("equipo");
        $this->db->join("tipoequipo","tipoequipo.idtequipo=equipo.tipoequipo");
        $data["equipos"]=$this->db->get()->result();

        $this->db->select("idservicio,nombre");
        $this->db->from("servicio");
        $this->db->where('habilitado',0);
        $data["servicios"]=$this->db->get()->result();

        $this->db->select("idtequipo,nombre");
        $this->db->from("tipoequipo");
        $this->db->where("habilitado",0);
        $data["tipo"]=$this->db->get()->result();

        $this->load->view('home/head');
		    $this->load->view('home/navbar');
		    $this->load->view("home/header");
		    $this->load->view('admin/equipos/inventarioEquipo',$data);
        $this->load->view("home/footer");

    }

    public function detallesEquipos($id)
    {
        $data["vali"]=true;
        $mensaje=$this->session->flashdata("mensaje");
        if(isset($mensaje)){
            $data["mensaje"]=$mensaje;
        }
        $error=$this->session->flashdata("error");
        if(isset($error)){
            $data["error"]=$error;
        }

        //Inventario de equipoF
        $this->db->select("idinvequipo,sede.sede,inventarioequipo.habilitado");
        $this->db->join("sede","sede.idsede=inventarioequipo.sede");
        $this->db->from("inventarioequipo");
        $this->db->where('idequipo',$id);
        $data["inventario"]=$this->db->get()->result();

        //Listado de sedes
        $this->db->select("idsede,sede");
        $this->db->from("sede");
        $this->db->where('habilitado',0);
        $data["sedes"]=$this->db->get()->result();

        //Informacion del equipo
        $this->db->select("eq.idequipo,eq.nombre,eq.marca,eq.serie,eq.modelo,eq.clasificacion,eq.fabricante,eq.aniofabri,servicio.nombre servicio,
        eq.tiempomantprev,eq.tipoequipo,f.formaadquisicion,f.fecha,f.precio,f.proveedor,f.numfactura,f.iniciogarantia,f.fingarantia,
        e.amperaje,e.frecuencia,e.potencia,e.voltaje,e.preinstalacion");
        $this->db->from("equipo eq");
        $this->db->join("formaadquisicion f","f.idadquisicion = eq.idadquisicion");
        $this->db->join("especificaciones e","e.idespecificaciones = eq.idespecificaciones");
        $this->db->join('servicio','servicio.idservicio=eq.servicio');
        $this->db->where('eq.idequipo',$id);
        $data["detalle"]=$this->db->get()->result()[0];

        //Informacion de tickets

        $this->db->select("ticket.idticket,descripcion,ticket.fechainicio,estado,tipomantenimiento,informetecnico");
        $this->db->from("ticket");
        $this->db->join("inventarioequipo","inventarioequipo.idinvequipo=ticket.idinvequipo");
     // $this->db->join("mantenimiento","mantenimiento.idticket = ticket.idticket");
        $this->db->where("inventarioequipo.idequipo",$id);
        $data["tickets"]=$this->db->get()->result();
        $this->load->view('home/head');
		$this->load->view('home/navbar');
        $this->load->view("home/header");

        $this->load->view("admin/equipos/detalles",$data);
        $this->load->view("home/footer");
    }

    public function guardarEquipo()
    {



        /*$this->input->post("formaadquisicion");
        $this->input->post("fechaadquisicion");
        $this->input->post("precioadquisicion");
        $this->input->post("proveedoradquisicion");
        $this->input->post("numfactad");
        $this->input->post("inigarantia");
        $this->input->post("fingarantia");*/
        $dataforma = array(
            'formaadquisicion'=>$this->input->post("formaadquisicion"),
            'fecha'=>$this->input->post("fechaadquisicion"),
            'precio'=>$this->input->post("precioadquisicion"),
            'proveedor'=>$this->input->post("proveedoradquisicion"),
            'numfactura'=>$this->input->post("numfactad"),
            'iniciogarantia'=>$this->input->post("inigarantia"),
            'fingarantia'=>$this->input->post("fingarantia")
        );
        $this->db->insert("formaadquisicion",$dataforma);
        $formaadquisicionID=$this->db->insert_id();
       // echo $formaadquisicionID;


        /*$this->input->post("amperaje");
        $this->input->post("frecuencia");
        $this->input->post("potencia");
        $this->input->post("voltaje");
        $this->input->post("preinstalacion");*/
        $dataespecificaciones= array(
            'amperaje'=>$this->input->post("amperaje"),
            'frecuencia'=>$this->input->post("frecuencia"),
            'potencia'=>$this->input->post("potencia"),
            'voltaje'=>$this->input->post("voltaje"),
            'preinstalacion'=>$this->input->post("preinstalacion")
        );
        $this->db->insert("especificaciones",$dataespecificaciones);
        $dataespecificacionesID=$this->db->insert_id();
       // echo '<br>'.$dataespecificacionesID;


       $this->input->post("nombre");
        $this->input->post("marca");
        $this->input->post("tipo");
        $this->input->post("serie");
        $this->input->post("modelo");
        $this->input->post("sede");
        $this->input->post("clasificacion");
        $this->input->post("fabricante");
        $this->input->post("aniofabricacion");
        $this->input->post("servicio");
        $this->input->post("tiempomantprev");
        $dataequipo=array(
            'nombre'=>$this->input->post("nombre"),
            'marca'=>$this->input->post("marca"),
            'serie'=>$this->input->post("serie"),
            'modelo'=>$this->input->post("modelo"),
           // 'sede'=>$this->input->post("sede"),
            'clasificacion'=>$this->input->post("clasificacion"),
            'clasificacion2'=>$this->input->post("clasificacion2"),
            'fabricante'=>$this->input->post("fabricante"),
            'aniofabri'=>$this->input->post("aniofabricacion"),
            'servicio'=>$this->input->post("servicio"),
            'tiempomantprev'=> $this->input->post("tiempomantprev"),
            'idespecificaciones'=>$dataespecificacionesID,
            'idadquisicion'=>$formaadquisicionID,
            'tipoequipo'=>$this->input->post("tipo")
        );
        $this->db->insert("equipo",$dataequipo);
        $LAST=$this->db->affected_rows();
        if($LAST>0)
		{
			$this->session->set_flashdata('mensaje',"Equipo insertado con exito!");
      		$this->session->set_flashdata('error',false);
       		redirect('index.php/CInventario/inventario','refresh');
		}else{
			$this->session->set_flashdata('mensaje',"Ocurrio un error!");
      		$this->session->set_flashdata('error',true);
       		redirect('index.php/CInventario/inventario','refresh');
		}
    }

    //Guardando equipo inventariado
    public function guardarInventarioEquipo()
    {
        $id =$this->input->post("idequipo");
        $sede=$this->input->post("sede");
        $datos=array('idequipo'=>$id,'sede'=>$sede);
        $this->db->insert('inventarioequipo',$datos);
        $LAST=$this->db->affected_rows();
        if($LAST>0)
		{
			$this->session->set_flashdata('mensaje',"Equipo inventariado insertado con exito!");
      		$this->session->set_flashdata('error',false);
       		redirect('index.php/CInventario/detallesEquipos/'.$id,'refresh');
		}else{
			$this->session->set_flashdata('mensaje',"Ocurrio un error!");
      		$this->session->set_flashdata('error',true);
       		redirect('index.php/CInventario/detallesEquipos/'.$id,'refresh');
		}


    }

    //deshabilitando equipo inventariado
    public function deshabilitar($id)
    {
        $this->db->where('idinvequipo',$id);
        $this->db->update('inventarioequipo',array('habilitado'=>0));
        $save =$this->db->affected_rows();

        $this->db->select("idequipo");
        $this->db->from("inventarioequipo");
        $this->db->where('idinvequipo',$id);
        $idEquipo=$this->db->get()->result()[0];
		if($save>0)
		{
			$this->session->set_flashdata('mensaje',"Equipo Inventariado Deshabilitado!");
      		$this->session->set_flashdata('error',false);
       		redirect('index.php/CInventario/detallesEquipos/'.$idEquipo->idequipo,'refresh');
		}else{
			$this->session->set_flashdata('mensaje',"Ocurrio un error!");
      		$this->session->set_flashdata('error',true);
       		redirect('index.php/CInventario/detallesEquipos/'.$idEquipo->idequipo,'refresh');
		}
    }


    public function habilitar($id)
    {
        $this->db->where('idinvequipo',$id);
        $this->db->update('inventarioequipo',array('habilitado'=>1));
        $save =$this->db->affected_rows();

        $this->db->select("idequipo");
        $this->db->from("inventarioequipo");
        $this->db->where('idinvequipo',$id);
        $idEquipo=$this->db->get()->result()[0];
		if($save>0)
		{
			$this->session->set_flashdata('mensaje',"Equipo Inventariado Habilitado!");
      		$this->session->set_flashdata('error',false);
       		redirect('index.php/CInventario/detallesEquipos/'.$idEquipo->idequipo,'refresh');
		}else{
			$this->session->set_flashdata('mensaje',"Ocurrio un error!");
      		$this->session->set_flashdata('error',true);
       		redirect('index.php/CInventario/detallesEquipos/'.$idEquipo->idequipo,'refresh');
		}
    }

    //Deshabilitar area
    public function deshabilitarArea($id)
    {
        $this->db->where('idsede',$id);
        $this->db->update('sede',array('habilitado'=>1));
        $save =$this->db->affected_rows();


		if($save>0)
		{
			$this->session->set_flashdata('mensaje',"Area Deshabilitada!");
      		$this->session->set_flashdata('error',false);
       		redirect('index.php/CInventario/verArea','refresh');
		}else{
			$this->session->set_flashdata('mensaje',"Ocurrio un error!");
      		$this->session->set_flashdata('error',true);
       		redirect('index.php/CInventario/verArea','refresh');
		}
    }

    //Habilitar area
    public function habilitarArea($id)
    {
        $this->db->where('idsede',$id);
        $this->db->update('sede',array('habilitado'=>0));
        $save =$this->db->affected_rows();


		if($save>0)
		{
			$this->session->set_flashdata('mensaje',"Area Habilitada!");
      		$this->session->set_flashdata('error',false);
       		redirect('index.php/CInventario/verArea','refresh');
		}else{
			$this->session->set_flashdata('mensaje',"Ocurrio un error!");
      		$this->session->set_flashdata('error',true);
       		redirect('index.php/CInventario/verArea','refresh');
		}
    }

    public function deshabilitarServicio($id)
    {
        $this->db->where('idservicio',$id);
        $this->db->update('servicio',array('habilitado'=>1));
        $save =$this->db->affected_rows();


		if($save>0)
		{
			$this->session->set_flashdata('mensaje',"Servicio Deshabilitada!");
      		$this->session->set_flashdata('error',false);
       		redirect('index.php/CInventario/verServicios','refresh');
		}else{
			$this->session->set_flashdata('mensaje',"Ocurrio un error!");
      		$this->session->set_flashdata('error',true);
       		redirect('index.php/CInventario/verServicios','refresh');
		}
    }

    public function habilitarServicio($id)
    {
        $this->db->where('idservicio',$id);
        $this->db->update('servicio',array('habilitado'=>0));
        $save =$this->db->affected_rows();


		if($save>0)
		{
			$this->session->set_flashdata('mensaje',"Area Habilitada!");
      		$this->session->set_flashdata('error',false);
       		redirect('index.php/CInventario/verServicios','refresh');
		}else{
			$this->session->set_flashdata('mensaje',"Ocurrio un error!");
      		$this->session->set_flashdata('error',true);
       		redirect('index.php/CInventario/verServicios','refresh');
		}
    }

    //Deshabilitar tipoequipo
    public function deshabilitarTipoEquipo($id)
    {
        $this->db->where('idtequipo',$id);
        $this->db->update('tipoequipo',array('habilitado'=>1));
        $save =$this->db->affected_rows();


		if($save>0)
		{
			$this->session->set_flashdata('mensaje',"Tipo equipo Deshabilitado!");
      		$this->session->set_flashdata('error',false);
       		redirect('index.php/CInventario/vertipo','refresh');
		}else{
			$this->session->set_flashdata('mensaje',"Ocurrio un error!");
      		$this->session->set_flashdata('error',true);
       		redirect('index.php/CInventario/vertipo','refresh');
		}
    }

    public function habilitarTipoEquipo($id)
    {
        $this->db->where('idtequipo',$id);
        $this->db->update('tipoequipo',array('habilitado'=>0));
        $save =$this->db->affected_rows();


		if($save>0)
		{
			$this->session->set_flashdata('mensaje',"Tipo equipo Habilitada!");
      		$this->session->set_flashdata('error',false);
       		redirect('index.php/CInventario/vertipo','refresh');
		}else{
			$this->session->set_flashdata('mensaje',"Ocurrio un error!");
      		$this->session->set_flashdata('error',true);
       		redirect('index.php/CInventario/vertipo','refresh');
		}
    }

    public function verServicios()
    {
        $data["vali"]=true;
        $mensaje=$this->session->flashdata("mensaje");
        if(isset($mensaje)){
            $data["mensaje"]=$mensaje;
        }
        $error=$this->session->flashdata("error");
        if(isset($error)){
            $data["error"]=$error;
        }
        $this->db->select("idservicio,nombre,habilitado");
        $this->db->from("servicio");
        $data["servicios"]=$this->db->get()->result();
        $this->load->view('home/head');
		$this->load->view('home/navbar');
        $this->load->view("home/header");
        $this->load->view("admin/servicios/servicios",$data);
        $this->load->view("home/footer");
    }

    public function vertipo()
    {
        $this->db->select("idtequipo,nombre,habilitado");
        $this->db->from("tipoequipo");

        $data["servicios"]=$this->db->get()->result();
        $this->load->view('home/head');
		$this->load->view('home/navbar');
        $this->load->view("home/header");
        $this->load->view("admin/tipoequipo/equipo",$data);
        $this->load->view("home/footer");
    }

    public function guardarServicio()
    {
        $nombre=$this->input->post("nombre");
        $data=array(
            'nombre'=>$nombre
        );
        $this->db->insert("servicio",$data);
        $LAST=$this->db->affected_rows();
        if($LAST>0)
		{
			$this->session->set_flashdata('mensaje',"Equipo inventariado insertado con exito!");
      		$this->session->set_flashdata('error',false);

		}else{
			$this->session->set_flashdata('mensaje',"Ocurrio un error!");
      		$this->session->set_flashdata('error',true);

		}
        redirect('index.php/CInventario/verServicios/','refresh');
    }

    public function guardartipo()
    {
        $nombre=$this->input->post("nombre");
        $data=array(
            'nombre'=>$nombre
        );
        $this->db->insert("tipoequipo",$data);
        $LAST=$this->db->affected_rows();
        if($LAST>0)
		{
			$this->session->set_flashdata('mensaje',"Insertado con exito!");
      		$this->session->set_flashdata('error',false);

		}else{
			$this->session->set_flashdata('mensaje',"Ocurrio un error!");
      		$this->session->set_flashdata('error',true);

		}
        redirect('index.php/CInventario/vertipo/','refresh');
    }

    public function modificarEquipo($id)
    {

        $data["vali"]=true;
        $mensaje=$this->session->flashdata("mensaje");
        if(isset($mensaje)){
            $data["mensaje"]=$mensaje;
        }
        $error=$this->session->flashdata("error");
        if(isset($error)){
            $data["error"]=$error;
        }

        //Informacion del equipo
        $this->db->select("eq.idequipo,eq.nombre,eq.marca,eq.serie,eq.modelo,eq.clasificacion,eq.fabricante,eq.aniofabri,servicio.nombre servicio,
        eq.tiempomantprev,eq.tipoequipo,f.formaadquisicion,f.fecha,f.precio,f.proveedor,f.numfactura,f.iniciogarantia,f.fingarantia,
        e.amperaje,e.frecuencia,e.potencia,e.voltaje,e.preinstalacion,f.idadquisicion,e.idespecificaciones");
        $this->db->from("equipo eq");
        $this->db->join("formaadquisicion f","f.idadquisicion = eq.idadquisicion");
        $this->db->join("especificaciones e","e.idespecificaciones = eq.idespecificaciones");
        $this->db->join("servicio","servicio.idservicio=eq.servicio");
        $this->db->where('eq.idequipo',$id);
        $data["detalle"]=$this->db->get()->result()[0];

        $this->db->select("idtequipo,nombre");
        $this->db->from("tipoequipo");
        $data["tipos"]=$this->db->get()->result();

        $this->db->select("idservicio,nombre");
        $this->db->from("servicio");
        $data["servicios"]=$this->db->get()->result();

        $this->load->view('home/head');
		$this->load->view('home/navbar');
        $this->load->view("home/header");
        $this->load->view("admin/equipos/modificar",$data);
        $this->load->view("home/footer");
    }

    public function modificarCampo()
    {
        $tabla = $this->input->post("tabla");
        $campo = $this->input->post("campo");
        $llave = $this->input->post("llave");
        $id = $this->input->post("id");
        $valor = $this->input->post("valor");

        $this->db->where($llave,$id);
        $this->db->update($tabla,array($campo=>$valor));
        $this->session->set_flashdata('error', false);
        $this->session->set_flashdata('mensaje',$this->input->post("mensaje"));
		redirect('/CInventario/modificarEquipo/'.$id, 'refresh');

    }
    public function verArea()
    {
        $data["vali"]=true;
        $mensaje=$this->session->flashdata("mensaje");
        if(isset($mensaje)){
            $data["mensaje"]=$mensaje;
        }
        $error=$this->session->flashdata("error");
        if(isset($error)){
            $data["error"]=$error;
        }

        $this->db->select("idsede,sede,habilitado");
        $this->db->from("sede");
        $data["areas"]=$this->db->get()->result();
        $this->load->view('home/head');
		    $this->load->view('home/navbar');
        $this->load->view("home/header");
        $this->load->view("admin/area/areas",$data);
        $this->load->view("home/footer");
    }

    public function guardarArea()
    {
        $nombre=$this->input->post("nombre");
        $data=array(
            'sede'=>$nombre
        );
        $this->db->insert("sede",$data);
        $LAST=$this->db->affected_rows();
        if($LAST>0)
		{
			$this->session->set_flashdata('mensaje',"Insertado con exito!");
      		$this->session->set_flashdata('error',false);

		}else{
			$this->session->set_flashdata('mensaje',"Ocurrio un error!");
      		$this->session->set_flashdata('error',true);

		}
        redirect('index.php/CInventario/verArea/','refresh');
    }

}


?>
