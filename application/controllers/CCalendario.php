<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class CCalendario extends CI_Controller
{
   public function  __construct()
   {
    parent::__construct();
    //$this->load->model('MCalendario');
   }

   public function index()
   {
    if (!$this->session->userdata('s_usuario')) {
      redirect('/login/','refresh');
    }
    $mensaje=$this->session->flashdata("mensaje");
      if(isset($mensaje)){
          $data["mensaje"]=$mensaje;
      }
      $error=$this->session->flashdata("error");
      if(isset($error)){
          $data["error"]=$error;
      }
      if (isset($data)) {
        $this->load->view('home/head');
        $this->load->view('home/navbar');
        $this->load->view('home/header');
        $this->load->view('calendario/vCalendario',$data);
        //$this->load->view('home/footer');
      }
      else {
        $this->load->view('home/head');
        $this->load->view('home/navbar');
        $this->load->view('home/header');
        $this->load->view('calendario/vCalendario');
        // $this->load->view('home/footer');
      }

   }

   public function verTicket($id,$estado)
 	{
    //informacion del ticket
    $this->db->select('t.idticket, u.nombre nombreusu, sede.sede area,
    u.sede, u.telefono, e.nombre nombreequipo, e.idequipo idequipo, e.marca, e.serie, e.modelo, e.clasificacion, e.fabricante, i.idinvequipo, t.descripcion,
    t.estado, t.tipomantenimiento, t.informetecnico, a.fechaasignacion');
    $this->db->from("ticket t");
    $this->db->join("inventarioequipo i","t.idinvequipo=i.idinvequipo");
    $this->db->join("equipo e","i.idequipo=e.idequipo");
    $this->db->join("usuario u","t.idusuario=u.idusuario");
    $this->db->join("sede","sede.idsede=u.idarea");
    $this->db->join("asignacionticket a","t.idticket=a.idticket");
		$this->db->where("t.idticket",$id);
		$data["infot"]=$this->db->get()->result()[0];

    $this->db->select('usuario.nombre, sede.sede area, usuario.telefono, usuario.salario');
    $this->db->from("asignacionticket");
    $this->db->join("ticket","ticket.idticket=asignacionticket.idticket");
    $this->db->join("usuario","usuario.idusuario=asignacionticket.idusuario");
    $this->db->join("sede","sede.idsede=usuario.idarea");
		$this->db->where("asignacionticket.idticket",$id);
		$data["infotec"]=$this->db->get()->result()[0];

    if ($estado==1) {
      $this->db->select('fechainicio, fechafinal, MINUTE(TIMEDIFF(fechafinal,fechainicio)) minutos');
      $this->db->from("mantenimiento");
  		$this->db->where("idticket",$id);
  		$data["infominutos"]=$this->db->get()->result()[0];
    }

 		$this->load->view('home/head');
 		$this->load->view('home/navbar');
 		$this->load->view("home/header");
 		$this->load->view('calendario/verCalendarioTicket',$data);
 		$this->load->view('home/footer');
 	}
}
