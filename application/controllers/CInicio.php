<?php
  class CInicio extends CI_Controller
  {

    function __construct()
    {
      parent:: __construct();
      $this->load->model('MInicio');
    }

    public function index()
   {
     if (!$this->session->userdata('s_usuario')) {
      redirect('/login/','refresh');
    }
     else{
      if ($this->session->userdata('s_tipoUser')==2) {
        $data = array(
          'years' => $this->MInicio->years(),
          'tickets' => $this->MInicio->countTickets(date('m'),date('Y')),
          'proceso' => $this->MInicio->countProceso(date('m'),date('Y')),
          'solicitadas' => $this->MInicio->countSolicitadas(date('m'),date('Y')),
          'inventario' => $this->MInicio->Inventario()
        );
        $this->load->view('home/head');
        $this->load->view('home/navbar');
        $this->load->view('home/header');
        $this->load->view('inicio',$data);
        $this->load->view('home/footer');
    }
    else if ($this->session->userdata('s_tipoUser')==1) {
      redirect('CCalendario','refresh');
    }
    else if ($this->session->userdata('s_tipoUser')==3){
      redirect('CUsuario/verPerfilTecnico','refresh');
    }

   }
 }

   public function getDataGrafico(){
     $year=$this->input->post('year');
     $resultados=$this->MInicio->tickets($year);
     echo json_encode($resultados);
   }

   public function getEstadisticas()
   {
       $this->db->select("count(*) contador");
       $this->db->from("ticket");
       $this->db->where("year(fechainicio)= year(curdate())");
       $this->db->where("estado",1);
       $resultTerminada=$this->db->get()->result();

       $this->db->select("count(*) contador");
       $this->db->from("ticket");
       $this->db->where("year(fechainicio)= year(curdate())");
       $this->db->where("estado",2);
       $resultSolicitada=$this->db->get()->result();

       $this->db->select("count(*) contador");
       $this->db->from("ticket");
       $this->db->where("year(fechainicio)= year(curdate())");
       $this->db->where("estado",4);
       $resultProceso=$this->db->get()->result();

       echo json_encode(array(
           'terminadas'=>$resultTerminada[0]->contador,
           'solicitadas'=>$resultSolicitada[0]->contador,
           'proceso'=>$resultProceso[0]->contador
        ));

   }

   public function getUsuarios()
   {
       $this->db->select("count(*) as contador, usuario.nombre");
       $this->db->from("asignacionticket");
       $this->db->join("mantenimiento",'mantenimiento.idticket=asignacionticket.idticket');
       $this->db->join("ticket",'ticket.idticket=asignacionticket.idticket');
       $this->db->join("usuario",'usuario.idusuario=asignacionticket.idusuario');
       $this->db->where("ticket.estado",1);
       $this->db->group_by("usuario.nombre");
       $result=$this->db->get()->result();
       echo json_encode($result);
   }

  }
?>
