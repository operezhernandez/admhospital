<?php
  class MInicio extends CI_Model
  {
    function __construct()
    {
      parent:: __construct();
    }

    public function years(){
      $this->db->reset_query();
      $this->db->select('YEAR(fechainicio) as year');
      $this->db->from('ticket');
      $this->db->group_by('year');
      $this->db->order_by('year','desc');
      $resultados = $this->db->get();
      return $resultados->result();
    }

    public function tickets($year){
      $this->db->reset_query();
      $this->db->select('MONTH(fechainicio)as mes, count(idticket) as tickets');
      $this->db->from('ticket');
      $this->db->where('fechainicio >=',$year.'-01-01');
      $this->db->where('fechainicio <=',$year.'-12-31');
      $this->db->where('estado',1);
      $this->db->group_by('mes');
      $this->db->order_by('mes');
      $resultados = $this->db->get();
      return $resultados->result();
    }

    public function countTickets($month,$year){
      $this->db->reset_query();
      $this->db->select('count(*) as nTickets');
      $this->db->from('ticket');
      $this->db->where('fechainicio >=',$year.'-'.$month.'-01');
      $this->db->where('fechainicio <',$year.'-'.($month+1).'-01');
      $this->db->where('estado',1);
      $nTickets = $this->db->get();
      return $nTickets->result();
    }

    public function countProceso($month,$year){
      $this->db->reset_query();
      $this->db->select('count(*) as nProceso');
      $this->db->from('ticket');
      $this->db->where('fechainicio >=',$year.'-'.$month.'-01');
      $this->db->where('fechainicio <',$year.'-'.($month+1).'-01');
      $this->db->where('estado',4);
      $nProceso = $this->db->get();
      return $nProceso->result();
    }

    public function countSolicitadas($month,$year){
      $this->db->reset_query();
      $this->db->select('count(*) as nSolicitada');
      $this->db->from('ticket');
      $this->db->where('fechainicio >=',$year.'-'.$month.'-01');
      $this->db->where('fechainicio <',$year.'-'.($month+1).'-01');
      $this->db->where('estado',2);
      $nSolicitada = $this->db->get();
      return $nSolicitada->result();
    }

    public function Inventario(){
      $this->db->reset_query();
      $this->db->select('count(*) as nInventario');
      $this->db->from('inventario');
      $nInv = $this->db->get();
      return $nInv->result();
    }

  }
?>
