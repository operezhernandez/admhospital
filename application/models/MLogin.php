<?php
  class Mlogin extends CI_Model
  {
    function __construct()
    {
      parent:: __construct();
    }

    public function ingresar($username,$pass){
      $this->db->select('usuario.idUsuario, usuario.nombre, usuario.username, usuario.idtipouser, usuario.foto, sede.idsede area');
      $this->db->from('usuario');
      $this->db->join('sede','sede.idsede=usuario.idarea');
      $this->db->where('username',$username);
      $this->db->where('password',$pass);

      $resultado=$this->db->get();

      if ($resultado->num_rows()==1) {
        $r=$resultado->row();

        $s_usuario=array(
          's_idusuario'=>$r->idUsuario,
          's_usuario'=>$r->nombre,
          's_tipoUser'=>$r->idtipouser,
          's_area'=>$r->area,
          's_foto'=>$r->foto
        );

        $this->session->set_userdata($s_usuario);
        return 1;
      }
      else{
        return array(
            'error'=>true,
            'mensaje'=>'Credenciales incorrectas, intentelo de nuevo.'
        );
      }
    }
  }
?>
