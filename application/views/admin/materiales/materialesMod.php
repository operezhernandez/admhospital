<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Modificar</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li class="active"></li>
                </ol>
            </div>
        </div>
    </div>
    
</div>

    <div class="content mt-12">
        <div class="card">
            <div class="card-header">
                <strong>Información</strong>
                <?php
                            if(isset($_SESSION["insert"]))
                            {
                                if($_SESSION["insert"]==true)
                            {
                                ?>
                                <div class="alert alert-success alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <strong><?php echo $_SESSION["mensaje"]; ?></strong> Actualizado con exito.
                                </div>
                                <?php
                            }
                            }
                          ?>
            </div>
            <div class="card-body card-block">
                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="descrip" class=" form-control-label">Nombre</label>
                    </div>
                    <div class="col-12 col-md-9">
                        <form action="<?php echo base_url(); ?>index.php/CMateriales/modificarNombre" method="POST">
                            <input type="hidden" name="id" value="<?php echo $inventario->idinventario; ?>">
                            <div class="input-group">
                            <input type="text" value="<?php echo $inventario->nombre; ?>" id="nombre" name="nombre" placeholder="Nombre" required class="form-control">
                            <input type="submit" class="btn btn-primary">
                                    
                            </div>
                        </form>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="descrip" class=" form-control-label">Cantidad</label>
                    </div>
                    <div class="col-12 col-md-9">
                        <form action="<?php echo base_url(); ?>index.php/CMateriales/modificarCantidad" method="POST">
                            <input type="hidden" name="id" value="<?php echo $inventario->idinventario; ?>">
                            <div class="input-group">
                            <input type="text" value="<?php echo $inventario->cantidad; ?>" id="cantidad" name="cantidad" placeholder="Cantidad" required class="form-control">
                            <input type="submit" class="btn btn-primary">
                                    
                            </div>
                        </form>
                    </div>
                </div>
                <div class="row form-group">
                        <div class="col col-md-3">
                            <label for="descrip" class=" form-control-label">Descripcion</label>
                        </div>
                        <div class="col-12 col-md-9">
                            <form action="<?php echo base_url(); ?>index.php/CMateriales/modificarDescripcion" method="POST">
                                <input type="hidden" name="id" value="<?php echo $inventario->idinventario; ?>">
                                <div class="input-group">
                                <input type="text" value="<?php echo $inventario->descripcion; ?>" id="descripcion" name="descripcion" placeholder="Descripcion" required class="form-control">
                                <input type="submit" class="btn btn-primary">
                                        
                                </div>
                            </form>
                        </div>
                    </div>
                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="descrip" class=" form-control-label">Precio</label>
                    </div>
                    <div class="col-12 col-md-9">
                        <form action="<?php echo base_url(); ?>index.php/CMateriales/modificarPrecio" method="POST">
                            <input type="hidden" name="id" value="<?php echo $inventario->idinventario; ?>">
                            <div class="input-group">
                            <input type="number" min="0" value="<?php echo $inventario->precio; ?>" id="precio" name="precio" placeholder="Precio" required class="form-control">
                            <input type="submit" class="btn btn-primary">
                                        
                           </div>
                        </form>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="descrip" class=" form-control-label">Proveedor</label>
                    </div>
                    <div class="col-12 col-md-9">
                        <form action="<?php echo base_url(); ?>index.php/CMateriales/modificarProveedor" method="POST">
                            <input type="hidden" name="id" value="<?php echo $inventario->idinventario; ?>">
                            <div class="input-group">
                            <input type="text" value="<?php echo $inventario->proveedor; ?>" id="proveedor" name="proveedor" placeholder="Proveedor" required class="form-control">
                            <input type="submit" class="btn btn-primary">
                                        
                           </div>
                        </form>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="descrip" class=" form-control-label">Sede</label>
                    </div>
                    <div class="col-12 col-md-9">
                        <form action="<?php echo base_url(); ?>index.php/CMateriales/modificarSede" method="POST">
                            <input type="hidden" name="id" value="<?php echo $inventario->idinventario; ?>">
                            <div class="input-group">
                            <input type="text" value="<?php echo $inventario->sede; ?>" id="sede" name="sede" placeholder="Sede" required class="form-control">
                            <input type="submit" class="btn btn-primary">
                                        
                           </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                
            </div>
        </div>
    </div>    