<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Equipo</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li class="active">Inventario</li>
                </ol>
            </div>
        </div>
    </div>
</div>

        <div class="content mt-12">
        <?php
   if(isset($mensaje))
   {
     ?>
    <div class='<?php
    if($error==false)
    {
       echo "alert alert-info alert-dismissible";
    }else{

     echo "alert alert-danger alert-dismissible";

    } ?>'>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-info"></i> Mensaje</h4>
                <?php echo $mensaje; ?>
    </div>
  <?php
   }
   ?>
        <div class="col-md-12 ">
            <ul class="nav nav-pills nav-fill" id="myTab" role="tablist">
                <li class="nav-item">
                  <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Listado</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Nuevo</a>
                </li>

              </ul>
              <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                        <br>
                        <table class="table table-striped table-bordered"  id="tablaInventario">
                             <thead>
                                 <tr>
                                 <th >ID</th>
                                 <th scope="col">Nombre</th>
                                 <th scope="col">Marca</th>
                                 <th scope="col">Serie</th>
                                 <th scope="col">Modelo</th>
                                <th scope="col">Tipo</th>
                                 <th scope="col">Clasificación</th>
                                 <th scope="col">Detalles</th>
                                 <th scope="col">Control</th>
                                 </tr>
                             </thead>
                             <tbody>
                             <?php
                             if(isset($equipos))
                             {
                                 foreach($equipos as $equipo)
                                 {

                                     echo '<tr>';
                                     echo '<td >'.$equipo->idequipo.'</td>';
                                     echo '<td>'.$equipo->nombre.'</td>';
                                     $newtext = wordwrap($equipo->marca, 15, "<br>",true);
                                     echo '<td >'.$equipo->marca.'</td>';
                                     echo '<td>'.$equipo->serie.'</td>';
                                     echo '<td>'.$equipo->modelo.'</td>';

                                    echo '<td>'.$equipo->tipoequipo.'</td>';
                                    echo '<td>'.$equipo->clasificacion.'</td>';
                                    echo '<td><a  rel="noopener noreferrer"
                                            href="'.base_url().'index.php/CInventario/detallesEquipos/'.$equipo->idequipo.'" class="btn btn-primary btn-sm" >Ver</a></td>';
                                    echo '<td><a  rel="noopener noreferrer"
                                            href="'.base_url().'index.php/CInventario/modificarEquipo/'.$equipo->idequipo.'" class="btn btn-primary btn-sm" >Modificar</a></td>';

                                     echo '</tr>';
                                 }
                             }

                         ?>




                             </tbody>
                         </table>

                </div>
                <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header">
                          <strong>Información</strong>
                        </div>
                        <div class="card-body card-block">
                          <form action="<?php echo base_url(); ?>index.php/CInventario/guardarEquipo" method="post" enctype="multipart/form-data" class="form-vertical">

                            <div class="row form-group">
                                <div class="col col-md-3">
                                  <label for="nombre" class=" form-control-label">Nombre</label>
                                </div>
                              <div class="col-12 col-md-9">
                                  <input type="text" id="nombre"
                                  name="nombre" placeholder="Nombre de Equipo"  class="form-control">
                                  <small class="form-text text-muted">Nombre de Equipo</small></div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3">
                                  <label for="marca" class=" form-control-label">Marca</label>
                                </div>
                                <div class="col-12 col-md-9">
                                  <input type="text" id="marca"
                                  name="marca" placeholder="Marca"  class="form-control">
                                  <small class="help-block form-text">Marca</small>
                                </div>
                            </div>
                            <div class="row form-group">
                                    <div class="col col-md-3"><label for="tipo" class=" form-control-label">Tipo de equipo</label></div>
                                    <div class="col-12 col-md-9">
                                    <select class="form-control" name="tipo" id="tipo">
                                      <?php
                                        foreach($tipo as $ser)
                                        {
                                          echo '<option value="'.$ser->idtequipo.'">'.$ser->nombre.'</option>';
                                        }
                                      ?>
                                    </select>
                                    </div>
                            </div>
                            <div class="row form-group">
                                    <div class="col col-md-3">
                                      <label for="serie" class=" form-control-label">Serie</label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                      <input type="text" id="serie"
                                      name="serie" placeholder="Serie"  class="form-control">

                                    </div>
                            </div>
                            <div class="row form-group">
                                    <div class="col col-md-3">
                                      <label for="modelo" class=" form-control-label">Modelo</label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                      <input type="text" id="modelo"
                                      name="modelo" placeholder="Modelo"  class="form-control">

                                    </div>
                            </div>
                            <!--<div class="row form-group">
                                    <div class="col col-md-3">
                                      <label for="sede" class=" form-control-label">Sede</label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                      <input type="text" id="sede"
                                      name="sede" placeholder="Sede"  class="form-control">

                                    </div>
                            </div>-->
                            <div class="row form-group">
                                    <div class="col col-md-3">
                                      <label for="clasificacion" class=" form-control-label">Clasificación</label>
                                    </div>
                                    <div class="col-12 col-md-9">

                                    <select name="clasificacion" class="form-control" id="clasificacion">
                                      <option >Clase 1</option>
                                      <option >Clase 2</option>
                                      <option >Clase 3</option>
                                    </select>
                                    </div>
                            </div>
                            <div class="row form-group">
                                    <div class="col col-md-3">
                                      <label for="clasificacion" class=" form-control-label">Clasificación 2</label>
                                    </div>
                                    <div class="col-12 col-md-9">

                                    <select name="clasificacion2" class="form-control" id="clasificacion2">
                                      <option >B</option>
                                      <option >BF</option>
                                      <option >CF</option>
                                    </select>
                                    </div>
                            </div>
                            <div class="row form-group">
                                    <div class="col col-md-3">
                                      <label for="fabricante" class=" form-control-label">Fabricante</label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                      <input type="text" id="fabricante"
                                      name="fabricante" placeholder="Fabricante"  class="form-control">

                                    </div>
                            </div>
                            <div class="row form-group">
                                    <div class="col col-md-3">
                                      <label for="aniofabricacion" class=" form-control-label">Fecha Fabricación</label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                      <input type="text" id="aniofabricacion"
                                      name="aniofabricacion" data-toggle="datepicker"  class="form-control"
                                      placeholder="YYYY-MM-DD">

                                    </div>
                            </div>
                            <div class="row form-group">
                                    <div class="col col-md-3">
                                      <label for="servicio" class=" form-control-label">Servicio</label>
                                    </div>
                                    <div class="col-12 col-md-9">

                                    <select class="form-control" name="servicio" id="servicio">
                                      <?php
                                        foreach($servicios as $ser)
                                        {
                                          echo '<option value="'.$ser->idservicio.'">'.$ser->nombre.'</option>';
                                        }
                                      ?>
                                    </select>
                                    </div>
                            </div>

                            <div class="row form-group">
                                    <div class="col col-md-3">
                                      <label for="tiempomantprev" class=" form-control-label">Tiempo Mantenimiento Preventivo</label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                      <input type="number" id="tiempomantprev"
                                      name="tiempomantprev" placeholder="Tiempo"  class="form-control">
                                        <small>En días</small>
                                    </div>
                            </div>
                            <div class="row form-group">
                                    <div class="col col-md-3">
                                      <label for="formaadquisicion" class=" form-control-label">Forma Adquisicion</label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                      <select id="formaadquisicion"
                                      name="formaadquisicion" class="form-control">
                                      <option >Compra</option>
                                      <option >Comodato</option>
                                      <option >Prestamo</option>
                                    </select>
                                    </div>
                            </div>
                            <div class="row form-group">
                                    <div class="col col-md-3">
                                      <label for="fechaadquisicion" class=" form-control-label">Fecha Adquisicion</label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                      <input type="text" id="fechaadquisicion"
                                      name="fechaadquisicion"  placeholder="YYYY-MM-DD" class="form-control">

                                    </div>
                            </div>
                            <div class="row form-group">
                                    <div class="col col-md-3">
                                      <label for="precioadquisicion" class=" form-control-label">Precio</label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                      <input type="text" id="precioadquisicion"
                                      name="precioadquisicion"  placeholder="Precio Adquisicion"  class="form-control">

                                    </div>
                            </div>
                            <div class="row form-group">
                                    <div class="col col-md-3">
                                      <label for="proveedoradquisicion" class=" form-control-label">Proveedor</label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                      <input type="text" id="proveedoradquisicion"
                                      name="proveedoradquisicion"  placeholder="Proveedor Adquisicion"  class="form-control">

                                    </div>
                            </div>
                            <div class="row form-group">
                                    <div class="col col-md-3">
                                      <label for="numfactad" class=" form-control-label">Numero Factura</label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                      <input type="text" id="numfactad"
                                      name="numfactad"  placeholder="Numero Factura"  class="form-control">

                                    </div>
                            </div>
                            <div class="row form-group">
                                    <div class="col col-md-3">
                                      <label for="inigarantia" class=" form-control-label">Inicio Garantia</label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                      <input type="text" id="inigarantia"
                                      name="inigarantia"  placeholder="YYYY-MM-DD"  class="form-control">

                                    </div>
                            </div>
                            <div class="row form-group">
                                    <div class="col col-md-3">
                                      <label for="fingarantia" class=" form-control-label">Fin Garantia</label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                      <input type="text" id="fingarantia"
                                      name="fingarantia"  placeholder="YYYY-MM-DD"  class="form-control">

                                    </div>
                            </div>
                            <div class="row form-group">
                                    <div class="col col-md-3">
                                      <label for="amperaje" class=" form-control-label">Amperaje</label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                      <input type="text" id="amperaje"
                                      name="amperaje"  placeholder="Amperaje"  class="form-control">

                                    </div>
                            </div>
                            <div class="row form-group">
                                    <div class="col col-md-3">
                                      <label for="frecuencia" class=" form-control-label">Frecuencia</label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                      <input type="text" id="frecuencia"
                                      name="frecuencia"  placeholder="Frecuencia"  class="form-control">

                                    </div>
                            </div>
                            <div class="row form-group">
                                    <div class="col col-md-3">
                                      <label for="potencia" class=" form-control-label">Potencia</label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                      <input type="text" id="potencia"
                                      name="potencia"  placeholder="Potencia"  class="form-control">

                                    </div>
                            </div>
                            <div class="row form-group">
                                    <div class="col col-md-3">
                                      <label for="voltaje" class=" form-control-label">Voltaje</label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                      <input type="text" id="voltaje"
                                      name="voltaje"  placeholder="Voltaje"  class="form-control">

                                    </div>
                            </div>
                            <div class="row form-group">
                                    <div class="col col-md-3">
                                      <label for="preinstalacion" class=" form-control-label">Preinstalacion</label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                      <input type="text" id="preinstalacion"
                                      name="preinstalacion"  placeholder="Preinstalacion"  class="form-control">

                                    </div>
                            </div>

                        </div>
                        <div class="card-footer">
                          <button type="submit" class="btn btn-primary btn-sm">
                            <i class="fa fa-dot-circle-o"></i> Agregar
                          </button>
                          </form>
                        </div>
                    </div>
                </div>

                </div>
              </div>


    </div>
    </div>
       <!-- Button trigger modal -->


<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
