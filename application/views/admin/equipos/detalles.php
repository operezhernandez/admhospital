<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Equipo</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li class="active">Inventario</li>
                </ol>
            </div>
        </div>
    </div>
</div>

        <div class="content mt-12">
        <?php
   if(isset($mensaje))
   {
     ?>
    <div class='<?php 
    if($error==false)
    {
       echo "alert alert-info alert-dismissible";
    }else{
      
     echo "alert alert-danger alert-dismissible";
      
    } ?>'>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-info"></i> Mensaje</h4>
                <?php echo $mensaje; ?>
    </div>
  <?php
   }
   ?>
        <div class="col-md-12 ">
            <ul class="nav nav-pills nav-fill" id="myTab" role="tablist">
                <li class="nav-item">
                  <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Detalles</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Inventario</a>
                </li>
               
              </ul>
              <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                        <br> 
                        <!--<table class="table table-striped table-bordered"  id="tablaInventario">
                             <thead>
                                 <tr>
                                 <th >ID</th>
                                 <th scope="col">Nombre</th>
                                 <th scope="col">Marca</th>
                                 <th scope="col">Serie</th>
                                 <th scope="col"> Modelo</th>
                                <th scope="col">Tipo</th>
                                 <th scope="col">Clasificación</th>
                                 <th scope="col">Detalles</th>
                                 <th scope="col">Controles</th>
                                 </tr>
                             </thead>
                             <tbody>
                             <?php
                            /* if(isset($equipos))
                             {   
                                 foreach($equipos as $equipo)
                                 {
                                     
                                     if($equipo->tipoequipo==0)
                                     {
                                         $val='Medico';
                                     }else
                                     {
                                         $val='No Medico';
                                     }

                                     echo '<tr>';
                                     echo '<td >'.$equipo->idequipo.'</td>';
                                     echo '<td>'.$equipo->nombre.'</td>';
                                     $newtext = wordwrap($equipo->marca, 15, "<br>",true);
                                     echo '<td >'.$equipo->marca.'</td>';
                                     echo '<td>'.$equipo->serie.'</td>';
                                     echo '<td>'.$equipo->modelo.'</td>';
                                     
                                    echo '<td>'.$val.'</td>';
                                     echo '<td>'.$equipo->clasificacion.'</td>';
                                     echo '<td><a target="_blank" rel="noopener noreferrer" 
                                            href="'.base_url().'index.php/CInventario/detallesEquipos/'.$equipo->idequipo.'" class="btn btn-primary btn-sm" >Ver</a></td>';
                                     echo '<td>'.'<a href="#" class="btn btn-primary btn-sm" >Detalles</a><hr>
                                                  <a href="#" class="btn btn-primary btn-sm" >Modificar</a>'.'</td>';
                                     echo '</tr>';
                                 }
                             }*/

                         ?>
                                 
                                 
                                 
                                 
                             </tbody>
                         </table>-->
                            <div class="col-md-6">
                                <div class="card">
                                    <div class="card-header">
                                    <strong>Información</strong>
                                    </div>
                                    <div class="card-body card-block">
                                    <form action="<?php echo base_url(); ?>index.php/CInventario/guardarEquipo" method="post" enctype="multipart/form-data" class="form-vertical">
                                        
                                        <div class="row form-group">
                                            <div class="col col-md-3">
                                            <label for="nombre" class=" form-control-label">Nombre</label>
                                            </div>
                                        <div class="col-12 col-md-9">
                                            <input type="text" id="nombre" 
                                            name="nombre" placeholder="Nombre de Equipo" value="<?php echo $detalle->nombre ?>"  disabled class="form-control">
                                            <small class="form-text text-muted">Nombre de Equipo</small></div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col col-md-3">
                                            <label for="marca" class=" form-control-label">Marca</label>
                                            </div>
                                            <div class="col-12 col-md-9">
                                            <input type="text" id="marca" 
                                            name="marca" placeholder="Marca" value="<?php echo $detalle->marca ?>"  disabled class="form-control">
                                            <small class="help-block form-text">Marca</small>
                                            </div>
                                        </div>
                                        <?php
                                            if($detalle->tipoequipo==1)
                                            {
                                                $var="No Medico";
                                            }else
                                            {
                                                $var ="Medico";
                                            }

                                        ?>
                                        <div class="row form-group">
                                                <div class="col col-md-3"><label for="tipo"  class=" form-control-label">Tipo de equipo</label></div>
                                                <div class="col-12 col-md-9">
                                                <select name="tipo" id="tipo" disabled class="form-control">
                                                    <option> <?php echo $var; ?> </option>

                                                </select>
                                                </div>
                                        </div>
                                        <div class="row form-group">
                                                <div class="col col-md-3">
                                                <label for="serie" class=" form-control-label">Serie</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                <input type="text" id="serie" 
                                                name="serie" placeholder="Serie" value="<?php echo $detalle->serie ?>" disabled class="form-control">
                                                
                                                </div>
                                        </div>
                                        <div class="row form-group">
                                                <div class="col col-md-3">
                                                <label for="modelo" class=" form-control-label">Modelo</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                <input type="text" id="modelo" 
                                                name="modelo" placeholder="Modelo" value="<?php echo $detalle->modelo ?>" disabled class="form-control">
                                            
                                                </div>
                                        </div>
                                        <!--<div class="row form-group">
                                                <div class="col col-md-3">
                                                <label for="sede" class=" form-control-label">Sede</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                <input type="text" id="sede" 
                                                name="sede" placeholder="Sede"  class="form-control">
                                            
                                                </div>
                                        </div>-->
                                        <div class="row form-group">
                                                <div class="col col-md-3">
                                                <label for="clasificacion" class=" form-control-label">Clasificación</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                <input type="text" id="clasificacion" 
                                                name="clasificacion" placeholder="Clasificacion" value="<?php echo $detalle->clasificacion ?>" disabled class="form-control">
                                            
                                                </div>
                                        </div>
                                        <div class="row form-group">
                                                <div class="col col-md-3">
                                                <label for="fabricante" class=" form-control-label">Fabricante</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                <input type="text" id="fabricante" 
                                                name="fabricante" placeholder="Fabricante" value="<?php echo $detalle->fabricante ?>" disabled class="form-control">
                                            
                                                </div>
                                        </div>
                                        <div class="row form-group">
                                                <div class="col col-md-3">
                                                <label for="aniofabricacion" class=" form-control-label">Fecha Fabricación</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                <input type="date" id="aniofabricacion" 
                                                name="aniofabricacion" disabled value="<?php echo $detalle->aniofabri ?>"  class="form-control">
                                            
                                                </div>
                                        </div>
                                        <div class="row form-group">
                                                <div class="col col-md-3">
                                                <label for="servicio" class=" form-control-label">Servicio</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                <input type="text" id="servicio" 
                                                name="servicio" value="<?php echo $detalle->servicio ?>" placeholder="Servicio" disabled class="form-control">
                                            
                                                </div>
                                        </div>
                                        
                                        <div class="row form-group">
                                                <div class="col col-md-3">
                                                <label for="tiempomantprev" class=" form-control-label">Tiempo Mantenimiento Preventivo</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                <input type="text" id="tiempomantprev" 
                                                name="tiempomantprev" value="Cada <?php echo $detalle->tiempomantprev ?> días"  placeholder="Tiempo" disabled class="form-control">
                                                    <small>En días</small>
                                                </div>
                                        </div>
                                        <div class="row form-group">
                                                <div class="col col-md-3">
                                                <label for="formaadquisicion" class=" form-control-label">Forma Adquisicion</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                <input type="text" id="formaadquisicion" 
                                                name="formaadquisicion" value="<?php echo $detalle->formaadquisicion ?>"  placeholder="Forma Adquisicion" disabled class="form-control">
                                            
                                                </div>
                                        </div>
                                        <div class="row form-group">
                                                <div class="col col-md-3">
                                                <label for="fechaadquisicion" class=" form-control-label">Fecha Adquisicion</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                <input type="date" id="fechaadquisicion" 
                                                name="fechaadquisicion" value="<?php echo $detalle->fecha ?>" placeholder="Fecha Adquisicion" disabled class="form-control">
                                            
                                                </div>
                                        </div>
                                        <div class="row form-group">
                                                <div class="col col-md-3">
                                                <label for="precioadquisicion" class=" form-control-label">Precio</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                <input type="text" id="precioadquisicion" 
                                                name="precioadquisicion" value="<?php echo $detalle->precio ?>"  placeholder="Precio Adquisicion" disabled class="form-control">
                                            
                                                </div>
                                        </div>
                                        <div class="row form-group">
                                                <div class="col col-md-3">
                                                <label for="proveedoradquisicion" class=" form-control-label">Proveedor</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                <input type="text" id="proveedoradquisicion" 
                                                name="proveedoradquisicion" value="<?php echo $detalle->proveedor ?>"  placeholder="Proveedor Adquisicion" disabled class="form-control">
                                            
                                                </div>
                                        </div>
                                        <div class="row form-group">
                                                <div class="col col-md-3">
                                                <label for="numfactad" class=" form-control-label">Numero Factura</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                <input type="text" id="numfactad" 
                                                name="numfactad"  value="<?php echo $detalle->numfactura ?>" placeholder="Numero Factura" disabled class="form-control">
                                            
                                                </div>
                                        </div>
                                        <div class="row form-group">
                                                <div class="col col-md-3">
                                                <label for="inigarantia" class=" form-control-label">Inicio Garantia</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                <input type="date" id="inigarantia" 
                                                name="inigarantia" value="<?php echo $detalle->iniciogarantia ?>"  placeholder="Inicio Garantia" disabled class="form-control">
                                            
                                                </div>
                                        </div>
                                        <div class="row form-group">
                                                <div class="col col-md-3">
                                                <label for="fingarantia" class=" form-control-label">Fin Garantia</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                <input type="date" id="fingarantia" 
                                                name="fingarantia" value="<?php echo $detalle->fingarantia ?>"  placeholder="Fin Garantia" disabled class="form-control">
                                            
                                                </div>
                                        </div>
                                        <div class="row form-group">
                                                <div class="col col-md-3">
                                                <label for="amperaje" class=" form-control-label">Amperaje</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                <input type="text" id="amperaje" 
                                                name="amperaje" value="<?php echo $detalle->amperaje ?>"  placeholder="Amperaje" disabled class="form-control">
                                            
                                                </div>
                                        </div>
                                        <div class="row form-group">
                                                <div class="col col-md-3">
                                                <label for="frecuencia" class=" form-control-label">Frecuencia</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                <input type="text" id="frecuencia" 
                                                name="frecuencia" value="<?php echo $detalle->frecuencia ?>" placeholder="Frecuencia" disabled class="form-control">
                                            
                                                </div>
                                        </div>
                                        <div class="row form-group">
                                                <div class="col col-md-3">
                                                <label for="potencia" class=" form-control-label">Potencia</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                <input type="text" id="potencia" 
                                                name="potencia" value="<?php echo $detalle->potencia ?>" placeholder="Potencia" disabled class="form-control">
                                            
                                                </div>
                                        </div>
                                        <div class="row form-group">
                                                <div class="col col-md-3">
                                                <label for="voltaje" class=" form-control-label">Voltaje</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                <input type="text" id="voltaje" 
                                                name="voltaje" value="<?php echo $detalle->voltaje ?>" placeholder="Voltaje" disabled class="form-control">
                                            
                                                </div>
                                        </div>
                                        <div class="row form-group">
                                                <div class="col col-md-3">
                                                <label for="preinstalacion" class=" form-control-label">Preinstalacion</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                <input type="text" id="preinstalacion" 
                                                name="preinstalacion" value="<?php echo $detalle->preinstalacion ?>" placeholder="Preinstalacion" disabled class="form-control">
                                            
                                                </div>
                                        </div>
                                    
                                    </div>
                                    <div class="card-footer">
                                
                                    </form>
                                    </div>
                                </div>
                            </div>
                        <div class="col-md-6">
                            <div class="card">
                                <div class="card-header">
                                    <strong>Historial de ordenes de trabajo</strong>
                                </div>
                                <div class="card-body card-block">
                                    <ul class="list-group">
                                        <?php 
                                        if(isset($tickets))
                                        {
                                            foreach($tickets as $tic){
                                            ?>
                                            <li class="list-group-item">
                                                <p></p>
                                                <p>Orden de trabajo: <strong><?php echo $tic->idticket ?></strong></p>
                                                <p>Descripción: <strong><?php echo $tic->descripcion ?></strong></p>
                                                <p>Fecha Incio: <strong><?php echo $tic->fechainicio ?></strong></p>
                                                <p>Estado: <strong><?php switch($tic->estado){  case 0:echo "Pendiente"; break;
                                                                            case 1: echo "Finalizado"; break; 
                                                                            case 2: echo "Solicitado"; break;
                                                                            case 3: echo "Cancelar"; break;
                                                                            case 4: echo "En Proceso"; break; } ?> </strong></p>
                                                <p>Tipo: <strong><?php echo $tic->tipomantenimiento ?></strong></p>
                                                <p>Informe Tecnico: <strong><?php echo $tic->informetecnico ?></strong></p>
                                            </li>
                                            <?php
                                            }
                                        }
                                        ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                </div>
                <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                <div class="row">
                    <div class="col-3">
                    <hr>
                        <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                        <a class="nav-link active" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">Listado</a>
                        <a class="nav-link" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-selected="false">Agregar</a>
                        </div>
                    </div>
                    <div class="col-9">
                        <hr>
                        <div class="tab-content" id="v-pills-tabContent">
                        <div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
                            <table  class="table table-striped table-bordered" >
                                <thead>
                                    <tr>
                                        <th >ID</th>
                                        <th scope="col">Area</th>
                                        <th>Controles</th>
                                     
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if(isset($inventario))
                                    {
                                        foreach($inventario as $pro)
                                        {
                                            if($pro->habilitado==1)
                                            {
                                                $val='<a href="'.base_url().'CInventario/deshabilitar/'.$pro->idinvequipo.'" class="btn btn-danger btn-sm">Deshabilitar</a>';
                                            }else
                                            {
                                                $val='<a href="'.base_url().'CInventario/habilitar/'.$pro->idinvequipo.'" class="btn btn-primary btn-sm">Habilitar</a>';
                                            }
                                            echo '<tr>';
                                            echo '<td>'.$pro->idinvequipo.'</td>';
                                            echo '<td>'.$pro->sede.'</td>';
                                            echo '<td>'.$val.'</td>';
                                            echo '</tr>';
                                        }
                                    }
                                    ?>
                                </tbody>    
                            </table>
                        </div>
                        <div class="tab-pane fade" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">
                            <form action="<?php echo base_url(); ?>index.php/CInventario/guardarInventarioEquipo" method="post" enctype="multipart/form-data" class="form-vertical">
                                
                                <div class="row form-group">
                                    <div class="col col-md-3">
                                    <label for="sede" class=" form-control-label">Area</label>
                                    </div>
                                <div class="col-12 col-md-9">
                                  
                                    <select name="sede" class="form-control">
                                        <?php
                                            foreach($sedes as $sede)
                                            {
                                                ?>
                                               <option value="<?php echo $sede->idsede ?>"><?php echo $sede->sede ?></option>     
                                            <?php
                                            }
                                        ?>
                                    </select>
                                    <input type="hidden" name="idequipo" value="<?php echo $detalle->idequipo ?>">
                                </div>
                                </div>
                                
                                    <button type="submit" class="btn btn-primary btn-sm">
                                        <i class="fa fa-dot-circle-o"></i> Agregar
                                    </button>
                                
                            </form>

                        </div>
                        </div>
                    </div>
                </div>

                </div>
              </div>
                  
                    
    </div>
    </div>
       <!-- Button trigger modal -->


<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
