<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Equipo</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li class="active">Inventario</li>
                </ol>
            </div>
        </div>
    </div>
</div>

        <div class="content mt-12">
        <?php
   if(isset($mensaje))
   {
     ?>
    <div class='<?php 
    if($error==false)
    {
       echo "alert alert-info alert-dismissible";
    }else{
      
     echo "alert alert-danger alert-dismissible";
      
    } ?>'>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-info"></i> Actualizacion de</h4>
                <?php echo $mensaje; ?>
    </div>
  <?php
   }
   ?>


   <div class="col-md-6">
                                <div class="card">
                                    <div class="card-header">
                                    <strong>Información</strong>
                                    </div>
                                    <div class="card-body card-block">
                                     
                                        <div class="row form-group">
                                            <div class="col col-md-3">
                                                <label for="descrip" class=" form-control-label">Nombre</label>
                                            </div>
                                            <div class="col-12 col-md-9">
                                                <form action="<?php echo base_url(); ?>index.php/CInventario/modificarCampo" method="POST">
                                                    <input type="hidden" name="id" value="<?php echo $detalle->idequipo; ?>">
                                                    <input type="hidden" value ="idequipo" name="llave">
                                                    <input type="hidden" value="equipo" name="tabla">
                                                    <input type="hidden" value="nombre" name="campo">
                                                    <input type="hidden" value="Nombre" name="mensaje">
                                                    <div class="input-group">
                                                    <input type="text" value="<?php echo $detalle->nombre; ?>" id="valor" name="valor" placeholder="Nombre" required class="form-control">
                                                    <input type="submit" class="btn btn-primary">
                                                                
                                                </div>
                                                </form>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col col-md-3">
                                            <label for="marca" class=" form-control-label">Marca</label>
                                            </div>
                                            <div class="col-12 col-md-9">
                                                <form action="<?php echo base_url(); ?>index.php/CInventario/modificarCampo" method="POST">
                                                    <input type="hidden" name="id" value="<?php echo $detalle->idequipo; ?>">
                                                    <input type="hidden" value ="idequipo" name="llave">
                                                    <input type="hidden" value="equipo" name="tabla">
                                                    <input type="hidden" value="marca" name="campo">
                                                    <input type="hidden" value="Marca" name="mensaje">
                                                    <div class="input-group">
                                                    <input type="text" value="<?php echo $detalle->marca; ?>" id="valor" name="valor" placeholder="Nombre" required class="form-control">
                                                    <input type="submit" class="btn btn-primary">
                                                                
                                                </div>
                                                </form>
                                            </div>
                                        </div>
                                        <?php
                                            if($detalle->tipoequipo==1)
                                            {
                                                $var="No Medico";
                                            }else
                                            {
                                                $var ="Medico";
                                            }

                                        ?>
                                        <div class="row form-group">
                                                <div class="col col-md-3"><label for="tipo"  class=" form-control-label">Tipo de equipo</label></div>
                                               
                                                <div class="col-12 col-md-9">
                                                <form action="<?php echo base_url(); ?>index.php/CInventario/modificarCampo" method="POST">
                                                    <input type="hidden" name="id" value="<?php echo $detalle->idequipo; ?>">
                                                    <input type="hidden" value ="idequipo" name="llave">
                                                    <input type="hidden" value="equipo" name="tabla">
                                                    <input type="hidden" value="tipoequipo" name="campo">
                                                    <input type="hidden" value="Tipo Equipo" name="mensaje">
                                                    <div class="input-group">
                                                    <select id="valor" name="valor" required class="form-control">
                                                    <?php 
                                                        if(isset($tipos))
                                                        {
                                                            foreach($tipos as $tipo){
                                                                ?>
                                                                <option value="<?php echo $tipo->idtequipo ?>"><?php echo $tipo->nombre; ?></option>
                                                                <?php
                                                            }
                                                        }
                                                    ?>
                                                    </select>
                                                    <input type="submit" class="btn btn-primary">
                                                                
                                                </div>
                                                </form>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                                <div class="col col-md-3">
                                                <label for="serie" class=" form-control-label">Serie</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                    <form action="<?php echo base_url(); ?>index.php/CInventario/modificarCampo" method="POST">
                                                        <input type="hidden" name="id" value="<?php echo $detalle->idequipo; ?>">
                                                        <input type="hidden" value ="idequipo" name="llave">
                                                        <input type="hidden" value="equipo" name="tabla">
                                                        <input type="hidden" value="serie" name="campo">
                                                        <input type="hidden" value="Serie" name="mensaje">
                                                        <div class="input-group">
                                                        <input type="text" value="<?php echo $detalle->serie; ?>" id="valor" name="valor" placeholder="Nombre" required class="form-control">
                                                        <input type="submit" class="btn btn-primary">
                                                                    
                                                    </div>
                                                    </form>
                                                </div> 
                                        </div>
                                        <div class="row form-group">
                                                <div class="col col-md-3">
                                                <label for="modelo" class=" form-control-label">Modelo</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                    <form action="<?php echo base_url(); ?>index.php/CInventario/modificarCampo" method="POST">
                                                        <input type="hidden" name="id" value="<?php echo $detalle->idequipo; ?>">
                                                        <input type="hidden" value ="idequipo" name="llave">
                                                        <input type="hidden" value="equipo" name="tabla">
                                                        <input type="hidden" value="modelo" name="campo">
                                                        <input type="hidden" value="Modelo" name="mensaje">
                                                        <div class="input-group">
                                                        <input type="text" value="<?php echo $detalle->modelo; ?>" id="valor" name="valor" placeholder="Nombre" required class="form-control">
                                                        <input type="submit" class="btn btn-primary">
                                                                    
                                                    </div>
                                                    </form>
                                                </div> 
                                        </div>
                                        <!--<div class="row form-group">
                                                <div class="col col-md-3">
                                                <label for="sede" class=" form-control-label">Sede</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                <input type="text" id="sede" 
                                                name="sede" placeholder="Sede"  class="form-control">
                                            
                                                </div>
                                        </div>-->
                                        <div class="row form-group">
                                                <div class="col col-md-3">
                                                <label for="clasificacion" class=" form-control-label">Clasificación</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                <form action="<?php echo base_url(); ?>index.php/CInventario/modificarCampo" method="POST">
                                                    <input type="hidden" name="id" value="<?php echo $detalle->idequipo; ?>">
                                                    <input type="hidden" value ="idequipo" name="llave">
                                                    <input type="hidden" value="equipo" name="tabla">
                                                    <input type="hidden" value="clasificacion" name="campo">
                                                    <input type="hidden" value="Clasificacion" name="mensaje">
                                                    <div class="input-group">
                                                    <select id="valor" name="valor" required class="form-control">
                                                        <option >Clase 1</option>
                                                        <option >Clase 2</option>
                                                        <option >Clase 3</option>
                                                    </select>
                                                    <input type="submit" class="btn btn-primary">
                                                                
                                                </div>
                                                </form>
                                                </div>
                                            </div>
                                        <div class="row form-group">
                                                <div class="col col-md-3">
                                                <label for="fabricante" class=" form-control-label">Fabricante</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                    <form action="<?php echo base_url(); ?>index.php/CInventario/modificarCampo" method="POST">
                                                        <input type="hidden" name="id" value="<?php echo $detalle->idequipo; ?>">
                                                        <input type="hidden" value ="idequipo" name="llave">
                                                        <input type="hidden" value="equipo" name="tabla">
                                                        <input type="hidden" value="fabricante" name="campo">
                                                        <input type="hidden" value="Fabricante" name="mensaje">
                                                        <div class="input-group">
                                                        <input type="text" value="<?php echo $detalle->fabricante; ?>" id="valor" name="valor" placeholder="Nombre" required class="form-control">
                                                        <input type="submit" class="btn btn-primary">         
                                                    </div>
                                                    </form>
                                                </div>
                                        </div>
                                        <div class="row form-group">
                                                <div class="col col-md-3">
                                                <label for="aniofabricacion" class=" form-control-label">Fecha Fabricación</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                    <form action="<?php echo base_url(); ?>index.php/CInventario/modificarCampo" method="POST">
                                                        <input type="hidden" name="id" value="<?php echo $detalle->idequipo; ?>">
                                                        <input type="hidden" value ="idequipo" name="llave">
                                                        <input type="hidden" value="equipo" name="tabla">
                                                        <input type="hidden" value="aniofabri" name="campo">
                                                        <input type="hidden" value="Fecha Fabricacion" name="mensaje">
                                                        <div class="input-group">
                                                        <input type="text" value="<?php echo $detalle->aniofabri; ?>" id="valor" name="valor" placeholder="Nombre" required class="form-control">
                                                        <input type="submit" class="btn btn-primary">         
                                                    </div>
                                                    </form>
                                                </div>
                                        </div>
                                        <div class="row form-group">
                                                <div class="col col-md-3">
                                                <label for="servicio" class=" form-control-label">Servicio</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                <form action="<?php echo base_url(); ?>index.php/CInventario/modificarCampo" method="POST">
                                                    <input type="hidden" name="id" value="<?php echo $detalle->idequipo; ?>">
                                                    <input type="hidden" value ="idequipo" name="llave">
                                                    <input type="hidden" value="equipo" name="tabla">
                                                    <input type="hidden" value="servicio" name="campo">
                                                    <input type="hidden" value="Servicio" name="mensaje">
                                                    <div class="input-group">
                                                    <select id="valor" name="valor" required class="form-control">
                                                    <?php 
                                                        if(isset($servicios))
                                                        {
                                                            foreach($servicios as $ser){
                                                                ?>
                                                                <option value="<?php echo $ser->idservicio ?>"><?php echo $ser->nombre; ?></option>
                                                                <?php
                                                            }
                                                        }
                                                    ?>
                                                    </select>
                                                    <input type="submit" class="btn btn-primary">
                                                                
                                                </div>
                                                </form>
                                            </div>
                                        </div>
                                        
                                        <div class="row form-group">
                                                <div class="col col-md-3">
                                                <label for="tiempomantprev" class=" form-control-label">Tiempo Mantenimiento Preventivo</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                    <form action="<?php echo base_url(); ?>index.php/CInventario/modificarCampo" method="POST">
                                                        <input type="hidden" name="id" value="<?php echo $detalle->idequipo; ?>">
                                                        <input type="hidden" value ="idequipo" name="llave">
                                                        <input type="hidden" value="equipo" name="tabla">
                                                        <input type="hidden" value="tiempomantprev" name="campo">
                                                        <input type="hidden" value="Tiempo Mantenimiento Preventivo" name="mensaje">
                                                        <div class="input-group">
                                                        <input type="text" value="<?php echo $detalle->tiempomantprev; ?>" id="valor" name="valor" placeholder="Nombre" required class="form-control">
                                                        <input type="submit" class="btn btn-primary">   
                                                            
                                                    </div>
                                                    <small>En dias</small>  
                                                    </form>
                                                </div>
                                        </div>
                                        <div class="row form-group">
                                                <div class="col col-md-3">
                                                <label for="formaadquisicion" class=" form-control-label">Forma Adquisicion</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                    <form action="<?php echo base_url(); ?>index.php/CInventario/modificarCampo" method="POST">
                                                        <input type="hidden" name="id" value="<?php echo $detalle->idadquisicion; ?>">
                                                        <input type="hidden" value ="idadquisicion" name="llave">
                                                        <input type="hidden" value="formaadquisicion" name="tabla">
                                                        <input type="hidden" value="formaadquisicion" name="campo">
                                                        <input type="hidden" value="Forma Adquisicion" name="mensaje">
                                                        <div class="input-group">
                                                    <select id="valor" name="valor" required class="form-control">
                                                        <option >Compra</option>
                                                        <option >Comodato</option>
                                                        <option >Prestamo</option>
                                                    </select>
                                                    <input type="submit" class="btn btn-primary">
                                                                
                                                </div>
                                                    
                                                    </form>
                                                </div>
                                        </div>
                                        <div class="row form-group">
                                                <div class="col col-md-3">
                                                <label for="fechaadquisicion" class=" form-control-label">Fecha Adquisicion</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                    <form action="<?php echo base_url(); ?>index.php/CInventario/modificarCampo" method="POST">
                                                        <input type="hidden" name="id" value="<?php echo $detalle->idadquisicion; ?>">
                                                        <input type="hidden" value ="idadquisicion" name="llave">
                                                        <input type="hidden" value="formaadquisicion" name="tabla">
                                                        <input type="hidden" value="fecha" name="campo">
                                                        <input type="hidden" value="Fecha Adquisicion" name="mensaje">
                                                        <div class="input-group">
                                                        <input type="text" value="<?php echo $detalle->fecha; ?>" id="valor" name="valor" placeholder="Nombre" required class="form-control">
                                                       
                                                    <input type="submit" class="btn btn-primary">
                                                                
                                                </div>
                                                    
                                                    </form>
                                                </div>
                                        </div>
                                        <div class="row form-group">
                                                <div class="col col-md-3">
                                                <label for="precioadquisicion" class=" form-control-label">Precio</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                    <form action="<?php echo base_url(); ?>index.php/CInventario/modificarCampo" method="POST">
                                                        <input type="hidden" name="id" value="<?php echo $detalle->idadquisicion; ?>">
                                                        <input type="hidden" value ="idadquisicion" name="llave">
                                                        <input type="hidden" value="formaadquisicion" name="tabla">
                                                        <input type="hidden" value="precio" name="campo">
                                                        <input type="hidden" value="Precio" name="mensaje">
                                                        <div class="input-group">
                                                        <input type="text" value="<?php echo $detalle->precio; ?>" id="valor" name="valor" placeholder="Nombre" required class="form-control">
                                                       
                                                    <input type="submit" class="btn btn-primary">
                                                                
                                                </div>
                                                    
                                                    </form>
                                                </div>
                                        </div>
                                        <div class="row form-group">
                                                <div class="col col-md-3">
                                                <label for="proveedoradquisicion" class=" form-control-label">Proveedor</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                    <form action="<?php echo base_url(); ?>index.php/CInventario/modificarCampo" method="POST">
                                                        <input type="hidden" name="id" value="<?php echo $detalle->idadquisicion; ?>">
                                                        <input type="hidden" value ="idadquisicion" name="llave">
                                                        <input type="hidden" value="formaadquisicion" name="tabla">
                                                        <input type="hidden" value="proveedor" name="campo">
                                                        <input type="hidden" value="Proveedor" name="mensaje">
                                                        <div class="input-group">
                                                        <input type="text" value="<?php echo $detalle->proveedor; ?>" id="valor" name="valor" placeholder="Nombre" required class="form-control">
                                                       
                                                    <input type="submit" class="btn btn-primary">
                                                                
                                                </div>
                                                    
                                                    </form>
                                                </div>
                                        </div>
                                        <div class="row form-group">
                                                <div class="col col-md-3">
                                                <label for="numfactad" class=" form-control-label">Numero Factura</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                    <form action="<?php echo base_url(); ?>index.php/CInventario/modificarCampo" method="POST">
                                                        <input type="hidden" name="id" value="<?php echo $detalle->idadquisicion; ?>">
                                                        <input type="hidden" value ="idadquisicion" name="llave">
                                                        <input type="hidden" value="formaadquisicion" name="tabla">
                                                        <input type="hidden" value="numfactura" name="campo">
                                                        <input type="hidden" value="Numero Factura" name="mensaje">
                                                        <div class="input-group">
                                                        <input type="text" value="<?php echo $detalle->numfactura; ?>" id="valor" name="valor" placeholder="Nombre" required class="form-control">
                                                       
                                                    <input type="submit" class="btn btn-primary">
                                                                
                                                    </div>
                                                    
                                                    </form>
                                                </div>
                                        </div>
                                        <div class="row form-group">
                                                <div class="col col-md-3">
                                                <label for="inigarantia" class=" form-control-label">Inicio Garantia</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                    <form action="<?php echo base_url(); ?>index.php/CInventario/modificarCampo" method="POST">
                                                        <input type="hidden" name="id" value="<?php echo $detalle->idadquisicion; ?>">
                                                        <input type="hidden" value ="idadquisicion" name="llave">
                                                        <input type="hidden" value="formaadquisicion" name="tabla">
                                                        <input type="hidden" value="iniciogarantia" name="campo">
                                                        <input type="hidden" value="Inicio de garantia" name="mensaje">
                                                        <div class="input-group">
                                                        <input type="text" value="<?php echo $detalle->iniciogarantia; ?>" id="valor" name="valor" placeholder="Nombre" required class="form-control">
                                                       
                                                    <input type="submit" class="btn btn-primary">
                                                                
                                                    </div>
                                                    
                                                    </form>
                                                </div>
                                        </div>
                                        <div class="row form-group">
                                                <div class="col col-md-3">
                                                <label for="fingarantia" class=" form-control-label">Fin Garantia</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                    <form action="<?php echo base_url(); ?>index.php/CInventario/modificarCampo" method="POST">
                                                        <input type="hidden" name="id" value="<?php echo $detalle->idadquisicion; ?>">
                                                        <input type="hidden" value ="idadquisicion" name="llave">
                                                        <input type="hidden" value="formaadquisicion" name="tabla">
                                                        <input type="hidden" value="fingarantia" name="campo">
                                                        <input type="hidden" value="Fin de Garantia" name="mensaje">
                                                        <div class="input-group">
                                                        <input type="text" value="<?php echo $detalle->fingarantia; ?>" id="valor" name="valor" placeholder="Nombre" required class="form-control">
                                                       
                                                    <input type="submit" class="btn btn-primary">
                                                                
                                                    </div>
                                                    
                                                    </form>
                                                </div>
                                        </div>
                                        <div class="row form-group">
                                                <div class="col col-md-3">
                                                <label for="amperaje" class=" form-control-label">Amperaje</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                    <form action="<?php echo base_url(); ?>index.php/CInventario/modificarCampo" method="POST">
                                                        <input type="hidden" name="id" value="<?php echo $detalle->idespecificaciones; ?>">
                                                        <input type="hidden" value ="idespecificaciones" name="llave">
                                                        <input type="hidden" value="especificaciones" name="tabla">
                                                        <input type="hidden" value="amperaje" name="campo">
                                                        <input type="hidden" value="Amperaje" name="mensaje">
                                                        <div class="input-group">
                                                        <input type="text" value="<?php echo $detalle->amperaje; ?>" id="valor" name="valor" placeholder="Nombre" required class="form-control">
                                                       
                                                    <input type="submit" class="btn btn-primary">
                                                                
                                                    </div>
                                                    
                                                    </form>
                                                </div>
                                        </div>
                                        <div class="row form-group">
                                                <div class="col col-md-3">
                                                <label for="frecuencia" class=" form-control-label">Frecuencia</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                    <form action="<?php echo base_url(); ?>index.php/CInventario/modificarCampo" method="POST">
                                                        <input type="hidden" name="id" value="<?php echo $detalle->idespecificaciones; ?>">
                                                        <input type="hidden" value ="idespecificaciones" name="llave">
                                                        <input type="hidden" value="especificaciones" name="tabla">
                                                        <input type="hidden" value="frecuencia" name="campo">
                                                        <input type="hidden" value="Frecuencia" name="mensaje">
                                                        <div class="input-group">
                                                        <input type="text" value="<?php echo $detalle->frecuencia; ?>" id="valor" name="valor" placeholder="Nombre" required class="form-control">
                                                       
                                                    <input type="submit" class="btn btn-primary">
                                                                
                                                    </div>
                                                    
                                                    </form>
                                                </div>
                                        </div>
                                        <div class="row form-group">
                                                <div class="col col-md-3">
                                                <label for="potencia" class=" form-control-label">Potencia</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                    <form action="<?php echo base_url(); ?>index.php/CInventario/modificarCampo" method="POST">
                                                        <input type="hidden" name="id" value="<?php echo $detalle->idespecificaciones; ?>">
                                                        <input type="hidden" value ="idespecificaciones" name="llave">
                                                        <input type="hidden" value="especificaciones" name="tabla">
                                                        <input type="hidden" value="potencia" name="campo">
                                                        <input type="hidden" value="Potencia" name="mensaje">
                                                        <div class="input-group">
                                                        <input type="text" value="<?php echo $detalle->potencia; ?>" id="valor" name="valor" placeholder="Nombre" required class="form-control">
                                                       
                                                    <input type="submit" class="btn btn-primary">
                                                                
                                                    </div>
                                                    
                                                    </form>
                                                </div>
                                        </div>
                                        <div class="row form-group">
                                                <div class="col col-md-3">
                                                <label for="voltaje" class=" form-control-label">Voltaje</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                    <form action="<?php echo base_url(); ?>index.php/CInventario/modificarCampo" method="POST">
                                                        <input type="hidden" name="id" value="<?php echo $detalle->idespecificaciones; ?>">
                                                        <input type="hidden" value ="idespecificaciones" name="llave">
                                                        <input type="hidden" value="especificaciones" name="tabla">
                                                        <input type="hidden" value="voltaje" name="campo">
                                                        <input type="hidden" value="Voltaje" name="mensaje">
                                                        <div class="input-group">
                                                        <input type="text" value="<?php echo $detalle->voltaje; ?>" id="valor" name="valor" placeholder="Nombre" required class="form-control">
                                                       
                                                    <input type="submit" class="btn btn-primary">
                                                                
                                                    </div>
                                                    
                                                    </form>
                                                </div>
                                        </div>
                                        <div class="row form-group">
                                                <div class="col col-md-3">
                                                <label for="preinstalacion" class=" form-control-label">Preinstalacion</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                    <form action="<?php echo base_url(); ?>index.php/CInventario/modificarCampo" method="POST">
                                                        <input type="hidden" name="id" value="<?php echo $detalle->idespecificaciones; ?>">
                                                        <input type="hidden" value ="idespecificaciones" name="llave">
                                                        <input type="hidden" value="especificaciones" name="tabla">
                                                        <input type="hidden" value="preinstalacion" name="campo">
                                                        <input type="hidden" value="Preinstalacion" name="mensaje">
                                                        <div class="input-group">
                                                        <input type="text" value="<?php echo $detalle->preinstalacion; ?>" id="valor" name="valor" placeholder="Nombre" required class="form-control">
                                                       
                                                    <input type="submit" class="btn btn-primary">
                                                                
                                                    </div>
                                                    
                                                    </form>
                                                </div>
                                        </div>
                                    
                                    </div>
                                    <div class="card-footer">
                                
                                    
                                    </div>
                                </div>
                            </div>