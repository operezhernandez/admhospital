<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Servicios</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li class="active">de equipos</li>
                </ol>
            </div>
        </div>
    </div>
</div>

      <div class="content mt-12">
        <?php
   if(isset($mensaje))
   {
     ?>
    <div class='<?php
    if($error==false)
    {
       echo "alert alert-info alert-dismissible";
    }else{

     echo "alert alert-danger alert-dismissible";

    } ?>'>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-info"></i> Mensaje</h4>
                <?php echo $mensaje; ?>
    </div>
  <?php
   }
   ?>
        <div class="col-md-12 ">
            <ul class="nav nav-pills nav-fill" id="myTab" role="tablist">
                <li class="nav-item">
                  <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Listado</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Nuevo</a>
                </li>

              </ul>
              <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                        <br>
                        <table class="table table-striped table-bordered"  id="tablaInventario">
                             <thead>
                                 <tr>
                                 <th >ID</th>
                                 <th scope="col">Nombre</th>
                                 <th></th>

                                 </tr>
                             </thead>
                             <tbody>
                             <?php
                             if(isset($servicios))
                             {
                                 foreach($servicios as $servicio)
                                 {

                                    if($servicio->habilitado==0)
                                    {
                                        $val='<a href="'.base_url().'CInventario/deshabilitarServicio/'.$servicio->idservicio.'" class="btn btn-danger btn-sm">Deshabilitar</a>';
                                    }else
                                    {
                                        $val='<a href="'.base_url().'CInventario/habilitarServicio/'.$servicio->idservicio.'" class="btn btn-primary btn-sm">Habilitar</a>';
                                    }

                                     echo '<tr>';
                                     echo '<td >'.$servicio->idservicio.'</td>';
                                     echo '<td>'.$servicio->nombre.'</td>';
                                     echo '<td>'.$val.'</td>';
                                      echo '</tr>';
                                 }
                             }

                         ?>




                             </tbody>
                         </table>

                </div>
                <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header">
                          <strong>Información</strong>
                        </div>
                        <div class="card-body card-block">
                          <form action="<?php echo base_url(); ?>index.php/CInventario/guardarServicio" method="post" enctype="multipart/form-data" class="form-vertical">

                            <div class="row form-group">
                                <div class="col col-md-3">
                                  <label for="nombre" class=" form-control-label">Nombre</label>
                                </div>
                              <div class="col-12 col-md-9">
                                  <input type="text" id="nombre"
                                  name="nombre" placeholder="Nombre Servicio"  class="form-control" required>
                                  <small class="form-text text-muted">Nombre Servicio</small></div>
                            </div>



                        </div>
                        <div class="card-footer">
                          <button type="submit" class="btn btn-primary btn-sm">
                            <i class="fa fa-dot-circle-o"></i> Agregar
                          </button>
                          </form>
                        </div>
                    </div>
                </div>

                </div>
              </div>


    </div>
    </div>
