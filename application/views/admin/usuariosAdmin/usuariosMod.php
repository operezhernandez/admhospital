<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Modificar Usuario</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li class="active"></li>
                </ol>
            </div>
        </div>
    </div>

</div>

    <div class="content mt-12">
        <div class="card">
            <div class="card-header">
                <strong>Información del Usuario</strong>
                <?php
                            if(isset($_SESSION["insert"]))
                            {
                                if($_SESSION["insert"]==true)
                            {
                                ?>
                                <div class="alert alert-success alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <strong><?php echo $_SESSION["mensaje"]; ?></strong> Actualizado con exito.
                                </div>
                                <?php
                            }
                            }
                          ?>
            </div>
            <div class="card-body card-block">
                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="descrip" class=" form-control-label">Nombre</label>
                    </div>
                    <div class="col-12 col-md-9">
                        <form action="<?php echo base_url(); ?>index.php/CUsuario/modificarNombre" method="POST">
                            <input type="hidden" name="id" value="<?php echo $usuario->idusuario; ?>">
                            <div class="input-group">
                            <input type="text" value="<?php echo $usuario->nombre; ?>" id="nombre" name="nombre" placeholder="Nombre completo" required class="form-control">
                            <input type="submit" class="btn btn-primary">

                            </div>
                        </form>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="descrip" class=" form-control-label">Username</label>
                    </div>
                    <div class="col-12 col-md-9">
                        <form action="<?php echo base_url(); ?>index.php/CUsuario/modificarUsername" method="POST">
                            <input type="hidden" name="id" value="<?php echo $usuario->idusuario; ?>">
                            <div class="input-group">
                            <input type="text" value="<?php echo $usuario->username; ?>" id="username" name="username" placeholder="Username" required class="form-control">
                            <input type="submit" class="btn btn-primary">

                            </div>
                        </form>
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="descrip" class=" form-control-label">Area</label>
                    </div>
                    <div class="col-12 col-md-9">
                        <form action="<?php echo base_url(); ?>index.php/CUsuario/modificarArea" method="POST">
                            <input type="hidden" name="id" id="idusuariomod" value="<?php echo $usuario->idusuario; ?>">
                            <div class="input-group">
                              <select name="area" id="cmbArea" class="form-control" required>
                              <?php
                                            if(isset($sedes))
                                            {
                                                foreach($sedes as $sede)
                                                {
                                                    echo '<option value="'.$sede->idsede.'">'.$sede->sede.'</option>';
                                                }
                                            }
                                        ?>
                              </select>
                            <input type="submit" class="btn btn-primary">

                            </div>
                        </form>
                    </div>
                </div>

                <div class="row form-group">
                        <div class="col col-md-3">
                            <label for="descrip" class=" form-control-label">Contraseña</label>
                        </div>
                        <div class="col-12 col-md-9">
                            <form action="<?php echo base_url(); ?>index.php/CUsuario/modificarPass" method="POST">
                                <input type="hidden" name="id" value="<?php echo $usuario->idusuario; ?>">
                                <div class="input-group">
                                <input type="password" value="" id="descripcion" name="password" placeholder="password" required class="form-control">
                                <input type="submit" class="btn btn-primary">

                                </div>
                            </form>
                        </div>
                    </div>
                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="descrip" class=" form-control-label">Telefono</label>
                    </div>
                    <div class="col-12 col-md-9">
                        <form action="<?php echo base_url(); ?>index.php/CUsuario/modificarTelefono" method="POST">
                            <input type="hidden" name="id" value="<?php echo $usuario->idusuario; ?>">
                            <div class="input-group">
                            <input type="text" value="<?php echo $usuario->telefono; ?>" id="telefono" name="telefono" placeholder="Telefono" required class="form-control">
                            <input type="submit" class="btn btn-primary">

                           </div>
                        </form>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="descrip" class=" form-control-label">Puesto</label>
                    </div>
                    <div class="col-12 col-md-9">
                        <form action="<?php echo base_url(); ?>index.php/CUsuario/modificarPuesto" method="POST">
                            <input type="hidden" name="id" value="<?php echo $usuario->idusuario; ?>">
                            <div class="input-group">
                            <input type="text" value="<?php echo $usuario->puesto; ?>" id="puesto" name="puesto" placeholder="Puesto" required class="form-control">
                            <input type="submit" class="btn btn-primary">

                           </div>
                        </form>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="descrip" class=" form-control-label">Foto</label>
                    </div>
                    <div class="col-12 col-md-9">
                        <form enctype="multipart/form-data" action="<?php echo base_url(); ?>index.php/CUsuario/actualizarImagen" method="POST">
                            <input type="hidden" name="id" value="<?php echo $usuario->idusuario; ?>">
                            <div class="input-group">
                            <input type="file" value="" id="imagen" name="imagen" placeholder="imagen" required class="form-control">
                            <input type="submit" class="btn btn-primary">

                           </div>
                        </form>
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="descrip" class=" form-control-label">Salario</label>
                    </div>
                    <div class="col-12 col-md-9">
                        <form action="<?php echo base_url(); ?>index.php/CUsuario/modificarSalario" method="POST">
                            <input type="hidden" name="id" value="<?php echo $usuario->idusuario; ?>">
                            <div class="input-group">
                            <input type="text" value="<?php echo $usuario->salario; ?>" id="salario" name="salario" placeholder="Salario" required class="form-control">

                            <input type="submit" class="btn btn-primary">

                           </div>
                           <small style="color:red">Ingresar salario, solo si es tecnico, este salario es por hora trabajada.</small>
                        </form>
                    </div>
                </div>

            </div>
            <div class="card-footer">

            </div>
        </div>
    </div>
