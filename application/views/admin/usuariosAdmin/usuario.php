<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Personal</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li class="active"></li>
                </ol>
            </div>
        </div>
    </div>

</div>

        <div class="content mt-12">
        <?php
   if(isset($mensaje))
   {
     ?>
    <div class='<?php
    if($error==false)
    {
       echo "alert alert-info alert-dismissible";
    }else{

     echo "alert alert-danger alert-dismissible";

    } ?>'>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-info"></i> Mensaje</h4>
                <?php echo $mensaje; ?>
    </div>
  <?php
   }
   ?>
        <div class="col-md-12 ">
            <ul class="nav nav-pills nav-fill" id="myTab" role="tablist">
                <li class="nav-item">
                  <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Listado</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Nuevo</a>
                </li>

              </ul>
              <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                        <br>

                        <table class="table"  id="tablaUsuario">
                             <thead>
                                 <tr>
                                 <th >ID</th>
                                 <th >Nombre</th>
                                 <th class="w-25">Username</th>
                                 <th scope="col">Telefono</th>
                                 <th scope="col">puesto</th>
                                 <th scope="col">Salario/hora</th>
                                 <th scope="col">Tipo</th>
                                <th scope="col">Modificar</th>
                                 <th scope="col">Controles</th>
                                 </tr>
                             </thead>
                             <tbody>
                                <?php
                                    if(isset($usuarios))
                                    {
                                        foreach($usuarios as $usuario)
                                        {

                                            if($usuario->habilitado==1)
                                            {
                                                $val='<a href="'.base_url().'CUsuario/deshabilitarUsuario/'.$usuario->idusuario.'" class="btn btn-danger btn-sm">Deshabilitar</a>';
                                            }else
                                            {
                                                $val='<a href="'.base_url().'CUsuario/habilitarUsuario/'.$usuario->idusuario.'" class="btn btn-primary btn-sm">Habilitar</a>';
                                            }

                                            echo '<tr>';
                                            echo '<td >'.$usuario->idusuario.'</td>';
                                            echo '<td>'.$usuario->nombre.'</td>';
                                            $newtext = wordwrap($usuario->username, 15, "<br>",true);
                                            echo '<td >'.$newtext.'</td>';
                                            echo '<td>'.$usuario->telefono.'</td>';
                                            echo '<td>'.$usuario->puesto.'</td>';

                                            if ($usuario->tipo != 'Tecnico') {
                                              echo '<td><span class="badge badge-warning">Salario fijo</span></td>';
                                            }
                                            else {
                                              echo '<td>$'.$usuario->salario.'</td>';
                                            }
                                            echo '<td>'.$usuario->tipo.'</td>';
                                            echo '<td><a href="'.base_url().'CUsuario/modificarUsuario/'.$usuario->idusuario.'" class="btn btn-primary btn-sm">Modificar</a></td>';
                                            echo '<td>'.$val.'</td>';
                                            echo '</tr>';
                                        }
                                    }

                                ?>



                             </tbody>
                         </table>


                </div>
                <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header">
                          <strong>Nuevo Usuario</strong>
                        </div>
                        <div class="card-body card-block">
                          <form action="<?php echo base_url(); ?>index.php/CUsuario/crearUsuario" method="post" enctype="multipart/form-data" class="form-vertical">

                            <div class="row form-group">
                                <div class="col col-md-3">
                                  <label for="nombre" class=" form-control-label">Nombre completo</label>
                                </div>
                              <div class="col-12 col-md-9">
                                  <input type="text" id="nombre"
                                  name="nombre" placeholder="Nombre"  class="form-control" required>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3">
                                  <label for="email-input" class=" form-control-label">Username</label>
                                </div>
                                <div class="col-12 col-md-9">
                                  <input type="text" id="username"
                                  name="username" placeholder="UserName"  class="form-control" required>

                                </div>
                            </div>
                            <div class="row form-group">
                                    <div class="col col-md-3"><label for="tipo" class=" form-control-label">Tipo de Usuario</label></div>
                                    <div class="col-12 col-md-9">
                                      <select name="tipo" id="tipo" class="form-control">
                                        <option value="0" required>Seleccione un tipo</option>
                                        <?php
                                            if(isset($tiposUsuarios))
                                            {
                                                foreach($tiposUsuarios as $tipo)
                                                {
                                                    echo '<option value="'.$tipo->idtipouser.'">'.$tipo->tipo.'</option>';
                                                }
                                            }
                                        ?>
                                      </select>
                                    </div>
                            </div>
                            <div class="row form-group">
                                    <div class="col col-md-3">
                                      <label for="telefono" class=" form-control-label">Telefono</label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                      <input type="text" id="telefono"
                                      name="telefono" placeholder="Telefono"  class="form-control" required>

                                    </div>
                            </div>
                            <div class="row form-group">
                                    <div class="col col-md-3">
                                      <label for="puesto" class=" form-control-label">Puesto</label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                      <input type="text" id="puesto"
                                      name="puesto" placeholder="Puesto"  class="form-control" required>

                                    </div>
                            </div>
                            <div class="row form-group">
                                    <div class="col col-md-3">
                                      <label for="salario" class=" form-control-label">Salario</label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                      <input type="text" id="salario"
                                      name="salario" placeholder="Salario"  class="form-control" onkeypress="return validar(event)">
                                      <small style="color:red">Ingresar salario, solo si es tecnico, este salario es por hora trabajada.</small>
                                    </div>
                            </div>

                            <div class="row form-group">
                                    <div class="col col-md-3"><label for="tipo" class=" form-control-label">Area</label></div>
                                    <div class="col-12 col-md-9">
                                      <select name="area" id="area" class="form-control" required>
                                      
                                        <?php
                                            if(isset($sedes))
                                            {
                                                foreach($sedes as $sede)
                                                {
                                                    echo '<option value="'.$sede->idsede.'">'.$sede->sede.'</option>';
                                                }
                                            }
                                        ?>
                                        </select>
                                    </div>
                            </div>

                            <div class="row form-group">
                                    <div class="col col-md-3">
                                      <label for="pass" class=" form-control-label">Contraseña</label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                      <input type="password" id="pass"
                                      name="pass" placeholder="Contraseña"  class="form-control" required>

                                    </div>
                            </div>
                            <div class="row form-group">
                                    <div class="col col-md-3">
                                      <label for="pass1" class=" form-control-label">Repetir Contraseña</label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                      <input type="password" id="pass1"
                                      name="pass1" placeholder="Contraseña"  class="form-control" required>

                                    </div>
                            </div>
                            <div class="row form-group">
                                    <div class="col col-md-3">
                                      <label for="pass1" class=" form-control-label">Agregar Imagen</label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                      <input type="file" id="imagen"
                                      name="imagen" placeholder="Contraseña"  class="form-control" required>

                                    </div>
                            </div>
                        </div>
                        <div class="card-footer">
                          <button type="submit" class="btn btn-primary btn-sm">
                            <i class="fa fa-dot-circle-o"></i> Agregar
                          </button>
                        </form>
                        </div>
                    </div>
                </div>

                </div>
              </div>


    </div>
    </div>
       <!-- Button trigger modal -->


<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>

<script>
  function validar(e){
    obj=e.srcElement || e.target;
    tecla_codigo = (document.all) ? e.keyCode : e.which;
    if(tecla_codigo==8)return true;
    patron =/[\d.]/;
    tecla_valor = String.fromCharCode(tecla_codigo);
    control=(tecla_codigo==46 && (/[.]/).test(obj.value))?false:true
    return patron.test(tecla_valor) &&  control;

  }
</script>
