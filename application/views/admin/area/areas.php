<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Areas</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
        </div>
    </div>
</div>

      <div class="content mt-12">
     
        <div class="col-md-12 ">
        <?php
   if(isset($mensaje))
   {
     ?>
    <div class='<?php
    if($error==false)
    {
       echo "alert alert-info alert-dismissible";
    }else{

     echo "alert alert-danger alert-dismissible";

    } ?>'>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-info"></i> Mensaje</h4>
                <?php echo $mensaje; ?>
    </div>
  <?php
   }
   ?>
            <ul class="nav nav-pills nav-fill" id="myTab" role="tablist">
                <li class="nav-item">
                  <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Listado</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Nuevo</a>
                </li>

              </ul>
              <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                        <br>
                        <table class="table table-striped table-bordered"  id="tablaInventario">
                             <thead>
                                 <tr>
                                 <th >ID</th>
                                 <th scope="col">Nombre</th>
                                 <th>Control</th>
                                 </tr>
                             </thead>
                             <tbody>
                             <?php
                             if(isset($areas))
                             {
                                 foreach($areas as $area)
                                 {
                                    if($area->habilitado==0)
                                            {
                                                $val='<a href="'.base_url().'CInventario/deshabilitarArea/'.$area->idsede.'" class="btn btn-danger btn-sm">Deshabilitar</a>';
                                            }else
                                            {
                                                $val='<a href="'.base_url().'CInventario/habilitarArea/'.$area->idsede.'" class="btn btn-primary btn-sm">Habilitar</a>';
                                            }

                                     echo '<tr>';
                                     echo '<td >'.$area->idsede.'</td>';
                                     echo '<td>'.$area->sede.'</td>';
                                     echo '<td>'.$val.'</td>';
                                      echo '</tr>';
                                 }
                             }

                         ?>




                             </tbody>
                         </table>

                </div>
                <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header">
                          <strong>Información</strong>
                        </div>
                        <div class="card-body card-block">
                          <form action="<?php echo base_url(); ?>index.php/CInventario/guardarArea" method="post" enctype="multipart/form-data" class="form-vertical">

                            <div class="row form-group">
                                <div class="col col-md-3">
                                  <label for="nombre" class=" form-control-label">Nombre</label>
                                </div>
                              <div class="col-12 col-md-9">
                                  <input type="text" id="nombre"
                                  name="nombre" placeholder="Nombre area"  class="form-control" required>
                                  <small class="form-text text-muted">Nombre area</small></div>
                            </div>



                        </div>
                        <div class="card-footer">
                          <button type="submit" class="btn btn-primary btn-sm">
                            <i class="fa fa-dot-circle-o"></i> Agregar
                          </button>
                          </form>
                        </div>
                    </div>
                </div>

                </div>
              </div>


    </div>
    </div>
