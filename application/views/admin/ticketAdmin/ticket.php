<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Ordenes de trabajo recibidas</h1>
                <?php
                            if(isset($_SESSION["insert"]))
                            {
                                if($_SESSION["insert"]==true)
                            {
                                ?>
                                <div class="alert alert-success alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <strong>Orden de trabajo </strong>Realizada con exito.
                                </div>
                                <?php
                            }
                            }
                          ?>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li class="active">Ordenes de trabajo</li>
                </ol>
            </div>
        </div>
    </div>
</div>

        <div class="content mt-12">
        <div class="col-md-12 ">

                   <div class="card">
                       <div class="card-header">

                       </div>
                       <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered" style="width:100%"  id="mitable">
                                <thead>
                                    <tr>
                                    <th >Correlativo</th>
                                    <th scope="col">Usuario</th>
                                    <th scope="col">Equipo</th>
                                    <th scope="col">Descripción</th>
                                    <th scope="col">Fecha</th>
                                    <th scope="col">Estado</th>
                                    <th scope="col">Tipo</th>
                                    <th scope="col">Controles</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if(isset($tickets))
                                    {
                                        foreach($tickets as $t)
                                        {
                                            ?>
                                        <tr>
                                            <td><?php echo $t->idticket ?></td>
                                            <td><?php echo $t->nombre ?></td>
                                            <td><?php echo $t->nombreequipo ?></td>
                                            <td><?php echo $t->descripcion ?></td>
                                            <td><?php echo $t->fecha ?></td>
                                            <td><?php  switch($t->estado){  case 0:echo '<span class="badge badge-dark">Pendiente</span>'; break;
                                                                            case 1: echo '<span class="badge badge-success">Finalizado</span>'; break;
                                                                            case 2: echo '<span class="badge badge-info">Solicitado</span>'; break;
                                                                            case 3: echo '<span class="badge badge-danger">Cancelado</span>'; break;
                                                                            case 4: echo '<span class="badge badge-warning">En Proceso</span>'; break; } ?></td>
                                            <?php if ($t->tipomantenimiento=='Correctivo'): ?>
                                              <td><?php echo '<span class="badge badge-danger">Correctivo</span>' ?></td>
                                              <?php else: ?>
                                              <td><?php echo '<span class="badge badge-primary">Preventivo</span>' ?></td>
                                            <?php endif; ?>


                                            <td>
                                                <?php
                                                    if($t->estado==0)
                                                    {
                                                        ?>
                                                        <a href="<?php echo base_url(); ?>Ctickets/ticketsAsignar/<?php echo $t->idticket ?>" class="btn btn-info btn-sm">Asignar</a><hr>
                                                        <!-- <a href="<?php echo base_url(); ?>Ctickets/ticketsCancelar/<?php echo $t->idticket ?>" class="btn btn-info btn-sm">Cancelar</a><hr> -->
                                                        <?php
                                                    }
                                                ?>

                                            </td>
                                        </tr>
                                            <?php
                                        }
                                    }
                                    ?>




                                </tbody>
                            </table>


                        </div>
                        </div>
                   </div>

    </div>
    </div>
       <!-- Button trigger modal -->


<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

      </div>
    </div>
  </div>
</div>
