<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Asignar ordenes de trabajo</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li class="active">Orden ed trabajo</li>
                </ol>
            </div>
        </div>
    </div>
</div>

    <div class="content mt-12">
        <div class="col-md-5">
                <div class="card">
                        <div class="card-header">
                          <strong>Información</strong>
                        </div>
                        <div class="card-body card-block">
                          <form action="" method="post" enctype="multipart/form-data" class="form-vertical">
                            <div class="row form-group">
                              <div class="col col-md-3"><label class=" form-control-label">Id OT</label></div>
                              <div class="col-12 col-md-9">
                                <p class="form-control-static"><?php echo $infot->idticket ?></p>
                              </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3">
                                  <label for="text-input" class=" form-control-label">Equipo</label>
                                </div>
                              <div class="col-12 col-md-9">
                                  <input type="text" id="text-input" name="text-input" placeholder="Equipo" value="<?php echo $infot->nombre ?>" disabled class="form-control">
                                  <small class="form-text text-muted">Equipo a reparar</small></div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3">
                                  <label for="email-input" class=" form-control-label">Usuario</label>
                                </div>
                                <div class="col-12 col-md-9">
                                  <input type="text" id="usuarioDet" name="usuarioDet" placeholder="Usuario" value="<?php echo $infot->name ?>" disabled class="form-control">
                                  <small class="help-block form-text">Usuario que detecto la falla</small>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3">
                                  <label for="password-input" class=" form-control-label">Descripción</label>
                                </div>
                              <div class="col-12 col-md-9">
                                    <textarea class="form-control" id="descripcion" rows="5" disabled><?php echo $infot->descripcion ?></textarea>
                                  <small class="help-block form-text">Descripción</small></div>
                            </div>
                            <div class="row form-group">
                              <div class="col col-md-3"><label for="disabled-input" class=" form-control-label">Fecha</label></div>
                              <div class="col-12 col-md-9"><input type="text" disabled id="disabled-input" name="disabled-input" value="<?php echo $infot->fechainicio ?>" placeholder="Fecha" disabled="" class="form-control"></div>
                            </div>


                          </form>
                        </div>
                        <div class="card-footer">


                        </div>
                      </div>
        </div>

        <div class="col-md-7">
          <div class="card">
            <div class="card-header">
              <strong>Tecnicos</strong>
            </div>
            <div class="card-body card-block">
                <div id="listaUsuarioTicket">
                    <input class="search form-control" placeholder="Buscar" /><hr>
                    <div class="list-group list">
                        <?php
                          if(isset($tecnicos))
                          {
                            foreach($tecnicos as $tec)
                            {?>
                               <a href="<?php echo base_url().'Ctickets/asignarTicketSave/'.$infot->idticket.'/'.$tec->idusuario ?>" class="list-group-item list-group-item-action">
                                 <p >Nombre: <strong class="nombre"><?php echo $tec->nombre ?></strong></p>
                                 <p class="">Area: <strong class="area"><?php echo $tec->area ?></strong></p>
                                 <p class="">Puesto: <strong class="puesto"><?php echo $tec->puesto ?></strong></p>
                                </a>

                           <?php }
                          }
                        ?>
                    </div>
                    <nav aria-label="Page navigation example">
                    <ul class="pagination"></ul>
                        </nav>
                </div>
            </div>
            <div class="card-footer">

            </div>
          </div>
        </div>
    </div>
