<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Añadir Observación</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li class="active"></li>
                </ol>
            </div>
        </div>
    </div>
</div>

    <div class="content mt-12">
        <div class="col-md-12">
                <div class="card">
                        <div class="card-header">
                          <strong>Observación</strong>
                        </div>
                        <div class="card-body card-block">
                            <form action="<?php echo base_url(); ?>Ctickets/addObservacionesEquipoSave" method="post">
                                <input type="hidden" name="id" value="<?php echo $id; ?>">
                                <div class="form-group">
                                    <label for="descripcion">Descripción</label>
                                    <textarea  class="form-control" id="descripcion" required name="descripcion" placeholder="Descripción"></textarea>
                                </div>
                                
                                <button type="submit" class="btn btn-default">Enviar</button>
                            </form>
                        </div>
                        <div class="card-footer">
                            
                        
                        </div>
                </div>
        </div>
    </div>    