<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Generar Orden de trabajo</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li class="active">Ordenes de trabajo</li>
                </ol>
            </div>
        </div>
    </div>
</div>

    <div class="content mt-12">
        <div class="col-md-12">
                <div class="card">
                        <div class="card-header">
                          <strong>Información</strong>
                        </div>
                        <div class="card-body card-block">
                        <form action="<?php echo base_url(); ?>Ctickets/saveTicket" method="post" enctype="multipart/form-data" class="form-vertical">

                            <div class="row form-group">
                                <div class="col col-md-3">

                                </div>
                                <div class="col-12 col-md-9">
                                    <input type="hidden" name="id" class="form-control" value="<?php echo $id ?>">
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col col-md-3">
                                  <label for="password-input" class=" form-control-label">Descripción</label>
                                </div>
                              <div class="col-12 col-md-9">
                                    <textarea class="form-control" required name="descripcion" rows="5" ></textarea>
                                  <small class="help-block form-text">Descripción</small></div>
                            </div>




                        </div>
                        <div class="card-footer">
                          <button type="submit" class="btn btn-primary btn-sm">
                            <i class="fa fa-dot-circle-o"></i> Generar
                          </button>
                        </form>
                        </div>
                      </div>
        </div>
    </div>
