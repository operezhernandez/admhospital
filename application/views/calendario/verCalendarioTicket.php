<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Informacion de la Orden de trabajo</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li class="active">Orden de trabajo</li>
                </ol>
            </div>
        </div>
    </div>
</div>

    <div class="content mt-12">
        <div class="col-md-4">
                <div class="card">
                        <div class="card-header">
                          <strong>Información del equipo</strong>
                        </div>
                        <div class="card-body card-block">
                          <form action="" method="post" enctype="multipart/form-data" class="form-vertical">

                            <div class="row form-group">
                                <div class="col col-md-3">
                                  <label for="text-input" class=" form-control-label">Equipo</label>
                                </div>
                              <div class="col-12 col-md-9">
                                  <input type="text" id="text-input" name="text-input" placeholder="Equipo" value="<?php echo $infot->nombreequipo ?>" disabled class="form-control">
                                  </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3">
                                  <label for="email-input" class=" form-control-label">Marca</label>
                                </div>
                                <div class="col-12 col-md-9">
                                  <input type="text" id="usuarioDet" name="usuarioDet" placeholder="Usuario" value="<?php echo $infot->marca ?>" disabled class="form-control">
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3">
                                  <label for="email-input" class=" form-control-label">Serie</label>
                                </div>
                                <div class="col-12 col-md-9">
                                  <input type="text" id="usuarioDet" name="usuarioDet" placeholder="Usuario" value="<?php echo $infot->serie ?>" disabled class="form-control">
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3">
                                  <label for="email-input" class=" form-control-label">Modelo</label>
                                </div>
                                <div class="col-12 col-md-9">
                                  <input type="text" id="usuarioDet" name="usuarioDet" placeholder="Usuario" value="<?php echo $infot->modelo ?>" disabled class="form-control">
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3">
                                  <label for="email-input" class=" form-control-label">Fabricante</label>
                                </div>
                                <div class="col-12 col-md-9">
                                  <input type="text" id="usuarioDet" name="usuarioDet" placeholder="Usuario" value="<?php echo $infot->fabricante ?>" disabled class="form-control">
                                </div>
                            </div>


                          </form>
                        </div>
                        <div class="card-footer">
                        </div>
              </div>
        </div>

        <div class="col-md-4">
                <div class="card">
                        <div class="card-header">
                          <strong>Información de la OT</strong>
                        </div>
                        <div class="card-body card-block">
                          <form action="" method="post" enctype="multipart/form-data" class="form-vertical">

                            <div class="row form-group">
                                <div class="col col-md-3">
                                  <label for="text-input" class=" form-control-label">id Orden</label>
                                </div>
                              <div class="col-12 col-md-9">
                                  <input type="text" id="text-input" name="text-input" placeholder="Equipo" value="<?php echo $infot->idticket ?>" disabled class="form-control">
                                  </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3">
                                  <label for="email-input" class=" form-control-label">Usuario</label>
                                </div>
                                <div class="col-12 col-md-9">
                                  <input type="text" id="usuarioDet" name="usuarioDet" placeholder="Usuario" value="<?php echo $infot->nombreusu ?>" disabled class="form-control">
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3">
                                  <label for="email-input" class=" form-control-label">Area</label>
                                </div>
                                <div class="col-12 col-md-9">
                                  <input type="text" id="usuarioDet" name="usuarioDet" placeholder="Usuario" value="<?php echo $infot->area ?>" disabled class="form-control">
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3">
                                  <label for="email-input" class=" form-control-label">Telefono</label>
                                </div>
                                <div class="col-12 col-md-9">
                                  <input type="text" id="usuarioDet" name="usuarioDet" placeholder="Usuario" value="<?php echo $infot->telefono ?>" disabled class="form-control">
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3">
                                  <label for="email-input" class=" form-control-label">Descripcion</label>
                                </div>
                                <div class="col-12 col-md-9">
                                  <textarea class="form-control" id="descripcion" rows="5" disabled><?php echo $infot->descripcion ?></textarea>
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col col-md-3">
                                  <label for="email-input" class=" form-control-label">Estado</label>
                                </div>
                                <div class="col-12 col-md-9">
                                  <?php if ($infot->estado==1): ?>
                                    <input type="text" id="usuarioDet" name="usuarioDet" placeholder="Usuario" value="Finalizado" disabled class="form-control">
                                  <?php elseif($infot->estado==2): ?>
                                  <input type="text" id="usuarioDet" name="usuarioDet" placeholder="Usuario" value="Solicitado" disabled class="form-control">
                                <?php elseif($infot->estado==3): ?>
                                  <input type="text" id="usuarioDet" name="usuarioDet" placeholder="Usuario" value="Cancelado" disabled class="form-control">
                                <?php elseif($infot->estado==4):?>
                                  <input type="text" id="usuarioDet" name="usuarioDet" placeholder="Usuario" value="En Proceso" disabled class="form-control">
                                  <?php endif; ?>
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col col-md-3">
                                  <label for="email-input" class=" form-control-label">Tipo mantenimiento</label>
                                </div>
                                <div class="col-12 col-md-9">
                                  <input type="text" id="usuarioDet" name="usuarioDet" placeholder="Usuario" value="<?php echo $infot->tipomantenimiento ?>" disabled class="form-control">
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3">
                                  <label for="email-input" class=" form-control-label">Fecha que se asigno</label>
                                </div>
                                <div class="col-12 col-md-9">
                                  <input type="text" id="usuarioDet" name="usuarioDet" placeholder="Usuario" value="<?php echo $infot->fechaasignacion ?>" disabled class="form-control">
                                </div>
                            </div>


                          </form>
                        </div>
                        <div class="card-footer">
                        </div>
              </div>
        </div>

        <div class="col-md-4">
                <div class="card">
                        <div class="card-header">
                          <strong>Información del tecnico encargado</strong>
                        </div>
                        <div class="card-body card-block">
                          <form action="" method="post" enctype="multipart/form-data" class="form-vertical">

                            <div class="row form-group">
                                <div class="col col-md-3">
                                  <label for="text-input" class=" form-control-label">Tecnico</label>
                                </div>
                              <div class="col-12 col-md-9">
                                  <input type="text" id="text-input" name="text-input" placeholder="Equipo" value="<?php echo $infotec->nombre ?>" disabled class="form-control">
                                  </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3">
                                  <label for="email-input" class=" form-control-label">Area</label>
                                </div>
                                <div class="col-12 col-md-9">
                                  <input type="text" id="usuarioDet" name="usuarioDet" placeholder="Usuario" value="<?php echo $infotec->area ?>" disabled class="form-control">
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3">
                                  <label for="email-input" class=" form-control-label">Telefono</label>
                                </div>
                                <div class="col-12 col-md-9">
                                  <input type="text" id="usuarioDet" name="usuarioDet" placeholder="Usuario" value="<?php echo $infotec->telefono ?>" disabled class="form-control">
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3">
                                  <label for="email-input" class=" form-control-label">Informe de la OT</label>
                                </div>
                                <div class="col-12 col-md-9">
                                  <?php if ($infot->informetecnico==''||$infot->informetecnico==null): ?>
                                    <textarea class="form-control" id="descripcion" rows="5" disabled>Esta orden de trabajo no tiene un informe.</textarea>
                                    <?php else: ?>
                                      <textarea class="form-control" id="descripcion" rows="5" disabled><?php echo $infot->informetecnico ?></textarea>
                                  <?php endif; ?>

                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3">
                                  <label for="email-input" class=" form-control-label">Fecha inicio</label>
                                </div>
                                <?php if ($infot->estado != 1): ?>
                                  <div class="col-12 col-md-9">
                                    <input type="text" id="usuarioDet" name="usuarioDet" placeholder="Usuario" value="Esta OT no ha sido finalizada." disabled class="form-control">
                                  </div>

                                <?php else: ?>
                                  <div class="col-12 col-md-9">
                                    <input type="text" id="usuarioDet" name="usuarioDet" placeholder="Usuario" value="<?php echo $infominutos->fechainicio ?>" disabled class="form-control">
                                  </div>
                                <?php endif; ?>
                            </div>

                            <div class="row form-group">
                                <div class="col col-md-3">
                                  <label for="email-input" class=" form-control-label">Fecha final</label>
                                </div>
                                <?php if ($infot->estado != 1): ?>
                                  <div class="col-12 col-md-9">
                                    <input type="text" id="usuarioDet" name="usuarioDet" placeholder="Usuario" value="Esta OT no ha sido finalizada." disabled class="form-control">
                                  </div>

                                <?php else: ?>
                                  <div class="col-12 col-md-9">
                                    <input type="text" id="usuarioDet" name="usuarioDet" placeholder="Usuario" value="<?php echo $infominutos->fechafinal ?>" disabled class="form-control">
                                  </div>
                                <?php endif; ?>
                            </div>

                            <div class="row form-group">
                                <div class="col col-md-3">
                                  <label for="email-input" class=" form-control-label">Comision por OT finalziada</label>
                                </div>
                                <?php if ($infot->estado != 1): ?>
                                  <div class="col-12 col-md-9">
                                    <input type="text" id="usuarioDet" name="usuarioDet" placeholder="Usuario" value="Esta OT no ha sido finalizada." disabled class="form-control">
                                  </div>

                                <?php else: ?>
                                  <div class="col-12 col-md-9">
                                    <input type="text" id="usuarioDet" name="usuarioDet" placeholder="Usuario" value="$ <?php echo (($infotec->salario/60)*$infominutos->minutos) ?>" disabled class="form-control">
                                  </div>
                                <?php endif; ?>
                            </div>


                          </form>
                        </div>
                        <div class="card-footer">
                        </div>
              </div>
        </div>



        </div>
    </div>
