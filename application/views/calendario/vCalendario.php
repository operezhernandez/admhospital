<!-- fullCalendar -->
<link rel="stylesheet" href="<?php echo base_url(); ?>public/bower_components/fullcalendar/dist/fullcalendar.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>public/bower_components/fullcalendar/dist/fullcalendar.print.min.css" media="print">


<!-- fullCalendar -->
<script src="<?php echo base_url(); ?>public/calendario/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>public/bower_components/moment/moment.js"></script>
<script src="<?php echo base_url(); ?>public/bower_components/fullcalendar/dist/fullcalendar.min.js"></script>
<script src="<?php echo base_url(); ?>public/calendario/locale/es.js"></script>
<!-- <script src="<?php echo base_url(); ?>public/bower_components/jquery-ui/jquery-ui.min.js"></script> -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<script>
$(document).ready(function(){

  $.post('<?php echo base_url(); ?>index.php/Ctickets/getTickets',
  function(data){
    $('#calendar').fullCalendar({
      header: {
        left: 'prev,next today',
        center: 'title'
      },
      buttonText: {
        today: 'Hoy'
      },
          //Random default events
          eventSources:[{
            events    : $.parseJSON(data),
            color:"#ed921c",
            textcolor:"white"
          }],
          eventClick: function(calEvent, jsEvent, view){
            window.location.href = '<?php echo base_url(); ?>index.php/CCalendario/verTicket/'+calEvent.idticket+'/'+calEvent.estado;
          },

        });
      });

    });
</script>


<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Calendario de actividades</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li class="active">Calendario</li>
                </ol>
            </div>
        </div>
    </div>
</div>


<div class="row"><div class ="col-md-2"></div>
    <!-- /.col -->
    <div class="col-md-8">
      <div class="box box-primary">
        <div class="box-body no-padding">
          <div class="card">
          <!-- THE CALENDAR -->
          <div id="calendar"></div>
          </div>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /. box -->
    </div>
    <!-- /.col -->
  </div>
<!-- Button trigger modal -->


<div class="modal fade" id="InfoDia">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Infomación general del Ticket</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" style="font-size:12px">

        <div class="list-group">
          <a class="list-group-item list-group-item-action flex-column align-items-start">
            <div class="d-flex w-100 justify-content-between">
              <h5 class="mb-1">Nombre usuario solicitante</h5>
            </div>
            <p class="mb-1" id="nombreusu"></p>
          </a>
        </div>

        <div class="list-group">
          <a class="list-group-item list-group-item-action flex-column align-items-start">
            <div class="d-flex w-100 justify-content-between">
              <h5 class="mb-1">Area</h5>
            </div>
            <p class="mb-1" id="area"></p>
          </a>
        </div>

        <div class="list-group">
          <a class="list-group-item list-group-item-action flex-column align-items-start">
            <div class="d-flex w-100 justify-content-between">
              <h5 class="mb-1">Sede</h5>
            </div>
            <p class="mb-1" id="sede"></p>
          </a>
        </div>

        <div class="list-group">
          <a class="list-group-item list-group-item-action flex-column align-items-start">
            <div class="d-flex w-100 justify-content-between">
              <h5 class="mb-1">Telefono</h5>
            </div>
            <p class="mb-1" id="telefono"></p>
          </a>
        </div>

        <div class="list-group">
          <a class="list-group-item list-group-item-action flex-column align-items-start">
            <div class="d-flex w-100 justify-content-between">
              <h5 class="mb-1">Equipo</h5>
            </div>
            <p class="mb-1" id="equipo"></p>
          </a>
        </div>

        <div class="list-group">
          <a class="list-group-item list-group-item-action flex-column align-items-start">
            <div class="d-flex w-100 justify-content-between">
              <h5 class="mb-1">Tipo mantenimiento</h5>
            </div>
            <p class="mb-1" id="tipomantenimiento"></p>
          </a>
        </div>

        <div class="list-group">
          <a class="list-group-item list-group-item-action flex-column align-items-start">
            <div class="d-flex w-100 justify-content-between">
              <h5 class="mb-1">Fecha que se asigno</h5>
            </div>
            <p class="mb-1" id="fechaasignacion"></p>
          </a>
        </div>

        <div class="list-group">
          <a class="list-group-item list-group-item-action flex-column align-items-start">
            <div class="d-flex w-100 justify-content-between">
              <h5 class="mb-1">Descripcion del problema</h5>
            </div>
            <p class="mb-1" id="descripcion"></p>
          </a>
        </div>

        <div class="list-group">
          <a class="list-group-item list-group-item-action flex-column align-items-start">
            <div class="d-flex w-100 justify-content-between">
              <h5 class="mb-1">Informe del tecnico</h5>
            </div>
            <p class="mb-1" id="informe"></p>
          </a>
        </div>

        <div class="list-group">
          <a class="list-group-item list-group-item-action flex-column align-items-start">
            <div class="d-flex w-100 justify-content-between">
              <h5 class="mb-1">Comision por orden terminada</h5>
            </div>
            <p class="mb-1" id="salario"></p>
          </a>
        </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
