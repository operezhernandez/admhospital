<!doctype html>
<html class="no-js" lang="">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Hospital Solutions</title>
    <meta name="description" content="Sufee Admin - HTML5 Admin Template">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" href="apple-icon.png">
    <link rel="shortcut icon" href="favicon.ico">
    <link rel="stylesheet" href="<?php echo base_url();?>public/assets/css/normalize.css">
    <link rel="stylesheet" href="<?php echo base_url();?>public/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>public/assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>public/assets/css/themify-icons.css">
    <link rel="stylesheet" href="<?php echo base_url();?>public/assets/css/flag-icon.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>public/assets/css/cs-skin-elastic.css">
    <!-- <link rel="stylesheet" href="assets/css/bootstrap-select.less"> -->
    <link rel="stylesheet" href="<?php echo base_url();?>public/assets/scss/style.css">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>

    <!-- <script type="text/javascript" src="https://cdn.jsdelivr.net/html5shiv/3.7.3/html5shiv.min.js"></script> -->
</head>
<body class="bg-dark">


    <div class="sufee-login d-flex align-content-center flex-wrap">
        <div class="container">
            <div class="login-content">
                <div class="login-logo">
                    <a href="index.html">
                        <img class="align-content" src="<?php echo base_url();?>logo.png" alt="">
                    </a>
                </div>
                <div class="login-form">
                    <form action="<?php echo base_url(); ?>verificar" method="post">
                        <div class="form-group">
                            <label>Username</label>
                            <input type="text" class="form-control" placeholder="Username" name="txtUsername" required>
                        </div>
                        <div class="form-group">
                            <label>Contraseña</label>
                            <input type="password" class="form-control" placeholder="Password" name="txtPass" required>
                        </div>

                        <button type="submit" class="btn btn-success btn-flat m-b-30 m-t-30">Ingresar</button>
                    </form>
                    <?php
                    if(isset($mensaje))
                    {
                      ?>
                      <div class="alert alert-danger" role="alert">
                        <h4><i class="icon fa fa-ban"></i>Error</h4>
                        <?php echo $mensaje; ?>
                      </div>
                      <?php
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
    <script src="<?php echo base_url();?>public/assets/js/vendor/jquery-2.1.4.min.js"></script>
    <script src="<?php echo base_url();?>public/assets/js/popper.min.js"></script>
    <script src="<?php echo base_url();?>public/assets/js/plugins.js"></script>
    <script src="<?php echo base_url();?>public/assets/js/main.js"></script>
</body>
</html>
