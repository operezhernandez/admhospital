<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Mi perfil</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li><a>Personal tecnico</a></li>
                    <li><a>Mi perfil</a></li>
                    <li class="active">Actividades</li>
                </ol>
            </div>
        </div>
    </div>
</div>

<div class="content mt-3">
  <?php
  if(isset($mensaje))
  {
    ?>
    <div class='<?php
    if($error==false)
    {
      echo "alert alert-info alert-dismissible";
    }else{

      echo "alert alert-danger alert-dismissible";

    } ?>'>
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
          <h4><i class="icon fa fa-info"></i> Mensaje</h4>
          <?php echo $mensaje; ?>
        </div>
        <?php
    }
?>
    <div class="animated fadeIn">
        <div class="row">
          <!-- perfil con foto -->
          <?php
            foreach($usuario as $usu)
            {
          ?>
            <div class="col-md-3">

                <div class="card">
                    <div class="card-header">
                        <strong class="card-title mb-3">Perfil</strong>
                    </div>
                    <div class="card-body">
                        <div class="mx-auto d-block">
                            <img class="rounded-circle mx-auto d-block" src="<?php echo base_url();?>public/photos/<?php echo $this->session->userdata("s_foto");?>" alt="Card image cap">
                            <h5 class="text-sm-center mt-2 mb-1"><?php echo $usu->nombre ?></h5>
                        </div>
                        <hr>
                    </div>
                </div>

                <aside class="profile-nav alt">
                    <section class="card">
                        <div class="card-header user-header alt bg-dark">
                            <div class="media">
                                <div class="media-body">
                                    <h2 class="text-light display-6"><?php echo $usu->puesto ?></h2>
                                </div>
                            </div>
                        </div>


                        <ul class="list-group list-group-flush">
                            <li class="list-group-item">
                                <a> <i class="fa fa-share"></i> Mantenimientos solicitados <span class="badge badge-primary pull-right"><?php echo $solicitados->solicitado ?></span></a>
                            </li>
                            <li class="list-group-item">
                                <a> <i class="fa fa-clock-o"></i> Mantenimientos pendientes <span class="badge badge-warning pull-right"><?php echo $enproceso->proceso ?></span></a>
                            </li>
                            <li class="list-group-item">
                                <a> <i class="fa fa-flag-checkered"></i> Mantenimientos finalizados <span class="badge badge-success pull-right"><?php echo $finalizados->finalizado ?></span></a>
                            </li>
                        </ul>

                    </section>
                </aside>

                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Información del tecnico</strong>
                    </div>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">
                            <a> <i class="fa fa-user"></i> Nombre: <span class="pull-right"><?php echo $usu->nombre ?></span></a>
                        </li>
                        <li class="list-group-item">
                            <a> <i class="fa fa-user"></i> Username <span class="pull-right"><?php echo $usu->username ?></span></a>
                        </li>
                        <li class="list-group-item">
                            <a> <i class="fa fa-sort-amount-asc"></i> Area <span class="pull-right"><?php echo $usu->area ?></span></a>
                        </li>
                        <li class="list-group-item">
                            <a> <i class="fa fa-users"></i> Tipo usuario
                              <span class="pull-right">
                              <?php if ($usu->idtipouser==1) {
                                echo "Medico";
                              }elseif ($usu->idtipouser==2) {
                                echo "Administrador";
                              }
                              elseif ($usu->idtipouser==3) {
                                echo "Tecnico";
                              }
                              ?>
                            </span></a>
                        </li>
                        <li class="list-group-item">
                            <a> <i class="fa fa-phone-square"></i> Telefono <span class="pull-right"><?php echo $usu->telefono ?></span></a>
                        </li>
                    </ul>
                </div>
            </div>
            <?php
            }
            ?>

            <div class="col-md-9">
              <div class="card">
                  <div class="card-header">
                      <strong class="card-title mb-3">Actividades a realizar</strong>
                  </div>

                  <div class="card-body">
                    <table class="table table-striped table-bordered" style="font-size:14px"  id="mitable">
                          <thead>
                              <tr>
                              <th>ID</th>
                              <th>Fecha</th>
                              <th>Usuario</th>
                              <th>Area</th>
                              <th>Tipo mantenimiento</th>
                              <th>Estado</th>
                              <th>Controles</th>
                              </tr>
                          </thead>
                          <tbody>

                              <?php
                                foreach($ticket as $row)
                                {
                              ?>
                              <tr>
                              <td><?php echo $row->idticket ?></td>
                              <td><?php echo $row->fechaasignacion ?></td>
                              <td><?php echo $row->nombreusu ?></td>
                              <td><?php echo $row->area ?></td>
                              <td><?php echo $row->tipomantenimiento ?></td>

                              <?php
                                if ($row->estado==1) {
                                ?>
                                <td><span class="badge badge-success pull-right">Finalizado</span></td>
                              <?php }
                                elseif ($row->estado==2) {
                                ?>
                                <td><span class="badge badge-primary pull-right">Solicitado</span></td>
                                <?php
                              }elseif ($row->estado==4) {
                                ?>
                                <td><span class="badge badge-warning pull-right">En proceso</span></td>
                                <?php
                                }
                                ?>

                                <?php if ($row->estado==1)
                                    {
                                    ?>
                                    <td>
                                      <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#exampleModal" id="btnverticket" onclick="SelidTicket(<?php echo $row->idticket;?>)" data-toggle="tooltip" title="Ver datos téccnicos"><i class="menu-icon ti-eye"></i></button>
                                      <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#exampleModal1" onclick="SelidEquipo(<?php echo $row->idinvequipo;?>)"><i class="menu-icon ti-timer" data-toggle="tooltip" title="Ver anotaciones"></i></button>
                                    </td>
                                    <?php
                                  }elseif ($row->estado==2)
                                      {
                                      ?>
                                      <td>
                                        <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#exampleModal" id="btnverticket" onclick="SelidTicket(<?php echo $row->idticket;?>)" data-toggle="tooltip" title="Ver datos téccnicos"><i class="menu-icon ti-eye"></i></button>
                                        <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#exampleModal1" onclick="SelidEquipo(<?php echo $row->idinvequipo;?>)" data-toggle="tooltip" title="Ver anotaciones"><i class="menu-icon ti-timer"></i></button>
                                        <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#modalEmpezar" onclick="SelIdEquipo(<?php echo $row->idticket;?>)" data-toggle="tooltip" title="Iniciar OT"><i class="menu-icon ti-control-play"></i></button>
                                      </td>
                                      <?php
                                    } elseif ($row->estado==4) {
                                      ?>
                                      <td>
                                        <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#exampleModal" id="btnverticket" onclick="SelidTicket(<?php echo $row->idticket;?>)" data-toggle="tooltip" title="Ver datos téccnicos"><i class="menu-icon ti-eye"></i></button>
                                        <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#exampleModal1" onclick="SelidEquipo(<?php echo $row->idinvequipo;?>)" data-toggle="tooltip" title="Ver anotaciones"><i class="menu-icon ti-timer" data-toggle="tooltip" title="Ver anotaciones"></i></button>
                                        <a href="<?php echo base_url(); ?>index.php/CUsuario/FinalizarTicket/<?php echo $row->idticket ?>" class="btn btn-warning btn-sm" data-toggle="tooltip" title="Finalizar OT"><i class="menu-icon ti-flag"></i></a><hr>
                                      </td>
                                      <?php
                                    }
                                      ?>

                              </tr>
                              <?php
                                }
                              ?>

                          </tbody>
                      </table>
                  </div>
              </div>
            </div>

        </div><!-- .row -->
    </div><!-- .animated -->
</div>

<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
});
</script>


<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Infomación general de la Orden de trabajo</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" style="font-size:12px">

        <div class="list-group">
          <a class="list-group-item list-group-item-action flex-column align-items-start">
            <div class="d-flex w-100 justify-content-between">
              <h5 class="mb-1">Nombre usuario solicitante</h5>
            </div>
            <p class="mb-1" id="nombreusu"></p>
          </a>
        </div>

        <div class="list-group">
          <a class="list-group-item list-group-item-action flex-column align-items-start">
            <div class="d-flex w-100 justify-content-between">
              <h5 class="mb-1">Area</h5>
            </div>
            <p class="mb-1" id="area"></p>
          </a>
        </div>

        <div class="list-group">
          <a class="list-group-item list-group-item-action flex-column align-items-start">
            <div class="d-flex w-100 justify-content-between">
              <h5 class="mb-1">Telefono</h5>
            </div>
            <p class="mb-1" id="telefono"></p>
          </a>
        </div>

        <div class="list-group">
          <a class="list-group-item list-group-item-action flex-column align-items-start">
            <div class="d-flex w-100 justify-content-between">
              <h5 class="mb-1">Equipo</h5>
            </div>
            <p class="mb-1" id="equipo"></p>
          </a>
        </div>

        <div class="list-group">
          <a class="list-group-item list-group-item-action flex-column align-items-start">
            <div class="d-flex w-100 justify-content-between">
              <h5 class="mb-1">Tipo mantenimiento</h5>
            </div>
            <p class="mb-1" id="tipomantenimiento"></p>
          </a>
        </div>

        <div class="list-group">
          <a class="list-group-item list-group-item-action flex-column align-items-start">
            <div class="d-flex w-100 justify-content-between">
              <h5 class="mb-1">Fecha que se asigno</h5>
            </div>
            <p class="mb-1" id="fechaasignacion"></p>
          </a>
        </div>

        <div class="list-group">
          <a class="list-group-item list-group-item-action flex-column align-items-start">
            <div class="d-flex w-100 justify-content-between">
              <h5 class="mb-1">Descripcion del problema</h5>
            </div>
            <p class="mb-1" id="descripcion"></p>
          </a>
        </div>

        <div class="list-group">
          <a class="list-group-item list-group-item-action flex-column align-items-start">
            <div class="d-flex w-100 justify-content-between">
              <h5 class="mb-1">Informe del tecnico</h5>
            </div>
            <p class="mb-1" id="informe"></p>
          </a>
        </div>

        <div class="list-group">
          <a class="list-group-item list-group-item-action flex-column align-items-start">
            <div class="d-flex w-100 justify-content-between">
              <h5 class="mb-1">Comision por orden terminada</h5>
            </div>
            <p class="mb-1" id="salario"></p>
          </a>
        </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>


<!-- Modal -->
<div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Observaciones del equipo</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body" style="font-size:12px">

          <div class="list-group" id="listaobservaciones">
          </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal para empezar el ticket -->
<div class="modal fade" id="modalEmpezar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <form action="<?php echo base_url();?>CUsuario/empezarTicket" method="post">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Empezar orden de trabajo</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body" style="font-size:12px">
          <div class="card bg-success">
              <div class="card-body">
                  <blockquote class="blockquote mb-0 text-light">
                      <input type="hidden" name="midEquipo" id="midEquipo">
                      <p class="text-light">¿Esta seguro que desea empezar esta orden de trabajo?</p>
                  </blockquote>
              </div>
          </div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-success pull-left" id="btnEmpezarT">Empezar orden</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
    </form>
  </div>
</div>

<!-- Modal para terminar el ticket -->
<div class="modal fade" id="modalTerminar">
    <div class="modal-dialog" role="document">
      <form action="" method="post">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Terminar orden de trabajo</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="row form-group">
            <div class="col col-md-3"><label for="textarea-input" class=" form-control-label">Informe del tecnico</label></div>
            <div class="col-12 col-md-9"><textarea name="textarea-input" id="informe" rows="9" placeholder="Comentarios..." class="form-control" style="margin-top: 0px; margin-bottom: 0px; height: 121px;"></textarea></div>
          </div>
          <br>
          <br>
          <div class="row form-group">
            <div class="col col-md-7"><label for="textarea-input" class=" form-control-label">Materiales utilizados para estea ticket</label></div>
            <div class="col col-md-4"><button type="button" name="addMateriales" id="addMateriales" class="btn btn-info" data-toggle="modal" data-target="#modalInventario">Agregar del inventario</button></div>
          </div>
          <div class="row form-group">
            <div class="col col-md-1"></div>
            <div class="col col-md-10">
              <ul class="list-group" id="listMateriales">
                <li class="list-group-item list-group-item-info">Tornillo goloso <span class="badge badge-warning pull-right">Cantidad: 10</span></li>
              </ul>
            </div>
            <div class="col col-md-1"></div>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success pull-left" id="btnEmpezarT">Terminar ticket</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
    </form>
  </div>
</div>

<!-- Modal para Agregar herrmainetas de inventario -->
<div class="modal fade" id="modalInventario">
    <div class="modal-dialog" role="document">
      <form action="" method="post">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Materiales de inventario</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="row form-group">
            <div class="col col-md-3"><label for="textarea-input" class=" form-control-label">Material</label></div>
            <div class="col col-md-9">
              <select name="cmbMateriales" id="cmbMateriales" class="form-control">
              </select>
            </div>
          </div>
          <br>
          <div class="row form-group">
            <div class="col col-md-3"><label for="textarea-input" class=" form-control-label">Cantidad</label></div>
            <div class="col col-md-9"><input type="number" name="Cantidad" id="Cantidad" value="" ></div>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success pull-left" id="btnAddMaterial">Agregar</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal" id="btnCerrarMaterial">Cerrar</button>
      </div>
    </div>
    </form>
  </div>
</div>
