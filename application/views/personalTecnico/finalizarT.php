<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Finalizar orden de trabajo</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li class="active">Orden de trabajo</li>
                </ol>
            </div>
        </div>
    </div>
</div>

    <div class="content mt-12">
        <div class="col-md-5">
                <div class="card">
                        <div class="card-header">
                          <strong>Información general de la orden de trabajo</strong>
                        </div>
                        <div class="card-body card-block">
                          <form action="" method="post" enctype="multipart/form-data" class="form-vertical">
                            <div class="row form-group">
                              <div class="col col-md-3"><label class=" form-control-label">id Orden</label></div>
                              <div class="col-12 col-md-9">
                                <p class="form-control-static"><?php echo $infot->idticket ?></p>
                              </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3">
                                  <label for="text-input" class=" form-control-label">Equipo</label>
                                </div>
                              <div class="col-12 col-md-9">
                                  <input type="text" id="text-input" name="text-input" placeholder="Equipo" value="<?php echo $infot->nombre ?>" disabled class="form-control">
                                  <small class="form-text text-muted">Equipo a reparar</small></div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3">
                                  <label for="email-input" class=" form-control-label">Usuario</label>
                                </div>
                                <div class="col-12 col-md-9">
                                  <input type="text" id="usuarioDet" name="usuarioDet" placeholder="Usuario" value="<?php echo $infot->name ?>" disabled class="form-control">
                                  <small class="help-block form-text">Usuario que detecto la falla</small>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3">
                                  <label for="password-input" class=" form-control-label">Descripción</label>
                                </div>
                              <div class="col-12 col-md-9">
                                    <textarea class="form-control" id="descripcion" rows="5" disabled><?php echo $infot->descripcion ?></textarea>
                                  <small class="help-block form-text">Descripción</small></div>
                            </div>
                            <div class="row form-group">
                              <div class="col col-md-3"><label for="disabled-input" class=" form-control-label">Fecha</label></div>
                              <div class="col-12 col-md-9"><input type="text" disabled id="disabled-input" name="disabled-input" value="<?php echo $infot->fechainicio ?>" placeholder="Fecha" disabled="" class="form-control"></div>
                            </div>

                          </form>
                        </div>
                        <div class="card-footer">
                        </div>
                      </div>
        </div>

        <form action="<?php echo base_url(); ?>index.php/Ctickets/materialesInventario" id="formNuevo" method="POST" onsubmit="return functionAgregar();" role="form">
        <div class="col-md-7">
          <div class="card">
            <div class="card-header">
              <strong>Terminar orden de trabajo</strong>
            </div>
            <div class="card-body card-block">
              <input type="hidden" name="idticket" value="<?php echo $infot->idticket ?>">
              <input type="hidden" name="idmantenimiento" value="<?php echo $infot->idmantenimiento ?>">
                <div class="col col-md-3"><label for="textarea-input" class=" form-control-label">Informe del tecnico</label></div>
                <div class="col-12 col-md-9"><textarea name="informe" id="informe" rows="9" placeholder="Comentarios..." class="form-control" style="margin-top: 0px; margin-bottom: 0px; height: 121px;"></textarea></div>
              </div>
              <br>
              <br>
              <div class="row form-group">
                <div class="col col-md-1"></div>
                <div class="col col-md-6"><label for="textarea-input" class=" form-control-label">Materiales utilizados para esta orden</label></div>
                <div class="col col-md-4"><button type="button" name="addMateriales" id="addMateriales" class="btn btn-info" data-toggle="modal" data-target="#modalInventario">Agregar del inventario</button></div>
              </div>
              <div class="row form-group">
                <div class="col col-md-3"></div>
                <div class="col col-md-6">
                  <ul class="list-group" id="listMateriales">

                  </ul>
                </div>
                <div class="col col-md-3"></div>
              </div>

              <div class="modal-footer">
                <button type="submit" class="btn btn-success pull-left" id="btnEmpezarT">Terminar orden</button>
                <a href="<?php echo base_url(); ?>index.php/CUsuario/verPerfilTecnico" class="btn btn-danger">Regresar</a><hr>
              </div>

            </div>
            <div class="card-footer">

            </div>
          </div>
          </form>

        </div>
    </div>

    <div class="modal fade" id="modalInventario">
        <div class="modal-dialog" role="document">
          <form action="" method="post">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Materiales de inventario</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="row form-group">
                <div class="col col-md-3"><label for="textarea-input" class=" form-control-label">Material</label></div>
                <div class="col col-md-9">
                  <select name="cmbMateriales" id="cmbMateriales" class="form-control">
                  </select>
                </div>
              </div>
              <br>
              <div class="row form-group">
                <div class="col col-md-3">
                  <label for="tiempomantprev" class=" form-control-label">Cantidad</label>
                </div>
                <div class="col-12 col-md-9">
                  <input type="number" name="Cantidad" id="Cantidad" class="form-control" min="1" max="5">
                    <small>Cantidad de material utilizado</small>
                  </div>
              </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-success pull-left" id="btnAddMaterial" data-dismiss="modal">Agregar</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
          </div>
        </div>
        </form>
      </div>
    </div>
