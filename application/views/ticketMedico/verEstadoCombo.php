<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Verificar Estado</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li class="active">Equipos</li>
                </ol>
            </div>
        </div>
    </div>
</div>

    <div class="content mt-12">
        <div class="col-md-8">
                <div class="card">
                        <div class="card-header">
                          <strong>Información</strong>
                        </div>
                        <div class="card-body card-block">
                        <div id="listadoEquiposEstado">
                    <input class="search form-control" placeholder="Buscar" /><hr>
                    <ul class="list-group list">
                        <?php
                          if(isset($equipos))
                          {
                            foreach($equipos as $tec)
                            {?>
                               <li class="list-group-item ">
                                <p >ID: <strong class="id"><?php echo $tec->idinvequipo.'-'.$tec->idequipo ?></strong></p>
                                 <p >Nombre: <strong class="nombre"><?php echo $tec->nombre ?></strong></p>
                                 <p class="">Marca: <strong class="marca"><?php echo $tec->marca ?></strong></p>
                                 <p class="">Estado: <strong class="estado">
                                    <?php switch($tec->estado ){case 0:echo '<span class="badge badge-dark">Pendiente</span>'; break;
                                                                    case 1: echo '<span class="badge badge-success">Finalizado</span>'; break;
                                                                    case 2: echo '<span class="badge badge-info">Solicitado</span>'; break;
                                                                    case 3: echo '<span class="badge badge-danger">Cancelado</span>'; break;
                                                                    case 4: echo '<span class="badge badge-warning">En Proceso</span>'; break;} ?>
                                 </strong></p>
                                 <p class="">Modelo: <strong class="modelo"><?php echo $tec->modelo ?></strong></p>
                                </li>

                           <?php }
                          }
                        ?>
                    </ul>
                    <nav aria-label="Page navigation example">
                    <ul class="pagination"></ul>
                        </nav>
                </div>





                        </div>
                        <div class="card-footer">

                        </div>
                      </div>
        </div>
    </div>
