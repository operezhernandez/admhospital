<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Añadir Observaciones</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li class="active">Observaciones</li>
                </ol>
            </div>
        </div>
    </div>
</div>

    <div class="content mt-12">
        <div class="col-md-12">
                <div class="card">
                        <div class="card-header">
                          <strong>Información</strong>
                          <?php
                            if(isset($_SESSION["insert"]))
                            {
                                if($_SESSION["insert"]==true)
                            {
                                ?>
                                <div class="alert alert-success alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <strong>Observación </strong>Realizada con exito.
                                </div>
                                <?php
                            }
                            }
                          ?>
                        </div>
                        <div class="card-body card-block">
                            <table id="tablaObservaciones" class="table table-striped table-bordered" >
                                <thead>
                                    <tr>
                                        <th >ID</th>
                                        <th scope="col">Area</th>
                                        <th>Nombre</th>
                                        <th>Control</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if(isset($inventario))
                                    {
                                        foreach($inventario as $pro)
                                        {

                                            echo '<tr>';
                                            echo '<td>'.$pro->idinvequipo.'-'.$pro->idequipo.'</td>';
                                            echo '<td>'.$pro->sede.'</td>';
                                            echo '<td>'.$pro->nombre.'</td>';
                                            echo '<td><a href="'.base_url().'/Ctickets/addObservacionesEquipo/'.$pro->idinvequipo.'" class="btn btn-primary btn-sm">Añadir</a></td>';
                                            echo '</tr>';
                                        }
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                        <div class="card-footer">


                        </div>
                </div>
        </div>
    </div>
