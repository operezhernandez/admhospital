<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Generar Orden de trabajo</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li class="active">Ordenes de trabajo</li>
                </ol>
            </div>
        </div>
    </div>
</div>

    <div class="content mt-12">
        <div class="col-md-12">
                <div class="card">
                        <div class="card-header">
                          <strong>Listado</strong>
                          <?php
                            if(isset($_SESSION["insert"]))
                            {
                                if($_SESSION["insert"]==true)
                            {
                                ?>
                                <div class="alert alert-success alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <strong>Orden de trabajo </strong>Realizada con exito!
                                </div>
                                <?php
                            }
                            }
                          ?>
                        </div>
                        <div class="card-body card-block">
                        <table class="table table-striped table-bordered"  id="tablaInventario">
                             <thead>
                                 <tr>
                                 <th >ID</th>
                                 <th scope="col">Nombre</th>
                                 <th scope="col">Marca</th>
                                 <th scope="col">Serie</th>
                                 <th scope="col"> Modelo</th>
                                <th scope="col">Tipo</th>
                                 <th scope="col">Clasificación</th>
                                 <th scope="col">Detalles</th>

                                 </tr>
                             </thead>
                             <tbody>
                             <?php
                             if(isset($equipos))
                             {
                                 foreach($equipos as $equipo)
                                 {

                                     echo '<tr>';
                                     echo '<td >'.$equipo->idequipo.'-'.$equipo->idinvequipo.'</td>';
                                     echo '<td>'.$equipo->nombre.'</td>';
                                     $newtext = wordwrap($equipo->marca, 15, "<br>",true);
                                     echo '<td >'.$equipo->marca.'</td>';
                                     echo '<td>'.$equipo->serie.'</td>';
                                     echo '<td>'.$equipo->modelo.'</td>';

                                    echo '<td>'.$equipo->tipoequipo.'</td>';
                                    echo '<td>'.$equipo->clasificacion.'</td>';
                                    echo '<td>
                                            <a 
                                            href="'.base_url().'index.php/Ctickets/generarTicketEquipo/'.$equipo->idinvequipo.'" class="btn btn-primary btn-sm" >
                                            Generar
                                            </a></td>';
                                    echo '</tr>';
                                 }
                             }

                         ?>





                             </tbody>
                         </table>
                        </div>
                        <div class="card-footer">

                        </div>
                      </div>
        </div>
    </div>
