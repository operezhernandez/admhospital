

<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Inicio</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li class="active">Panel de control</li>
                </ol>
            </div>
        </div>
    </div>
</div>

<div class="content mt-3">

    <div class="col-xl-3 col-lg-6">
        <div class="card">
            <div class="card-body">
                <div class="stat-widget-one">
                    <div class="stat-icon dib"><i class="ti-face-smile text-success border-success"></i></div>
                    <div class="stat-content dib">
                        <div class="stat-text">OT terminadas</div>
                        <div class="stat-digit">
                            <?php foreach ($tickets as $t): ?>
                              <h3><?php echo $t->nTickets;?></h3>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="col-xl-3 col-lg-6">
        <div class="card">
            <div class="card-body">
                <div class="stat-widget-one">
                    <div class="stat-icon dib"><i class="ti-time text-warning border-warning"></i></div>
                    <div class="stat-content dib">
                        <div class="stat-text">OT en proceso</div>
                        <div class="stat-digit">
                          <?php foreach ($proceso as $t): ?>
                            <h3><?php echo $t->nProceso;?></h3>
                          <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xl-3 col-lg-6">
        <div class="card">
            <div class="card-body">
                <div class="stat-widget-one">
                    <div class="stat-icon dib"><i class="ti-close text-info border-info"></i></div>
                    <div class="stat-content dib">
                        <div class="stat-text">OT solicitadas</div>
                        <div class="stat-digit">
                          <?php foreach ($solicitadas as $t): ?>
                            <h3><?php echo $t->nSolicitada;?></h3>
                          <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xl-3 col-lg-6">
        <div class="card">
            <div class="card-body">
                <div class="stat-widget-one">
                    <div class="stat-icon dib"><i class="ti-shopping-cart-full text-primary border-primary"></i></div>
                    <div class="stat-content dib">
                        <div class="stat-text">Elementos inventario</div>
                        <div class="stat-digit">
                          <?php foreach ($inventario as $t): ?>
                            <h3><?php echo $t->nInventario;?></h3>
                          <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-1"></div>
    <div class="col-lg-10">

        <div class="card">
            <div class="card-header">
                <strong>Resumen mensual</strong>
                <div class="box-tools pull-right">
                      <select name="year" id="year" class="form-control">
                        <?php foreach ($years as $year): ?>
                          <option value="<?php echo $year->year;?>"><?php echo $year->year;?></option>
                        <?php endforeach; ?>
                      </select>
                    </div>
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-md-12">
                    <div id="grafica">

                    </div>
                </div>
              </div>
            </div>
        </div>

    </div>

    <div class="col-lg-6">

        <div class="card">
            <div class="card-header">
                <strong>Información OT - Resumen Anual</strong>
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-md-12">
                  <canvas id="cotizacionesChart" style="height: 172px; width: 344px;"></canvas>
                </div>
              </div>
            </div>
        </div>

    </div>
    <div class="col-lg-6">

      <div class="card">
          <div class="card-header">
              <strong>Estadisticas de desempeño - Resumen Anual(OT finalizadas por tecnico)</strong>
          </div>
          <div class="card-body">
            <div class="row">
              <div class="col-md-12">
                <canvas id="CotizacionesUsuarioChart" style="height: 172px; width: 344px;"></canvas>
              </div>
            </div>
          </div>
      </div>

    </div>


</div> <!-- .content -->
</div><!-- /#right-panel -->

<!-- Right Panel -->

<script type="text/javascript">
function graficar(meses, tickets,year){
  Highcharts.chart('grafica', {
          chart: {
            type: 'column'
          },
          title: {
            text: 'OT finalizadas por mes'
          },
          subtitle: {
            text: 'Año '+ year
          },
          xAxis: {
            categories: meses,
              crosshair: true
            },
            yAxis: {
              min: 0,
              title: {
                text: 'OT finalizadas'
              }
            },
            tooltip: {
              headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
              pointFormat: '<tr><td style="color:{series.color};padding:0">OTs: </td>' +
                '<td style="padding:0"><b>{point.y}</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
              },
              plotOptions: {
                column: {
                  pointPadding: 0.2,
                  borderWidth: 0
                },
                series:{
                  dataLabels:{
                    enabled:true,
                    formatter:function(){
                      return Highcharts.numberFormat(this.y,1)
                    }
                  }
                }
              },
              series: [{
                name: 'Meses',
                data: tickets,
                color: '#00a65a'
              }],

            });
}
</script>
