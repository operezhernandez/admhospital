

</div><!-- /#right-panel -->

    <!-- jQuery 3 -->
    <script src="<?php echo base_url(); ?>public/bower_components/jquery/dist/jquery.min.js"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="<?php echo base_url(); ?>public/bower_components/jquery-ui/jquery-ui.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js"></script>
    <script src="<?php echo base_url();?>public/assets/js/plugins.js"></script>
    <script src="<?php echo base_url();?>public/assets/js/main.js"></script>

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>public/assets/js/datepicker.js"></script>


    <!--  Data Table -->
    <script src="<?php echo base_url();?>public/assets/js/lib/data-table/datatables.min.js"></script>
    <script src="<?php echo base_url();?>public/assets/js/lib/data-table/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>public/assets/js/lib/data-table/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url();?>public/assets/js/lib/data-table/buttons.bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>public/assets/js/lib/data-table/jszip.min.js"></script>
    <script src="<?php echo base_url();?>public/assets/js/lib/data-table/pdfmake.min.js"></script>
    <script src="<?php echo base_url();?>public/assets/js/lib/data-table/vfs_fonts.js"></script>
    <script src="<?php echo base_url();?>public/assets/js/lib/data-table/buttons.html5.min.js"></script>
    <script src="<?php echo base_url();?>public/assets/js/lib/data-table/buttons.print.min.js"></script>
    <script src="<?php echo base_url();?>public/assets/js/lib/data-table/buttons.colVis.min.js"></script>
    <script src="<?php echo base_url();?>public/assets/js/lib/data-table/datatables-init.js"></script>

     <script src="<?php echo base_url();?>public/assets/js/list.js"></script>

     <!-- highcharts -->
     <script src="<?php echo base_url(); ?>public/bower_components/highcharts/highcharts.js"></script>
     <script src="<?php echo base_url(); ?>public/bower_components/highcharts/exporting.js"></script>

     <!-- Morris.js charts -->
     <script src="<?php echo base_url(); ?>public/bower_components/raphael/raphael.min.js"></script>
     <script src="<?php echo base_url(); ?>public/bower_components/morris.js/morris.min.js"></script>

     <!--ChartJS Plugin -->
<script src="<?php echo base_url(); ?>public/bower_components/chart.js/Chart.js"></script>

        <script>
            var options = {
            valueNames: [ 'nombre','area','puesto','sede' ],page: 3,
  pagination: true
            };
            var hackerList = new List('listaUsuarioTicket', options);

          var listaEstado= new List('listadoEquiposEstado',{
              valueNames: ['nombre','marca','estado','modelo','id'],
              page:4,
              pagination:true
          });
        </script>

    <script src="<?php echo base_url();?>public/plugins/select/bootstrap-select.js"></script>

    <script>
       $(document).ready( function () {

            $('#mitable').DataTable();
            $('#tablaInventario').DataTable();
           $("#tablaUsuario").DataTable();
           $("#tablaObservacionesAdmin").DataTable();
            //SELECT
            $('.selectpicker').selectpicker();
        } );


    </script>

    <script type="text/javascript">
      var baseurl="<?php echo base_url(); ?>";
    </script>

      <?php   if ($this->uri->segment(1)=='inicio') {?>
        <script src="<?php echo base_url(); ?>public/assets/js/auditoria.js"></script>

        <script>
        function datagrafico(base_url,year){
          namesMonth = ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'];
          $.ajax({
            url: base_url +'index.php/CInicio/getDataGrafico',
            type: 'POST',
            data: {year: year},
            dataType: 'json',
            beforeSend: function(){
              },
              success: function(data){
                var meses= new Array();
                var tickets= new Array();
                $.each(data,function(key,value){
                  meses.push(namesMonth[value.mes-1]);
                  valor=Number(value.tickets);
                  tickets.push(valor);
                });
                graficar(meses,tickets,year);
              }
            });
          }

          var year=(new Date).getFullYear();
          datagrafico(baseurl,year);
          $('#year').on('change',function(){
            yearselect = $(this).val();
            datagrafico(baseurl,yearselect);
          });
          </script>
        <?php }?>

    <?php if ($this->uri->segment(2)=='verPerfilTecnico') {?>


      <script src="<?php echo base_url(); ?>public/assets/js/perfil.js"></script>
    <?php } ?>

    <?php if ($this->uri->segment(2)=='FinalizarTicket') {?>
      <script src="<?php echo base_url(); ?>public/assets/js/perfil.js"></script>
    <?php } ?>

    <?php if ($this->uri->segment(2)=='modificarUsuario') {?>
      <script src="<?php echo base_url(); ?>public/assets/js/usuariomod.js"></script>
    <?php } ?>



</body>
</html>
