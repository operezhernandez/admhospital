    <!-- Right Panel -->

    <div id="right-panel" class="right-panel">

<!-- Header-->
<header id="header" class="header">

    <div class="header-menu">

        <div class="col-sm-7">
        </div>

        <div class="col-sm-5">
            <div class="user-area dropdown float-right">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <img class="user-avatar rounded-circle" src="<?php echo base_url();?>public/photos/<?php echo $this->session->userdata("s_foto") ?>" alt="User Avatar">
                    <?php echo date('D d-m-Y h:i   ').'<br>'; ?>
                </a>

                <div class="user-menu dropdown-menu">


                        <a class="nav-link" href="<?php echo base_url();?>/logout"><i class="fa fa-power -off"></i>Cerrar Sesión</a>
                </div>
            </div>
        </div>
    </div>
</header><!-- /header -->
<!-- Header-->
