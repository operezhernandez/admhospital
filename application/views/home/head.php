<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Hospital Solutions</title>
    <meta name="description" content="Sufee Admin - HTML5 Admin Template">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="apple-icon.png">
    <link rel="shortcut icon" href="favicon.ico">

    <link rel="stylesheet" href="<?php echo base_url();?>public/assets/css/normalize.css">
    <link rel="stylesheet" href="<?php echo base_url();?>public/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>public/assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>public/assets/css/themify-icons.css">
    <link rel="stylesheet" href="<?php echo base_url();?>public/assets/css/flag-icon.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>public/assets/css/cs-skin-elastic.css">
    <link rel="stylesheet" href="<?php echo base_url();?>public/assets/css/bootstrap-select.css">
    <link rel="stylesheet" href="<?php echo base_url();?>public/assets/css/lib/datatable/dataTables.bootstrap.min.css">
    <!-- Morris chart -->
<link rel="stylesheet" href="<?php echo base_url(); ?>public/bower_components/morris.js/morris.css">



    <link rel="stylesheet" href="<?php echo base_url();?>public/assets/css/bootstrap-select.css">
   <link rel="stylesheet" href="<?php echo base_url();?>public/assets/scss/style.css">
    <link href="<?php echo base_url();?>public/assets/css/lib/vector-map/jqvmap.min.css" rel="stylesheet">

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>

    <!-- <script type="text/javascript" src="https://cdn.jsdelivr.net/html5shiv/3.7.3/html5shiv.min.js"></script> -->

    <style>
        dropdown-toggle.btn-default {
  color: #292b2c;
  background-color: #fff;
  border-color: #ccc;
}

.bootstrap-select.show>.dropdown-menu>.dropdown-menu {
  display: block;
}

.bootstrap-select > .dropdown-menu > .dropdown-menu li.hidden{
  display:none;
}

.bootstrap-select > .dropdown-menu > .dropdown-menu li a{
  display: block;
  width: 100%;
  padding: 3px 1.5rem;
  clear: both;
  font-weight: 400;
  color: #292b2c;
  text-align: inherit;
  white-space: nowrap;
  background: 0 0;
  border: 0;
}

.dropdown-menu > li.active > a {
  color: #fff !important;
  background-color: #337ab7 !important;
}

.bootstrap-select .check-mark::after {
  content: "✓";
}

.bootstrap-select button {
  overflow: hidden;
  text-overflow: ellipsis;
}


/* Make filled out selects be the same size as empty selects */
.bootstrap-select.btn-group .dropdown-toggle .filter-option {
  display: inline !important;
}

        </style>
</head>
<body>
