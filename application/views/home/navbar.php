 <!-- NavBar -->

    <aside id="left-panel" class="left-panel">
        <nav class="navbar navbar-expand-sm navbar-default">

            <div class="navbar-header">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fa fa-bars"></i>
                </button>
               <!-- <a class="navbar-brand" href="./"><img src="images/logo.png" alt="Logo"></a>
                <a class="navbar-brand hidden" href="./"><img src="images/logo2.png" alt="Logo"></a> -->
            </div>

            <div id="main-menu" class="main-menu collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li class="active">
                        <a href="<?php echo base_url();?>index.php/inicio"> <i class="menu-icon fa fa-dashboard"></i>Hospital Solutions</a>
                    </li>
                   <?php
                    if($this->session->userdata('s_tipoUser')==2)
                    {
                        ?>
                    <h3 class="menu-title">Administrador</h3><!-- /.menu-title -->
                    <li>
                        <a href="<?php echo base_url();?>index.php/Ctickets/ticketsAdmin"> <i class="menu-icon ti-write"></i>Ordenes de trabajos </a>
                    </li><!--
                    <li>
                        <a href="<?php //echo base_url();?>index.php/Ctickets/observacionesAdmin"> <i class="menu-icon ti-write"></i>Observaciones </a>
                    </li>-->
                    <li>
                        <a href="<?php echo base_url();?>index.php/CCalendario"><i class="menu-icon fa fa-calendar"></i>Calendario </a>
                    </li>
                    <li>
                        <a href="<?php echo base_url(); ?>index.php/CUsuario/usuariosAdministrador"> <i class="menu-icon ti-user"></i>Personal </a>
                    </li>
                    <li>
                        <a href="<?php echo base_url();?>index.php/CInventario/inventario "> <i class="menu-icon ti-desktop"></i>Listado equipos</a>
                    </li>
                    <li>
                        <a href="<?php echo base_url();?>index.php/CMateriales/verInventarioMateriales"> <i class="menu-icon ti-ruler-pencil"></i>Inventario de materiales</a>
                    </li>
                    <li>
                        <a href="<?php echo base_url();?>index.php/CInventario/verServicios"> <i class="menu-icon ti-eye"></i>Ver servicios</a>
                    </li>
                    <li>
                        <a href="<?php echo base_url();?>index.php/CInventario/vertipo"> <i class="menu-icon ti-menu-alt"></i>Tipos de equipos</a>
                    </li>
                    <li>
                        <a href="<?php echo base_url();?>index.php/CInventario/verArea"> <i class="menu-icon ti-tag"></i>Areas</a>
                    </li>

                        <?php
                    }
                   ?>
                   <?php
                        if($this->session->userdata('s_tipoUser')==1)
                    {
                        ?>
                        <h3 class="menu-title">Personal Medico</h3><!-- /.menu-title -->
                    <li>
                        <a href="<?php echo base_url();?>index.php/Ctickets/generarTickets"> <i class="menu-icon ti-email"></i>Crear Orden de trabajo</a>
                    </li>
                    <li>
                        <a href="<?php echo base_url();?>index.php/Ctickets/addObservaciones"> <i class="menu-icon ti-comment-alt"></i>Añadir observaciones</a>
                    </li>
                    <li>
                        <a href="<?php echo base_url();?>index.php/Ctickets/verEstado"> <i class="menu-icon ti-check-box"></i>Verificar estado</a>
                    </li>
                    <li>
                        <a href="<?php echo base_url();?>index.php/CCalendario"><i class="menu-icon fa fa-calendar"></i>Calendario </a>
                    </li>
                        <?php
                    }
                    ?>
                    <?php
                        if($this->session->userdata('s_tipoUser')==3)
                    {
                        ?>
                        <h3 class="menu-title">Personal Tecnico</h3><!-- /.menu-title -->
                    <li>
                        <a href="<?php echo base_url();?>index.php/CUsuario/verPerfilTecnico"> <i class="menu-icon ti-user"></i>Perfil y actividades</a>
                    </li>
                        <?php
                    }
                    ?>


                </ul>
            </div><!-- /.navbar-collapse -->
        </nav>
    </aside><!-- /#left-panel -->

    <!-- /NavBar -->
