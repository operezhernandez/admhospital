    var pieChartCanvas = $('#cotizacionesChart').get(0).getContext('2d');
    var pieChart       = new Chart(pieChartCanvas)
    $.ajax({
      url: baseurl+'index.php/CInicio/getEstadisticas',
      method: 'post',
      beforeSend: function(){
      },
      success: function(data) {
        data=JSON.parse(data);
        var PieData        = [
          {
            value    : data.terminadas,
            color    : '#0eaf64',
            highlight: '#0eaf64',
            label    : 'Finalizadas'
          },
          {
            value    : data.solicitadas,
            color    : '#07ADF5',
            highlight: '#07ADF5',
            label    : 'Solicitadas'
          },
          {
            value    : data.proceso,
            color    : '#f2a32e',
            highlight: '#f2a32e',
            label    : 'Proceso'
          }
        ];

        console.log(PieData);
        var pieOptions     = {
          //Boolean - Whether we should show a stroke on each segment
          segmentShowStroke    : true,
          //String - The colour of each segment stroke
          segmentStrokeColor   : '#fff',
          //Number - The width of each segment stroke
          segmentStrokeWidth   : 2,
          //Number - The percentage of the chart that we cut out of the middle
          percentageInnerCutout: 50, // This is 0 for Pie charts
          //Number - Amount of animation steps
          animationSteps       : 100,
          //String - Animation easing effect
          animationEasing      : 'easeOutBounce',
          //Boolean - Whether we animate the rotation of the Doughnut
          animateRotate        : true,
          //Boolean - Whether we animate scaling the Doughnut from the centre
          animateScale         : false,
          //Boolean - whether to make the chart responsive to window resizing
          responsive           : true,
          // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
          maintainAspectRatio  : true,
          //String - A legend template
          legendTemplate       : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<segments.length; i++){%><li><span style="background-color:<%=segments[i].fillColor%>"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>'
        }
        //Create pie or douhnut chart
        // You can switch between pie and douhnut using the method below.
        pieChart.Doughnut(PieData, pieOptions)

        $(".overlay").remove();
      },
      error: function()
      {
          alert("Error de conexión!");
      }
  });


var pieChartCanvasUsuarios = $('#CotizacionesUsuarioChart').get(0).getContext('2d');

var pieChartUsuarios       = new Chart(pieChartCanvasUsuarios)
    $.ajax({
      url: baseurl+'index.php/CInicio/getUsuarios',
      method: 'post',
      beforeSend: function(){
      },
      success: function(data) {
        data=JSON.parse(data);
        let dataUsuarios=new Array();
        data.forEach(element => {
          let color="hsl(" + 360 * Math.random() + ',' +
          (25 + 70 * Math.random()) + '%,' +
          (70 + 10 * Math.random()) + '%)';
          let nuevo={value:element.contador, color:color,highlight:color,label:element.nombre };
          dataUsuarios.push(nuevo);
        });
        var pieOptions     = {
          //Boolean - Whether we should show a stroke on each segment
          segmentShowStroke    : true,
          //String - The colour of each segment stroke
          segmentStrokeColor   : '#fff',
          //Number - The width of each segment stroke
          segmentStrokeWidth   : 2,
          //Number - The percentage of the chart that we cut out of the middle
          percentageInnerCutout: 50, // This is 0 for Pie charts
          //Number - Amount of animation steps
          animationSteps       : 100,
          //String - Animation easing effect
          animationEasing      : 'easeOutBounce',
          //Boolean - Whether we animate the rotation of the Doughnut
          animateRotate        : true,
          //Boolean - Whether we animate scaling the Doughnut from the centre
          animateScale         : false,
          //Boolean - whether to make the chart responsive to window resizing
          responsive           : true,
          // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
          maintainAspectRatio  : true,
          //String - A legend template
          legendTemplate       : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<segments.length; i++){%><li><span style="background-color:<%=segments[i].fillColor%>"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>'
        }
        //Create pie or douhnut chart
        // You can switch between pie and douhnut using the method below.
        console.log(dataUsuarios);
        pieChartUsuarios.Doughnut(dataUsuarios, pieOptions)

        $(".overlay").remove();
      },
      error: function()
      {
          alert("Error de conexión!");
      }
  });
