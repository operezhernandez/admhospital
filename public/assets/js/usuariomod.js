$(document).ready(function()
{
  //funcion para cargar datos de los cmb en vista Perfil
  infoUsuario();

});

//funcion que  de la bdd a la vista Perfil
function infoUsuario(){

  $.ajax({
      url: baseurl+'index.php/CUsuario/infoUsuario',
      method: 'post',
      data:{
        idusuario:$("#idusuariomod").val()
      },
      beforeSend: function(){
      },
      success: function(data) {
        //toda la info del user perfil
        var c=JSON.parse(data);
        //recorriendo la informacion y poniendola en los combo
        $.each(c,function(i,item){
          //cambiando cmbGenero
          $('#cmbArea').val(item.idarea);
        });
      },
      error: function()
      {
          alert("Error de conexión!");
      }
  });
}
