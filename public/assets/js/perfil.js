
var materialesBusqueda= new Array();
var materiales = new Array();
var totals;

$(document).ready(function(){
//obteniendo todos los materiales
$.ajax({
      url: baseurl+'index.php/CMateriales/getInventarioMateriales',
      method: 'post',
      beforeSend: function(){
      },
      success: function(data) {
        //todos los materiales de la base de datos
        let c=JSON.parse(data);
        materialesBusqueda=c;
      },
      error: function()
      {
          alert("Error de conexión!");
      }
  });

  //funcion para abrir modal del finalizar
    $("#btnfinalizar").click(function(){
      $("#modalTerminar").modal({
              backdrop:'static',
              keyboard: false});
    });


  //funcion para abrir modal del inventario-agregar
    $("#addMateriales").click(function(){
      MaterialesInsert();
    });

    //funcion para agregar material del inventario
      $("#btnAddMaterial").click(function(){

        if($("#cmbMateriales :selected").val()=='')
        {
         alert("Seleccione un material.");
         $("#cmbMateriales").focus();
         return false;
        }else{
            //nada
        }

        if($("#Cantidad").val()<=0)
        {
         alert("No ingresar numeros negativos.");
         $("#Cantidad").focus();
         return false;
        }else{
            //nada
        }

          materialesBusqueda.forEach(element => {

            if(element.idinventario==$("#cmbMateriales").val())
            {
              element.cantidad=$("#Cantidad").val();
              materiales.push(element);
              $("#listMateriales").append(
                '<li class="list-group-item list-group-item-info" id="'+element.idinventario+'-LiDeMateriales">'+element.nombre+'          '+
                  '<small class="badge badge-warning">Cantidad: '+$("#Cantidad").val()+'</small>'+
                  '<div class="tools">'+
                    '<i class="fa fa-trash-o pull-right eliminarRequerimientos" idElement="'+element.idinventario+'"></i>'+
                  '</div>'+
                '</li>');
              }
          });
      });

      //funcion que cambia el valor del input cantidad al mover cmbMateriales
      $("#cmbMateriales").change(function() {
        $("#Cantidad").val(0);
        $.ajax({
              url: baseurl+'index.php/CMateriales/getCantidadMaterial',
              method: 'post',
              data:
              {
                idmaterial:$("#cmbMateriales :selected").val()
              },
              beforeSend: function(){
              },
              success: function(data) {
                //cantidades de material
                let c=JSON.parse(data);
                c.forEach(element =>{
                  $("#Cantidad").attr("max",element.cantidad);
                });
              },
              error: function()
              {
                  alert("Error de conexión!");
              }
          });
      });

});

//Funcion para cargar datos del cliente en el Modal para Firmar
 SelidTicket=function(idticket){
   let total;
   $('#nombreusu').html();
   $('#area').html('');
   $('#sede').html('');
   $('#telefono').html('');
   $('#equipo').html('');
   $('#tipomantenimiento').html('');
   $('#fechaasignacion').html('');
   $('#descripcion').html('');
   $('#informe').html('');

  $.ajax({
      url: baseurl+'index.php/CUsuario/infoTicket',
      method: 'post',
      data:{
          idticket:idticket,
          },

          beforeSend: function(){
          },

          success: function(data) {
            //informacion del ticket
            var data=JSON.parse(data);

            //Peticion ajax para obtener cantidad de minutos de trabajo para comision
            $.ajax({
                url: baseurl+'index.php/CUsuario/minutosTicket',
                method: 'post',
                data:{
                    idticket:idticket,
                    },
                beforeSend: function(){
                },
                success: function(data) {
                  var c=JSON.parse(data);
                  $.each(c,function(i,item){
                    var minutos=parseInt(item.minutos);

                    $.ajax({
                        url: baseurl+'index.php/CUsuario/salarioTecnico',
                        method: 'post',
                        beforeSend: function(){
                        },
                        success: function(data) {
                          var c=JSON.parse(data);
                          $.each(c,function(i,item){
                            var salario=parseInt(item.salario);
                            totals=(salario/60)*minutos;

                            if (totals<0 || totals==null) {
                              $('#salario').html('<b style="color:#F80606">No ha finalizado la orden de trabajo.</b>');
                            }
                            else if(Number.isNaN(totals)){
                              $('#salario').html('<b style="color:#F80606">No ha finalizado la orden de trabajo.</b>');
                            }
                            else {
                              $('#salario').html('<b style="color:#F80606">$'+totals+'</b>');
                            }
                          });
                        },
                        error: function()
                        {
                            alert("Error de conexión!");
                        }
                    });


                  });
                },
                error: function()
                {
                    alert("Error de conexión!");
                }
            });

            //peticion ajax cantidad minutos
            //alert(totals);
            data.forEach(function(item,i){
              $('#nombreusu').html(item.nombreusu);
              $('#area').html(item.area);
              $('#sede').html(item.sede);
              $('#telefono').html(item.telefono);
              $('#equipo').html(item.nombreequipo);
              $('#tipomantenimiento').html(item.tipomantenimiento);
              $('#fechaasignacion').html(item.fechaasignacion);
              $('#descripcion').html(item.descripcion);
              if (item.informetecnico==null) {
                $('#informe').html('<b style="color:#F80606">Esta orden de trabajo, aun no posee un informe del tecnico.</b>');
              }
              else {
                $('#informe').html(item.informetecnico);
              }

            });

          },

          error: function()
          {
            alert("Error de conexión!");
          }
        });
  //final funcion ajax

}


//Funcion para ver historial del equipo
SelidEquipo=function(idequipo){
  $("#listaobservaciones").empty();
  $.ajax({
      url: baseurl+'index.php/CUsuario/infoEquipo',
      method: 'post',
      data:{
          idequipo:idequipo,
          },

          beforeSend: function(){
          },

          success: function(data) {
            //historial del equipo
            var data=JSON.parse(data);
            if (data=='') {
              $("#listaobservaciones").append(
                '<a class="list-group-item list-group-item-action flex-column align-items-start">'+
                  '<div class="d-flex w-100 justify-content-between">'+
                  '</div>'+
                  '<p class="mb-1"id="observacion">Este equipo no tienen ninguna observación.</p>'+
                '</a>'
              );
            }
            else {
              data.forEach(function(item,i){

                $("#listaobservaciones").append(
                  '<a class="list-group-item list-group-item-action flex-column align-items-start">'+
                    '<div class="d-flex w-100 justify-content-between">'+
                      '<small class="text-muted"id="fecha">'+item.fecha+'</small>'+
                    '</div>'+
                    '<p class="mb-1"id="observacion">'+item.observacion+'</p>'+
                  '</a>'
                );

              });
            }
          },

          error: function()
          {
            alert("Error de conexión!");
          }
        });
  //final funcion ajax
}

//Funcion poner id
SelIdEquipo=function(idequipo){
  //asignar id
  $("#midEquipo").val(idequipo);
};


//funcion para listar los materiales en el modalInventario
function MaterialesInsert(){
  $("#cmbMateriales").empty();
  $("#cmbMateriales").append("<option value =''>Elegir un servicio</option>");
  //$('#cmbMateriales').selectpicker('refresh');

  //Listar Servicios en cmbMateriales
  $.ajax({
      url: baseurl+'index.php/CMateriales/getInventarioMateriales',
      method: 'post',
      beforeSend: function(){
      },
      success: function(data) {
        //todos los materiales de la base de datos
        var c=JSON.parse(data);
        //recorriendo los materiales que estan en la base de datos
        $.each(c,function(i,item){
          var contador=0;

          //recorriendo los materiales que uso el user
          $.each(materiales,function(j,key){
            if (item.idinventario == key.idinventario){//si el servicio se encuentra en el paquete aumenta el contador
              contador++;
            }
          });
          if (contador==0) {
            $('#cmbMateriales').append('<option value="'+item.idinventario+'">'+item.nombre+'</option>');
            //$('#cmbMateriales').selectpicker('refresh');
          }
        });
      },
      error: function()
      {
          alert("Error de conexión!");
      }
  });
}

//funcion que elimina un subservicio de insertar
$("#listMateriales").on('click','.fa.fa-trash-o.eliminarRequerimientos',function(){
    let id=$(this).attr("idElement");
    materiales=materiales.filter(function(item){
        return item.idinventario != id;
    });
    $("#"+id+"-LiDeMateriales").remove();
});

//Funcion antes de enviar el form
function functionAgregar()
{
    if($("#informe").val()=="")
    {
     alert("Agregue un comentario, acerca la orden de trabajo.");
     $("#informe").focus();
     return false;
    }else   {
        //nada
    }
    if (materiales.lenght==0) {

    }else {
      materiales.forEach(element => {
          $("#formNuevo").append(
              '<input name="materiales[]" type="hidden" value="'+element.idinventario+'">'+
              '<input name="cantidades[]" type="hidden" value="'+element.cantidad+'">'
          );
      });
    }
    
}
