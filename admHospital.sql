-- MySQL dump 10.16  Distrib 10.3.9-MariaDB, for Win64 (AMD64)
--
-- Host: localhost    Database: admhospital
-- ------------------------------------------------------
-- Server version	10.3.9-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `asignacionticket`
--

DROP TABLE IF EXISTS `asignacionticket`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `asignacionticket` (
  `idasignacion` int(11) NOT NULL AUTO_INCREMENT,
  `idticket` int(11) DEFAULT NULL,
  `idusuario` int(11) DEFAULT NULL,
  `fechaAsignacion` datetime DEFAULT NULL,
  `idorden` int(11) DEFAULT NULL,
  PRIMARY KEY (`idasignacion`),
  KEY `idticket` (`idticket`),
  KEY `idusuario` (`idusuario`),
  KEY `idorden` (`idorden`),
  CONSTRAINT `asignacionticket_ibfk_1` FOREIGN KEY (`idticket`) REFERENCES `ticket` (`idticket`),
  CONSTRAINT `asignacionticket_ibfk_2` FOREIGN KEY (`idusuario`) REFERENCES `usuario` (`idusuario`),
  CONSTRAINT `asignacionticket_ibfk_3` FOREIGN KEY (`idorden`) REFERENCES `orden` (`idorden`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `asignacionticket`
--

LOCK TABLES `asignacionticket` WRITE;
/*!40000 ALTER TABLE `asignacionticket` DISABLE KEYS */;
INSERT INTO `asignacionticket` VALUES (1,1,23,'2018-11-07 03:11:19',1),(2,8,23,'2018-11-27 03:11:19',2),(3,9,23,'2018-11-17 03:11:19',3),(4,10,23,'2018-12-07 03:11:19',4),(5,11,23,'2018-12-27 03:11:19',5),(6,13,19,'2018-11-07 03:44:30',6),(7,14,26,'2018-11-07 03:44:40',7),(8,16,26,'2018-11-07 11:15:30',8),(9,15,19,'2018-11-07 12:21:38',9),(10,17,26,'2018-11-07 14:15:42',10),(11,18,19,'2018-11-07 14:16:18',11),(12,19,19,'2018-11-07 14:25:59',12);
/*!40000 ALTER TABLE `asignacionticket` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `equipo`
--

DROP TABLE IF EXISTS `equipo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `equipo` (
  `idequipo` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(260) DEFAULT NULL,
  `marca` varchar(500) DEFAULT NULL,
  `serie` varchar(500) DEFAULT NULL,
  `modelo` varchar(500) DEFAULT NULL,
  `clasificacion` varchar(500) DEFAULT NULL,
  `fabricante` varchar(500) DEFAULT NULL,
  `aniofabri` date DEFAULT NULL,
  `servicio` varchar(500) DEFAULT NULL,
  `tipoevento` varchar(500) DEFAULT NULL,
  `idespecificaciones` int(11) DEFAULT NULL,
  `idadquisicion` int(11) DEFAULT NULL,
  `tiempomantprev` int(11) DEFAULT NULL,
  `tipoequipo` varchar(250) DEFAULT NULL,
  `preventido` int(11) DEFAULT 1,
  `clasificacion2` varchar(250) DEFAULT NULL,
  `habilitado` bit(1) DEFAULT b'0',
  PRIMARY KEY (`idequipo`),
  KEY `idespecificaciones` (`idespecificaciones`),
  KEY `idadquisicion` (`idadquisicion`),
  CONSTRAINT `idadquisicion` FOREIGN KEY (`idadquisicion`) REFERENCES `formaadquisicion` (`idadquisicion`),
  CONSTRAINT `idespecificaciones` FOREIGN KEY (`idespecificaciones`) REFERENCES `especificaciones` (`idespecificaciones`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `equipo`
--

LOCK TABLES `equipo` WRITE;
/*!40000 ALTER TABLE `equipo` DISABLE KEYS */;
INSERT INTO `equipo` VALUES (2,'Tomografo ','Siemens ','55481651','Sensation','Clase 1','Siemens ','2009-01-01','8',NULL,2,2,20,'5',2,'B','\0'),(3,'Acelerador lineal ','Variant','54812135','Unique','Clase 1','Variant','2009-01-01','8',NULL,3,3,150,'9',2,'BF','\0'),(4,'Equipo de ultrasonido','PHILIPS',' 4535 611 8562','M2540A','Clase 1','PHILIPS','2015-01-01','6',NULL,4,4,20,'2',2,'BF','\0'),(5,'Cuna termica','Dräger','8004','8004','Clase 1','Dräger','2012-01-01','10',NULL,5,5,40,'2',3,'BF','\0'),(6,'Digitalizador ','Kodak','18373837','Kodak-200','Clase 1','Kodak','0000-00-00','8',NULL,6,6,60,'5',1,'B','\0');
/*!40000 ALTER TABLE `equipo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `especificaciones`
--

DROP TABLE IF EXISTS `especificaciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `especificaciones` (
  `idespecificaciones` int(11) NOT NULL AUTO_INCREMENT,
  `amperaje` varchar(50) DEFAULT NULL,
  `frecuencia` varchar(50) DEFAULT NULL,
  `potencia` varchar(50) DEFAULT NULL,
  `voltaje` varchar(50) DEFAULT NULL,
  `preinstalacion` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`idespecificaciones`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `especificaciones`
--

LOCK TABLES `especificaciones` WRITE;
/*!40000 ALTER TABLE `especificaciones` DISABLE KEYS */;
INSERT INTO `especificaciones` VALUES (1,'-','-','-','-','Apantallamiento de plomo, UPS , Red de tierra.'),(2,'-','-','-','-','-'),(3,'-','-','-','-','-'),(4,'-','60','-','120','Red de tierra '),(5,'-','60','-','120','Red de tierra '),(6,'-','60','-','120','');
/*!40000 ALTER TABLE `especificaciones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `formaadquisicion`
--

DROP TABLE IF EXISTS `formaadquisicion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `formaadquisicion` (
  `idadquisicion` int(11) NOT NULL AUTO_INCREMENT,
  `formaadquisicion` varchar(500) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `precio` decimal(10,0) DEFAULT NULL,
  `proveedor` varchar(500) DEFAULT NULL,
  `numfactura` varchar(50) DEFAULT NULL,
  `iniciogarantia` date DEFAULT NULL,
  `fingarantia` date DEFAULT NULL,
  PRIMARY KEY (`idadquisicion`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `formaadquisicion`
--

LOCK TABLES `formaadquisicion` WRITE;
/*!40000 ALTER TABLE `formaadquisicion` DISABLE KEYS */;
INSERT INTO `formaadquisicion` VALUES (1,'Compra','2018-10-07',3000000,'Promed','asd81822145','2018-11-07','2025-01-01'),(2,'Compra','2018-10-07',500000,'Promed','4815540561','2018-10-07','2025-10-07'),(3,'Compra','2018-06-10',4000000,'Promed','18714151712','2018-06-10','2025-06-10'),(4,'Compra','2018-10-07',5000,'ST medic','519156151','2018-10-07','2022-11-08'),(5,'Compra','2018-08-03',5800,'Nipro','58911417','2018-08-03','2019-08-03'),(6,'Compra','2018-10-20',6000,'Stmedic','7173833','2018-10-20','2020-10-20');
/*!40000 ALTER TABLE `formaadquisicion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inventario`
--

DROP TABLE IF EXISTS `inventario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inventario` (
  `idinventario` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(250) DEFAULT NULL,
  `cantidad` int(11) DEFAULT NULL,
  `descripcion` varchar(500) DEFAULT NULL,
  `precio` decimal(10,0) DEFAULT NULL,
  `proveedor` varchar(500) DEFAULT NULL,
  `sede` varchar(2500) DEFAULT NULL,
  PRIMARY KEY (`idinventario`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inventario`
--

LOCK TABLES `inventario` WRITE;
/*!40000 ALTER TABLE `inventario` DISABLE KEYS */;
INSERT INTO `inventario` VALUES (1,'Estaño para soldadura ',5,'5 Yardas de estaño para soldadura ',4,'Casa Rivas ',NULL),(2,'Tornillo Goloso ',1000,'Tornillo de tipo goloso una caja de 1000 unidades de 1/2 pulgada',1,'Ferreteria San Jorge',NULL),(3,'contact cleaner',5,'2 latas ',5,'Vidri',NULL),(4,'UTP',20,'20 metros de UTP',0,'Casa rivas ',NULL),(5,'Poxipol ',4,'Tubos de material ',4,'Vidri',NULL);
/*!40000 ALTER TABLE `inventario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inventarioequipo`
--

DROP TABLE IF EXISTS `inventarioequipo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inventarioequipo` (
  `idinvequipo` int(11) NOT NULL AUTO_INCREMENT,
  `idequipo` int(11) DEFAULT NULL,
  `sede` int(11) DEFAULT NULL,
  `habilitado` bit(1) DEFAULT b'1',
  PRIMARY KEY (`idinvequipo`),
  KEY `idequipo` (`idequipo`),
  CONSTRAINT `inventarioequipo_ibfk_1` FOREIGN KEY (`idequipo`) REFERENCES `equipo` (`idequipo`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inventarioequipo`
--

LOCK TABLES `inventarioequipo` WRITE;
/*!40000 ALTER TABLE `inventarioequipo` DISABLE KEYS */;
INSERT INTO `inventarioequipo` VALUES (3,2,11,''),(5,3,1,'\0'),(6,3,11,''),(7,4,12,''),(8,5,5,''),(9,3,1,'\0'),(10,6,11,''),(11,6,12,'');
/*!40000 ALTER TABLE `inventarioequipo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mantenimiento`
--

DROP TABLE IF EXISTS `mantenimiento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mantenimiento` (
  `idmantenimiento` int(11) NOT NULL AUTO_INCREMENT,
  `idticket` int(11) DEFAULT NULL,
  `idasignacion` int(11) DEFAULT NULL,
  `fechainicio` datetime DEFAULT NULL,
  `fechafinal` datetime DEFAULT NULL,
  PRIMARY KEY (`idmantenimiento`),
  KEY `idticket` (`idticket`),
  KEY `idasignacion` (`idasignacion`),
  CONSTRAINT `mantenimiento_ibfk_1` FOREIGN KEY (`idticket`) REFERENCES `ticket` (`idticket`),
  CONSTRAINT `mantenimiento_ibfk_2` FOREIGN KEY (`idasignacion`) REFERENCES `asignacionticket` (`idasignacion`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mantenimiento`
--

LOCK TABLES `mantenimiento` WRITE;
/*!40000 ALTER TABLE `mantenimiento` DISABLE KEYS */;
INSERT INTO `mantenimiento` VALUES (1,13,NULL,'2018-11-07 03:53:13','2018-11-07 03:54:28');
/*!40000 ALTER TABLE `mantenimiento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mantinvent`
--

DROP TABLE IF EXISTS `mantinvent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mantinvent` (
  `idmantenimiento` int(10) unsigned NOT NULL,
  `idinventario` int(11) DEFAULT NULL,
  `cantidad` int(11) DEFAULT NULL,
  KEY `idinventario` (`idinventario`),
  CONSTRAINT `mantinvent_ibfk_1` FOREIGN KEY (`idinventario`) REFERENCES `inventario` (`idinventario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mantinvent`
--

LOCK TABLES `mantinvent` WRITE;
/*!40000 ALTER TABLE `mantinvent` DISABLE KEYS */;
/*!40000 ALTER TABLE `mantinvent` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `observaciones`
--

DROP TABLE IF EXISTS `observaciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `observaciones` (
  `idobservacion` int(11) NOT NULL AUTO_INCREMENT,
  `idinvequipo` int(11) DEFAULT NULL,
  `observacion` varchar(1000) DEFAULT NULL,
  `visto` bit(1) DEFAULT NULL,
  `estado` bit(1) DEFAULT b'0',
  `fecha` datetime DEFAULT NULL,
  PRIMARY KEY (`idobservacion`),
  KEY `idinvequipo` (`idinvequipo`),
  CONSTRAINT `observaciones_ibfk_1` FOREIGN KEY (`idinvequipo`) REFERENCES `inventarioequipo` (`idinvequipo`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `observaciones`
--

LOCK TABLES `observaciones` WRITE;
/*!40000 ALTER TABLE `observaciones` DISABLE KEYS */;
INSERT INTO `observaciones` VALUES (1,7,'El equipo únicamente funciona cuando se fuerza el conector. ','\0','\0','2018-11-07 03:52:08'),(2,7,'El cabezal del transductor presenta un fisura','\0','\0','2018-11-07 10:24:03'),(3,11,'Las últimas semanas se apagaba constantemente ','\0','\0','2018-11-07 14:24:19');
/*!40000 ALTER TABLE `observaciones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orden`
--

DROP TABLE IF EXISTS `orden`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orden` (
  `idorden` int(11) NOT NULL AUTO_INCREMENT,
  `idusuario` int(11) DEFAULT NULL,
  PRIMARY KEY (`idorden`),
  KEY `idusuario` (`idusuario`),
  CONSTRAINT `orden_ibfk_1` FOREIGN KEY (`idusuario`) REFERENCES `usuario` (`idusuario`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orden`
--

LOCK TABLES `orden` WRITE;
/*!40000 ALTER TABLE `orden` DISABLE KEYS */;
INSERT INTO `orden` VALUES (1,2),(2,2),(3,2),(4,2),(5,2),(6,2),(7,2),(8,2),(9,2),(10,2),(11,2),(12,2);
/*!40000 ALTER TABLE `orden` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sede`
--

DROP TABLE IF EXISTS `sede`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sede` (
  `idsede` int(11) NOT NULL AUTO_INCREMENT,
  `sede` varchar(250) DEFAULT NULL,
  `habilitado` bit(1) DEFAULT b'0',
  PRIMARY KEY (`idsede`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sede`
--

LOCK TABLES `sede` WRITE;
/*!40000 ALTER TABLE `sede` DISABLE KEYS */;
INSERT INTO `sede` VALUES (1,'Administración','\0'),(2,'Emergencia','\0'),(3,'Rayos X','\0'),(4,'Laboratorio','\0'),(5,'Neonatologia','\0'),(6,'U. cuidados Criticos','\0'),(7,'Odontologia','\0'),(8,'Desarrollo','\0'),(11,'Medicina Nuclear ','\0'),(12,'Rehabilitación ','\0');
/*!40000 ALTER TABLE `sede` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `servicio`
--

DROP TABLE IF EXISTS `servicio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `servicio` (
  `idservicio` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(250) DEFAULT NULL,
  `habilitado` bit(1) DEFAULT b'0',
  PRIMARY KEY (`idservicio`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `servicio`
--

LOCK TABLES `servicio` WRITE;
/*!40000 ALTER TABLE `servicio` DISABLE KEYS */;
INSERT INTO `servicio` VALUES (1,'Consulta Externa','\0'),(2,'Central Esterilización','\0'),(3,'Centro Obstetrico','\0'),(4,'Centro Quirurgico','\0'),(5,'Emergencia','\0'),(6,'Rehabilitación Fisica','\0'),(7,'Hospitalización','\0'),(8,'Imagenes Medicas','\0'),(9,'Laboratorio','\0'),(10,'Neonatologia','\0'),(11,'U. cuidados Criticos','\0');
/*!40000 ALTER TABLE `servicio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_usuario`
--

DROP TABLE IF EXISTS `t_usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_usuario` (
  `idtipouser` int(11) NOT NULL AUTO_INCREMENT,
  `tipo` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`idtipouser`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_usuario`
--

LOCK TABLES `t_usuario` WRITE;
/*!40000 ALTER TABLE `t_usuario` DISABLE KEYS */;
INSERT INTO `t_usuario` VALUES (1,'Medico'),(2,'Administrador'),(3,'Tecnico');
/*!40000 ALTER TABLE `t_usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ticket`
--

DROP TABLE IF EXISTS `ticket`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ticket` (
  `idticket` int(11) NOT NULL AUTO_INCREMENT,
  `idusuario` int(11) DEFAULT NULL,
  `idinvequipo` int(11) DEFAULT NULL,
  `descripcion` varchar(500) DEFAULT NULL,
  `fechainicio` datetime DEFAULT NULL,
  `estado` int(11) DEFAULT NULL,
  `tipomantenimiento` varchar(250) DEFAULT NULL,
  `informetecnico` varchar(5000) DEFAULT NULL,
  `fechafinal` date DEFAULT NULL,
  `color` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`idticket`),
  KEY `idusuario` (`idusuario`),
  KEY `idinvequipo` (`idinvequipo`),
  CONSTRAINT `ticket_ibfk_1` FOREIGN KEY (`idusuario`) REFERENCES `usuario` (`idusuario`),
  CONSTRAINT `ticket_ibfk_2` FOREIGN KEY (`idinvequipo`) REFERENCES `inventarioequipo` (`idinvequipo`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ticket`
--

LOCK TABLES `ticket` WRITE;
/*!40000 ALTER TABLE `ticket` DISABLE KEYS */;
INSERT INTO `ticket` VALUES (1,6,3,'MANTENIMIENTO PREVENTIVO, CREADO POR EL SISTEMA','2018-11-07 02:02:56',2,'Preventivo',NULL,NULL,'#19A7F8'),(8,6,3,'MANTENIMIENTO PREVENTIVO, CREADO POR EL SISTEMA','2018-11-27 02:02:56',2,'Preventivo',NULL,NULL,'#19A7F8'),(9,6,3,'MANTENIMIENTO PREVENTIVO, CREADO POR EL SISTEMA','2018-11-17 02:02:56',2,'Preventivo',NULL,NULL,'#19A7F8'),(10,6,3,'MANTENIMIENTO PREVENTIVO, CREADO POR EL SISTEMA','2018-12-07 02:02:56',2,'Preventivo',NULL,NULL,'#19A7F8'),(11,6,3,'MANTENIMIENTO PREVENTIVO, CREADO POR EL SISTEMA','2018-12-27 02:02:56',2,'Preventivo',NULL,NULL,'#19A7F8'),(12,6,5,'MANTENIMIENTO PREVENTIVO, CREADO POR EL SISTEMA','2018-11-07 03:35:36',0,'Preventivo',NULL,NULL,'#19A7F8'),(13,6,6,'MANTENIMIENTO PREVENTIVO, CREADO POR EL SISTEMA','2018-11-07 03:35:36',1,'Preventivo','El conector del equipo se cambio por uno nuevo',NULL,'#19A7F8'),(14,6,7,'MANTENIMIENTO PREVENTIVO, CREADO POR EL SISTEMA','2018-11-07 03:35:37',2,'Preventivo',NULL,NULL,'#19A7F8'),(15,6,8,'MANTENIMIENTO PREVENTIVO, CREADO POR EL SISTEMA','2018-11-07 03:51:09',2,'Preventivo',NULL,NULL,'#19A7F8'),(16,18,7,'Equipo de ultrasonidos presenta falso con el cable de alimentación. ','2018-11-07 03:51:45',2,'Correctivo',NULL,NULL,'#F81919'),(17,6,8,'MANTENIMIENTO PREVENTIVO, CREADO POR EL SISTEMA','2018-11-07 03:52:44',2,'Preventivo',NULL,NULL,'#19A7F8'),(18,18,7,'El display del ultrasonido tiene un falso, de manera que los parametros en la pantalla aparecen y desaparecen constantemente.','2018-11-07 10:10:24',2,'Correctivo',NULL,NULL,'#F81919'),(19,18,11,'No enciende el equipo ','2018-11-07 14:23:33',2,'Correctivo',NULL,NULL,'#F81919');
/*!40000 ALTER TABLE `ticket` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipoequipo`
--

DROP TABLE IF EXISTS `tipoequipo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipoequipo` (
  `idtequipo` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(250) DEFAULT NULL,
  `habilitado` int(11) DEFAULT NULL,
  PRIMARY KEY (`idtequipo`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipoequipo`
--

LOCK TABLES `tipoequipo` WRITE;
/*!40000 ALTER TABLE `tipoequipo` DISABLE KEYS */;
INSERT INTO `tipoequipo` VALUES (1,'Equipo medico',0),(2,'Terapia y rehabilitación',0),(3,'Esterilización',0),(4,'Apoyo medico',0),(5,'Imagenes diagnosticas',0),(6,'Odontologico',0),(7,'Laboratorio',0),(8,'Otro',0),(9,'Radioterapia ',0);
/*!40000 ALTER TABLE `tipoequipo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario` (
  `idusuario` int(11) NOT NULL AUTO_INCREMENT,
  `idtipouser` int(11) DEFAULT NULL,
  `area` varchar(250) DEFAULT NULL,
  `nombre` varchar(250) DEFAULT NULL,
  `username` varchar(250) DEFAULT NULL,
  `password` varchar(250) DEFAULT NULL,
  `telefono` varchar(9) DEFAULT NULL,
  `foto` varchar(250) DEFAULT NULL,
  `puesto` varchar(250) DEFAULT NULL,
  `salario` float DEFAULT NULL,
  `habilitado` bit(1) DEFAULT NULL,
  `sede` varchar(500) DEFAULT NULL,
  `idarea` int(11) DEFAULT NULL,
  PRIMARY KEY (`idusuario`),
  UNIQUE KEY `username` (`username`),
  KEY `idtipouser` (`idtipouser`),
  KEY `idarea` (`idarea`),
  CONSTRAINT `usuario_ibfk_1` FOREIGN KEY (`idtipouser`) REFERENCES `t_usuario` (`idtipouser`),
  CONSTRAINT `usuario_ibfk_2` FOREIGN KEY (`idarea`) REFERENCES `sede` (`idsede`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` VALUES (2,2,'3','Salvador Juárez','Juarez','8cd6c7848c5e7e4e9c0aa36cdaf8d591718342e8','7760-6044','1541476844.8681.jpg','Jefatura de Informatica',0,'','1',1),(6,2,'SISTEMA','SISTEMA','SISTEMA','965b38734b55904903e3f2e1589b99b7697a4546','0000-0000','SISTEMA','SISTEMA',0,'','1',1),(18,1,'Administración','Helmer Vasquez','Helmer','06b43948ef431b5fadea95a968e2db037428f1d3','7321-2548','1541483066.1718.jpg','Dermatologo ',0,'',NULL,12),(19,3,'Administración','Antonio Ventura ','Tony','cfa1150f1787186742a9a884b73a43d8cf219f9b','2215-5689','1541485998.4834.png','Técnico de equipo medico ',6.4,'',NULL,11),(23,3,NULL,'Fernando Torrento','Torrento','afc97ea131fd7e2695a98ef34013608f97f34e1d','5555-8888','1541581142.0048.jpg','Técnico de equipo medico ',4.5,'',NULL,11),(24,1,NULL,'Beatriz','Bea','6216f8a75fd5bb3d5f22b6f9958cdede3fc086c2','999-87888','1541583602.5812.jpg','Medica internista ',0,'',NULL,11),(25,1,NULL,'Amanda ','Amanda','51eac6b471a284d3341d8c0c63d0f1a286262a18','78151651','1541583750.4859.jpg','Pediatra ',0,'',NULL,5),(26,3,NULL,'Anabella','Anabella','fc7a734dba518f032608dfeb04f4eeb79f025aa7','419815165','1541583828.7349.jpg','Tecnica de equipo medico. ',5,'',NULL,5);
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-05-11 22:12:22
